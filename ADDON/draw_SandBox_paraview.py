#! /usr/bin/env python

# This script reads the output from a SandBox simulations and 
# prints several vtp files which called inside a single pvd file

# For pytevtk you have two options: either install it by pip install PyEVTK or
# install pyEVTK from source from https://bitbucket.org/pauloh/pyevtk 

# global imports 
#import vtk
#from evtk.hl import pointsToVTK # from source - pauloh bitbucket
from pyevtk.hl import pointsToVTK # from PyEVTK
import numpy as np


# gets the number of frames to be read
def get_nframes(fname) :
    '''First line assumed to me 'nframes = total number of frames', where 'nframes' is the 
    total number of frames printed'''
    auxfile = open(fname)
    auxdata = auxfile.readline().split()
    auxfile.close()
    print ("Number of frames = " + auxdata[0])
    return int(auxdata[0])

# gets the total number of particles. Planes are always 4
def get_nparticles(fname) :
    '''First line asuumed to be '4 NBODIES-4 NBODIES' '''
    auxfile = open(fname)
    auxdata = auxfile.readline().split()
    auxfile.close()
    print("Number of particles = " + auxdata[1])
    return int(auxdata[1])

# reads constant particles properties
def read_grains_properties(fname, radius) :
    ''' Reads the radius from bodies properties'''
    # open file
    auxfile = open(fname)
    # discard firs two lines
    auxfile.readline(); auxfile.readline();
    # discard iobj
    auxfile.readline();
    # read the radii
    for rad in auxfile.readline().replace('E', 'e').split() :
        radius.append(float(rad))
    # close filea
    auxfile.close()
        

# read a given frame, writes to several arrays, returns time and timestep
def read_frame(fstream) :
    '''Reads bodies positions, rotation, velocities, angular velocities, and alive status'''
    fstream.readline(); # skip first line
    timestep, time = map(float, fstream.readline().replace('E', 'e').split())
    rx    = map(float, fstream.readline().replace('E', 'e').split())
    ry    = map(float, fstream.readline().replace('E', 'e').split())
    rrot  = map(float, fstream.readline().replace('E', 'e').split())
    vx    = map(float, fstream.readline().replace('E', 'e').split())
    vy    = map(float, fstream.readline().replace('E', 'e').split())
    vrot  = map(float, fstream.readline().replace('E', 'e').split())
    alive = map(float, fstream.readline().replace('E', 'e').split())
    fstream.readline() # discards last new line
    # end
    return rx, ry, rrot, vx, vy, vrot, alive, timestep, time

# print particles to vtp file
def print_vtp(fname, nparticles, rx, ry, radius):
    x = np.array(rx[4:], dtype=float)
    y = np.array(ry[4:], dtype=float)
    z = np.zeros(nparticles, dtype=float)
    pointsToVTK(fname, x, y, z, data = {"radius" : np.array(radius[4:])})

# header, append line, and footer for pvd file


# Main 
from sys import argv

script, dirpath = argv
BASEDIR=dirpath

print("This script process SandBox output to write a series of vtk files into the display/ subdirectory")

# datastructures
radius = rx = ry = rrot = vx = vy = vrot = alive = []

# read general info
nframes=get_nframes(BASEDIR+"output/cpu")
nparticles=get_nparticles(BASEDIR+"output/parf")
nbodies = nparticles + 4

# read particle info
radius = []
read_grains_properties(BASEDIR+"output/parf", radius)

# open the file streams
posfile = open(BASEDIR+"output/posh")

# for each frame, read the frame and print to vtp
for iframe in range(nframes):
    rx, ry, rrot, vx, vy, vrot, alive, timestep, time = read_frame(posfile)
    print ("iframe, time = " + str(iframe) + " , " + str(time))
    print_vtp(BASEDIR+"display/particles"+str(iframe).zfill(5), nparticles, rx, ry, radius)
# close the posh file
posfile.close()

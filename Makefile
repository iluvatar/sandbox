SUBDIRS := SANDBOX BuildSample POSTPRO DRAW 
CLEANDIRS = $(SUBDIRS:%=clean-%)

.PHONY : all $(SUBDIRS)
all : $(SUBDIRS)

$(SUBDIRS) :
	@echo "\nProcessing : $@ ..."
	$(MAKE) -C $@ clean all
	@echo "Done.\n"

clean: $(CLEANDIRS)
$(CLEANDIRS): 
	$(MAKE) -C $(@:clean-%=%) clean

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     Input-output routines linked to program pre.f
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c==============================================================
c     SR rpre15
c     Preprocesor file: pre15
c-----Put particles on a regular triangular array in a rectangular box 

      subroutine rpre15(fid)
      implicit none 
      include '../VAR/preglob'

c-----local variables

      integer fid

      read(fid,*) nd,nl  				
      read(fid,*) rxw,ryw,rrotw
      read(fid,*) rdmin,rdmax
      read(fid,*) ngran,psp,pvsp
      read(fid,*) icol,xline,nwidth

      return
      end

c==============================================================
c     SR rpre16

c     Preprocesor file:  pre16

c     The particles are deposited by means of geometric criteria as to prepare
c     an initial configuration (dense or loose)
c    

      subroutine rpre16(fid)
      implicit none 
      include '../VAR/preglob'

c-----local variables

      integer i
      integer fid

      read(fid,*) nd, nl
      read(fid,*) rxw,ryw,rrotw
      read(fid,*) rdmin,rdmax
      read(fid,*) ngran,psp,pvsp

      read(fid,*) npg,npw,nphp
      read(fid,*) c1,c2,c3,chp1,chp2
      read(fid,*) zxmin,zymin,zxmax,zymax

      read(fid,*) nhp

c-----Read position and radii of heterogeneous (big) particles
      do i=1,nhp
        read(fid,*) rhp(i),xhp(i),yhp(i)
      enddo

      return
      end

c==============================================================
c     SR rpre17
c-----Read coordinates of the points in a geological unit,
c     and granulometry parameters

c     Preprocesor file:  pre17

      subroutine rpre17(fid)
      implicit none 
      include '../VAR/preglob'

c-----local variables

      integer i,j
      integer fid

       read(fid,*) ngu,nl,ehgu,nehgu,cris
       read(fid,*) rxw,ryw,rrotw

       do j=1,ngu
         read(fid,*) npgu(j)
         jtgu(j)=1
        do i=1,npgu(j)
          read(fid,*) xgu(i,j),ygu(i,j)
        enddo

         read(fid,*) rdming(j),rdmaxg(j)
         read(fid,*) ngrang(j),pspg(j),pvspg(j)

       enddo

         if (ehgu.eq.1) then
          read(fid,*)
          read(fid,*) rdming(ngu+1),rdmaxg(ngu+1)
c         read(fid,*) ngrang(ngu+1),pspg(ngu+1),pvspg(ngu+1)
         endif

       close(1)

      return
      end

c==============================================================
c     SR rpre18
c-----Read coordinates of the points in the geological units

c     Preprocesor file:  pre18

      subroutine rpre18(fid)
      implicit none 
      include '../VAR/preglob'

c-----local variables

      integer i,j
      integer fid

       read(fid,*) ngu,nguc,inftyg
       do j=1,ngu
         read(fid,*) npgu(j),jtgu(j)
        do i=1,npgu(j)
          read(fid,*) xgu(i,j),ygu(i,j)
        enddo
       enddo

       close(1)

      return
      end
c==============================================================
c     SR rpre19
c-----Read coordinates of the points in the slope

c     Preprocesor file:  pre19

      subroutine rpre19(fid)
      implicit none 
      include '../VAR/preglob'

c-----local variables

      integer i
      integer fid

       read(fid,*) nslc,inftys
       read(fid,*) npsl
      do i=1,npsl
        read(fid,*) xsl(i),ysl(i)
      enddo
       close(1)

      return
      end

c==============================================================
c     SR rpre20
c-----Read data concerning joints and fractures in geological units,
c    

c     Preprocesor file:  pre20

      subroutine rpre20(fid)
      implicit none 
      include '../VAR/preglob'

c-----local variables

      integer i,j
      integer fid

       read(fid,*) nfrac,ncfr,inftyf

       do j=1,nfrac
         read(fid,*) gufrac(j)
         read(fid,*) xfrac(1,j),yfrac(1,j),xfrac(2,j),yfrac(2,j)
        do i=1,gufrac(j)
          read(fid,*) ngufr(i,j),whfrac(i,j)
        enddo

       enddo

       close(1)

      return
      end

c==============================================================
c     SR rpre21
c-----Read coordinates of the points in the slope

c     Preprocesor file:  pre21

      subroutine rpre21(fid)
      implicit none 
      include '../VAR/preglob'



c-----local variables

      integer i,j
      integer fid

       read(fid,*) npbs,rdel,bshch
      do i=1,npbs
        read(fid,*) xb(i),yb(i)
      enddo

       read(fid,*) ngu
       read(fid,*) rxw,ryw,rrotw

       do j=1,ngu
         read(fid,*) npgu(j)
         jtgu(j)=1

        do i=1,npgu(j)
          read(fid,*) xgu(i,j),ygu(i,j)
        enddo

         read(fid,*) rdming(j),rdmaxg(j)
         read(fid,*) ngrang(j),pspg(j),pvspg(j)
         if (j.eq.1) then
             rdminm=rdming(j)
         else
             rdminm=dmin1(rdminm,rdming(j))
         endif

       enddo

       close(1)

      return
      end

c==============================================================
c     SR arcos(x,y,v,a)
c-----Calculate the arccos of a vector (x,y)  between [-pi,pi]

      subroutine arcos(x,y,v,a)
      implicit none
      include '../VAR/preglob'

      real*8 x,y,v,a

      if (dabs(x).le.v) then
        a=dacos(x/v)
      else
       if (x.gt.v) then
         a=0.D0
       else
         a=pi
       endif
      endif
       if (y.lt.0.D0) a=-a

      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     Input-output routines linked to programs pre.f, SandBox.f, and
c     post.f
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c==============================================================
c     SR rpar
c-----Read particle type and radius

      subroutine rpar(fid)
      implicit none 
      include '../VAR/glob'

c-----local variables

      integer i
      integer fid

      read(fid,*) nl,nd,np
      read(fid,*) ns,tm

      read(fid,*) (iobj(i),i=1,np)
      read(fid,*) (rd(i),i=1,np)
      read(fid,*) (idgd(i),i=1,np)

      return
      end

c==============================================================
c     SR wpar
c-----Write particle type and radius

      subroutine wpar(fid)
  
      implicit none 
      include '../VAR/glob'

c-----local variables

      integer i
      integer fid

      write(fid,*) nl,nd,np
      write(fid,*) ns,tm

      write(fid,*) (iobj(i),i=1,np)
      write(fid,*) (rd(i),i=1,np)
      write(fid,*) (idgd(i),i=1,np)

      return
      end

c==============================================================
c     SR rpos
c-----Read particle positions and velocities

      subroutine rpos(fid)
  
      implicit none 
      include '../VAR/glob'

c-----local variables

      integer i
      integer fid

      read(fid,*) nl,nd,np
      read(fid,*) ns,tm

      read(fid,*) (rx(i),i=1,np)
      read(fid,*) (ry(i),i=1,np)
      read(fid,*) (rrot(i),i=1,np)
      read(fid,*) (vx(i),i=1,np)
      read(fid,*) (vy(i),i=1,np)
      read(fid,*) (vrot(i),i=1,np)
      read(fid,*) (live(i),i=1,np)

      read(fid,*) 

      return
      end

c==============================================================
c     SR wpos
c-----Write particle positions and velocities

      subroutine wpos(fid)
  
      implicit none 
      include '../VAR/glob'

c-----local variables

      integer i
      integer fid


      write(fid,*) nl,nd,np
      write(fid,*) ns,tm

      write(fid,*) (rx(i),i=1,np)
      write(fid,*) (ry(i),i=1,np)
      write(fid,*) (rrot(i),i=1,np)
      write(fid,*) (vx(i),i=1,np)
      write(fid,*) (vy(i),i=1,np)
      write(fid,*) (vrot(i),i=1,np)
      write(fid,*) (live(i),i=1,np)

      write(fid,*) 

      return
      end

c==============================================================
c     SR rposfh
c-----Read last record of particle positions and velocities in history file

      subroutine rposfh(fid)
  
      implicit none 
      include '../VAR/glob'

c-----local variables

      integer i
      integer fid

 10   read(fid,*,END=20) nl,nd,np
      read(fid,*) ns,tm

      read(fid,*) (rx(i),i=1,np)
      read(fid,*) (ry(i),i=1,np)
      read(fid,*) (rrot(i),i=1,np)
      read(fid,*) (vx(i),i=1,np)
      read(fid,*) (vy(i),i=1,np)
      read(fid,*) (vrot(i),i=1,np)
      read(fid,*) (live(i),i=1,np)

      read(fid,*) 
     
      goto 10

 20   continue

      return
      end

c==============================================================
c     SR rcon
c-----Read contact forces

      subroutine rcon(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer fid,i


      read(fid,*) nl,nd,np
      read(fid,*) nc,ns,tm

      read(fid,*) (cpoint(i),i=1,np)
      read(fid,*) (clist(i),i=1,nc)

      read(fid,*) (fn(i),i=1,nc)
      read(fid,*) (ft(i),i=1,nc)
      read(fid,*) (fs(i),i=1,nc)
      read(fid,*) (fel(i),i=1,nc)
      read(fid,*) (csta(i),i=1,nc)
      read(fid,*) (dani(i),i=1,nc)
      read(fid,*) (danj(i),i=1,nc)
      read(fid,*) (dpor(i),i=1,nc)
      read(fid,*) (cax(i),i=1,nc)
      read(fid,*) (axn(i),i=1,nc)
      read(fid,*) (axt(i),i=1,nc)

      read(fid,*)

      return
      end

c==============================================================
c     SR wcon

c-----Write contact forces

      subroutine wcon(fid)
  
      implicit none 
      include '../VAR/mpglob'

c     local variables

      integer fid,i


      write(fid,*) nl,nd,np
      write(fid,*) nc,ns,tm

      write(fid,*) (cpoint(i),i=1,np)
      write(fid,*) (clist(i),i=1,nc)

      write(fid,*) (fn(i),i=1,nc)
      write(fid,*) (ft(i),i=1,nc)
      write(fid,*) (fs(i),i=1,nc)
      write(fid,*) (fel(i),i=1,nc)
      write(fid,*) (csta(i),i=1,nc)
      write(fid,*) (dani(i),i=1,nc)
      write(fid,*) (danj(i),i=1,nc)
      write(fid,*) (dpor(i),i=1,nc)
      write(fid,*) (cax(i),i=1,nc)
      write(fid,*) (axn(i),i=1,nc)
      write(fid,*) (axt(i),i=1,nc)
      write(fid,*)

      return
      end

c==============================================================
c     SR rprop
c-----Read parameters of the system including physical ande mechanical
c     properties of the geological units

      subroutine rprop(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer i,fid
      
c--------1---------2---------3---------4---------5---------6---------7----

c      Physical and mecanical porperties of granular materials
	  read(fid,*)
	  read(fid,*) gv,gvdir 
	  read(fid,*)
	  read(fid,*) nrho  ! nrho = number of densities
      do i=1,nrho
	     read(fid,*) rhol(i)
      enddo
	  read(fid,*)
	  read(fid,*) nrc  ! nrc = number of restitution coefficients
      do i=1,nrc
	     read(fid,*) enl(i),etl(i),esl(i)
      enddo
	  read(fid,*)
	  read(fid,*) nmp  ! nmp = number of mechanical parameters
      do i=1,nmp
	     read(fid,*) mul(i),cstr(i),exlr(i),
     &            sllr(i),exln(i),slln(i)
      enddo
	  read(fid,*)
	  read(fid,*) nnc  ! nnc = number of new contact distance criteria
      do i=1,nnc
	     read(fid,*) newc(i),dnewr(i),dnewn(i)
      enddo
	  read(fid,*)
	  read(fid,*) ncd  ! ncd = number of cohesion distance (ellipse)
      do i=1,ncd
          read(fid,*) neli(i),axnr(i),
     &         axtr(i),axnn(i),axtn(i)
      enddo

      return
      end
c==============================================================
c     SR wprop
c-----Write parameters of the system including physical ande mechanical
c     properties of the geological units. This routine is usefull to
c     verify if the input files have been read correctly.

      subroutine wprop(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer i,fid
      
c--------1---------2---------3---------4---------5---------6---------7----

c      Physical and mecanical porperties of granular materials
	  write(fid,*)
	  write(fid,*) gv,gvdir 
	  write(fid,*)
	  write(fid,*) nrho  ! nrho = number of densities
      do i=1,nrho
	     write(fid,*) rhol(i)
      enddo
	  write(fid,*)
	  write(fid,*) nrc  ! nrc = number of restitution coefficients
      do i=1,nrc
	     write(fid,*) enl(i),etl(i),esl(i)
      enddo
	  write(fid,*)
	  write(fid,*) nmp  ! nmp = number of mechanical parameters
      do i=1,nmp
	     write(fid,*) mul(i),cstr(i),exlr(i),
     &             sllr(i),exln(i),slln(i)
      enddo
	  write(fid,*)
	  write(fid,*) nnc  ! nnc = number of new contact distance criteria
      do i=1,nnc
	     write(fid,*) newc(i),dnewr(i),dnewn(i)
      enddo
	  write(fid,*)
	  write(fid,*) ncd  ! ncd = number of cohesion distance (ellipse)
      do i=1,ncd
          write(fid,*) neli(i),axnr(i),
     &          axtr(i),axnn(i),axtn(i)
      enddo

      return
      end

c==============================================================
c     SR rdom
c-----Read parameters of the system including physical ande mechanical
c     properties of the geological units

      subroutine rdom(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer i,k,fid
	  
      k=1 ! there is only one step
      
      read(fid,*)
      read(fid,*) ngd
	  read(fid,*)
      do i=1,ngd
	     read(fid,*) gdrho(i)
      enddo
      read(fid,*)
      do i=1,ngd
	     read(fid,*) gdrc(i,1,k),gdmpr(i,1,k),
     &            gdnc(i,1,k),gdcd(i,1,k),
     &            gdrc(i,2,k),gdmpr(i,2,k),
     &            gdnc(i,2,k),gdcd(i,2,k)
      enddo
      read(fid,*)
      read(fid,*) ilaymp(k),ilayc(k),ilayf(k)

      return
      end
c==============================================================
c     SR wdom
c-----Write parameters of the system including physical ande mechanical
c     properties of the geological units. This routine is usefull to
c     verify if the input files have been read correctly.

      subroutine wdom(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer i,k,fid
	  
      k=1 ! there is only one step
      
      write(fid,*)
      write(fid,*) ngd
	  write(fid,*)
      do i=1,ngd
	     write(fid,*) gdrho(i)
      enddo
      write(fid,*)
      do i=1,ngd
	     write(fid,*) gdrc(i,1,k),gdmpr(i,1,k),
     &             gdnc(i,1,k),gdcd(i,1,k),
     &             gdrc(i,2,k),gdmpr(i,2,k),
     &             gdnc(i,2,k),gdcd(i,2,k)
      enddo
      write(fid,*)
      write(fid,*) ilaymp(k),ilayc(k),ilayf(k)

      return
      end

c==============================================================
c     SR rwal
c-----Read parameters of the system including physical ande mechanical
c     properties of the geological units

      subroutine rwal(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer i,k,fid
      
c--------1---------2---------3---------4---------5---------6---------7----

      k=1 ! there is only one step

      read(fid,*)
      read(fid,*) nl,speri
      read(fid,*)
	  read(fid,*) sfmaw,sfmiw
	  read(fid,*)
	  
c     read the surface properties of the 4 walls
	  do i=1,nl
         read(fid,*) warc(i,1,k),wamp(i,1,k),wanc(i,1,k),wacd(i,1,k),
     &               warc(i,2,k),wamp(i,2,k),wanc(i,2,k),wacd(i,2,k)
      enddo
      read(fid,*)

c     read the driven degrees of freedom of the 4 walls
	  do i=1,nl	  
         read(fid,*)
         read(fid,*)
         read(fid,*) idfx(i),idfunx(i)
		 read(fid,*) cnst1x(i),amp_x(i),freq_x(i),
     &               phse_x(i),cnst2x(i),cnst3x(i),tmx(i)
         read(fid,*)
         read(fid,*) idfy(i),idfuny(i)
		 read(fid,*) cnst1y(i),amp_y(i),freq_y(i),
     &               phse_y(i),cnst2y(i),cnst3y(i),tmy(i)
      enddo

      return
      end
c==============================================================
c     SR wwal
c-----Write parameters of the system including physical ande mechanical
c     properties of the geological units. This routine is usefull to
c     verify if the input files have been read correctly.

      subroutine wwal(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer i,k,fid
      
c--------1---------2---------3---------4---------5---------6---------7----

      k=1 ! there is only one step

      write(fid,*)
      write(fid,*) nl,speri
      write(fid,*)
	  write(fid,*) sfmaw,sfmiw
	  write(fid,*)
c     read the surface properties of the 4 walls
	  do i=1,nl
         write(fid,*) warc(i,1,k),wamp(i,1,k),wanc(i,1,k),wacd(i,1,k),
     &               warc(i,2,k),wamp(i,2,k),wanc(i,2,k),wacd(i,2,k)
      enddo
      write(fid,*)
c     read the driven degrees of freedom of the 4 walls
	  do i=1,nl	  
         write(fid,*)
         write(fid,*)
         write(fid,*) idfx(i),idfunx(i)
		 write(fid,*) cnst1x(i),amp_x(i),freq_x(i),
     &               phse_x(i),cnst2x(i),cnst3x(i),tmx(i)
         write(fid,*)
         write(fid,*) idfy(i),idfuny(i)
		 write(fid,*) cnst1y(i),amp_y(i),freq_y(i),
     &               phse_y(i),cnst2y(i),cnst3y(i),tmy(i)
      enddo

      return
      end

c==============================================================
c     SR rexec
c-----Read parameters of biaxial numerical experiment

      subroutine rexec(fid)
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer fid

       read(fid,*)
       read(fid,*)
       read(fid,*) inftyg,ouftyg
       read(fid,*)
       read(fid,*) ihis,nhis
       read(fid,*)
       read(fid,*) icon
	   read(fid,*)
	   read(fid,*) nver,dver
       read(fid,*)
       read(fid,*) dt
       read(fid,*) nitermn,nitermx
       read(fid,*) epsf
       read(fid,*)
       read(fid,*) nsf
       read(fid,*)
       read(fid,*) ielas,iovlap
       read(fid,*) rov,ieoab,nefi       ! elastic and overlap parameters
       read(fid,*) eoa,eob,pk           ! definition of elastic law
       read(fid,*) plef,pler,sef,pef    ! definition of elastic law

      return
      end
c==============================================================
c     SR wexec
c-----Write parameters of biaxial numerical experiment. This routine is usefull to
c     verify if the input files have been read correctly.

      subroutine wexec(fid)
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer fid

       write(fid,*)
       write(fid,*)
       write(fid,*) inftyg,ouftyg
       write(fid,*)
       write(fid,*) ihis,nhis
       write(fid,*)
       write(fid,*) icon
	   write(fid,*)
	   write(fid,*) nver,dver
       write(fid,*)
       write(fid,*) dt
       write(fid,*) nitermn,nitermx
       write(fid,*) epsf
       write(fid,*)
       write(fid,*) nsf
       write(fid,*)
       write(fid,*) ielas,iovlap
       write(fid,*) rov,ieoab,nefi       ! elastic and overlap parameters
       write(fid,*) eoa,eob,pk           ! definition of elastic law
       write(fid,*) plef,pler,sef,pef    ! definition of elastic law

      return
      end

c==============================================================
c     SR wwallx
c-----Write stress/strain of wall(i) in the x direction

      subroutine wwallx(fid,i)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer fid,i

      write(fid,*) tm,rx(i),vx(i),fcwx(i),fxe(i)

c 10   format(F9.5,1x,F8.4,1x,F6.3,1x,F10.1,1x,F14.1,1x,I3)
      return
      end
c==============================================================
c     SR wwally
c-----Write stress/strain of wall(i) in the y direction

      subroutine wwally(fid,i)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer fid,i

      write(fid,*) tm,ry(i),vy(i),fcwy(i),fye(i)

c 10   format(F9.5,1x,F8.4,1x,F6.3,1x,F10.1,1x,F14.1,1x,I3,1x,F14.6)
      return
      end
c==============================================================
c     SR wcpu
c-----Write a cpu file

      subroutine wcpu
	  
      implicit none 
      include '../VAR/mpglob'
	    
c-----Local variables

      integer ntf
      real etime,cpu(2),cputime,cpunorm  ! for CPU time
c	  character*15 f_cpu
   
c-----CPU time 

       cputime = etime(cpu)
       cpunorm = cputime / ((ns-nsi)*np)
	   
c	   f_cpu = 'cpu'//ouftyg
	   
       open(1,file='./output/cpu')
 
c      save information concerning the number of frames in history files
       if (ihis.eq.1) then
         ntf=int(float(ns-(nsi+1))/float(nhis)) + 1
         write(1,*) ntf,'  = total number of frames '
       endif

       write(1,'(a,f12.1,a)') ' Job was consumed ', 
     &               cputime, ' CPU seconds'
       write(1,'(a,e12.4,a)') ' Per grain and time step: ',
     &               cpunorm,' CPU seconds'
       write(1,*) 
       write(1,*) 'Number of steps done: ',ns-nsi
       write(1,'(a,f12.3,a)') ' Physical time duration: ',
     &           dfloat(ns-nsi)*dt,' seconds.'
       close(1)
	   
      return
      end

c==============================================================
c     SR wdistot
c-----Write particle positions and velocities

      subroutine wdistot(fid)
  
      implicit none 
      include '../VAR/glob'

c-----local variables

      integer i
      integer fid


      write(fid,*) nl,nd,np
      write(fid,*) ns,tm

      write(fid,*) (xtot(i),i=1,np)
      write(fid,*) (ytot(i),i=1,np)
      write(fid,*) (rottot(i),i=1,np)

      write(fid,*) 

      return
      end

c==============================================================
c     SR wipfv
c-----Write particle positions and velocities

      subroutine wipfv(fid)
  
      implicit none 
      include '../VAR/mpglob'

c-----local variables

      integer i
      integer fid

      do i=1,nump
       write(fid,*) tm,fx(nip(i)),fy(nip(i)),vx(nip(i)),vy(nip(i))
      enddo 

      return
      end

c==============================================================
c     SR rotation(x,y,teta,xp,yp)
c-----Calculate coordinates of a point, after counter-clockwise teta rotation 

      subroutine rotation(x,y,teta,xp,yp)
      implicit none
      include '../VAR/mpglob'

      real*8 x,y,teta,xp,yp

      xp=cos(teta)*x-sin(teta)*y
      yp=sin(teta)*x+cos(teta)*y

      return
      end

      program draw_SandBox
      implicit none 

      include '../VAR/gpglob'

c-----Local variables

      integer i,k,ntf,nfcount

c-----Constants

      pi=4.d0*datan(1.d0)
      pi2=pi/2.d0
      ra2de=180.d0/pi
      de2ra=1.d0/ra2de

c-----Read the menu for processing

      open(1,file='./input/draw_parameters',status='unknown')

      read(1,*)
      read(1,*)
      read(1,*) ihfile
      read(1,*)
      read(1,*) ifcon
      read(1,*)
      read(1,*) f_par
      read(1,*) f_pos
      read(1,*) f_con
      read(1,*)
      read(1,*) nodmin
      read(1,*) sfn
      read(1,*) sft
      read(1,*) sfs
      read(1,*) snet
      read(1,*) nanit
      do i=1,nanit
         read(1,*) nhfr(i),namhfr(i)
      enddo

c      do i=1,nanit
c         write(*,*) nhfr(i),namhfr(i)
c      enddo
c      read(*,*)

      close(1)
      write(*,*) 'Read the "draw_parameters" file --> OK'
	  
c-----Read system data

      open(1,file='./input/exec_parameters',status='unknown')
      call rexec(1)
      close(1)
      write(*,*) 'Read the "exec_parameters" file --> OK'

c-----Read material properties of the system
         
      open(1,file='./input/properties',status='unknown')
      call rprop(1)
      close(1)
      write(*,*) 'Read the "properties" file --> OK'
	
      open(1,file='./input/walls',status='unknown')
      call rwal(1)
      close(1)
      write(*,*) 'Read the "walls" file --> OK'
                 
      open(1,file='./input/domains',status='unknown')
      call rdom(1)
      close(1)
      write(*,*) 'Read the "domains" file --> OK'

c       ks indicates the number of phase in the numerical experiment
c   For geo.f: 1 / 2 = relaxation / erosion;

      ks=1
      
c     read the initial positions and velocities of the partilces

      open(1,file=f_pos)

      read(1,*) nl,nd,np
      read(1,*) ns,tm
      read(1,*) (rxo(i),i=1,np)
      read(1,*) (ryo(i),i=1,np)
      read(1,*) (rroto(i),i=1,np)

      close(1)

c     open the particles, positions, and contacts files

c      f_pos = './input/'//f_pos
c      f_con = './input/'//f_con
c      f_par = './input/'//f_par

      open(11,file=f_pos)
      open(12,file=f_con)
      open(13,file=f_par)

      if (ihfile.eq.0) then ! there are not history files, and only one
c        is going to be read
         write(*,*) 'No history files, only one frame will be read'

c     read the particles file

         call rpar(13)
         write(*,*) 'Read the "par" file --> OK'

c     read the positions and velocities

         call rpos(11)
         write(*,*) 'Read the "pos" file --> OK'
       
c     read the contacts

         if (ifcon.eq.1) then
		    call rcon(12)
			write(*,*) 'Read the "con" file --> OK'
         endif
c     copy arrays of particle properties. This stage redefines
c     all this properties as reals of single precision.
c         do i=1,np
c            rxoc(npl)=sngl(rxo(i))
c            ryoc(npl)=sngl(ryo(i))
c            rrotoc(npl)=sngl(rroto(i))
            
c            rxc(npl)=sngl(rx(i))
c            ryc(npl)=sngl(ry(i))
c            rrotc(npl)=sngl(rrot(i))

c            vxc(npl)=sngl(vx(i))
c            vyc(npl)=sngl(vy(i))
c            vrotc(npl)=sngl(vrot(i))
               
c            rdc(npl)=sngl(rd(i))
c            idgdc(npl)=idgd(i)

c         enddo

c     initialize the minimum and maximum radius
         ddmin=sngl(rd(nl+1))
         ddmax=sngl(rd(nl+1))

         do i=(nl+2),np
            ddmin=amin1(ddmin,sngl(rd(i)))
		    ddmax=amax1(ddmax,sngl(rd(i)))
         enddo

         call gmv_file()

      elseif (ihfile.eq.1) then ! history frames will be read
         write(*,*) 'History files, ',nanit,' frames will be read'

c     read the particles file

         call rpar(13)
		 write(*,*) 'Read the "par" file --> OK'

c     read the pos and con history files
       
         nfcount=1
   
         do kfr=1,nanit

c           history file
c           Note that the initial frame corresponds to nfcount=0
            do while (nfcount.le.nhfr(kfr))
c           read a frame from the history file
			   write(*,*) 'nanit ',nanit
			   write(*,*) 'kfr ',kfr
			   write(*,*) 'nhfr(kfr) ',nhfr(kfr)
			   write(*,*) 'nfcount ',nfcount
               call rpos(11)
               if (ifcon.eq.1) then
			      call rcon(12)
			   endif
			   if (nfcount.eq.nhfr(kfr)) then
			      write(*,*) 'Read the "posh" file # ',nfcount,'--> OK'
			      write(*,*) 'Read the "conh" file # ',nfcount,'--> OK'
			   endif
               nfcount=nfcount+1
            enddo

c     copy arrays of particle properties. This stage redefines
c     all this properties as reals of single precision.
c            do i=1,np
c               rxoc(npl)=sngl(rxo(i))
c               ryoc(npl)=sngl(ryo(i))
c               rrotoc(npl)=sngl(rroto(i))
            
c               rxc(npl)=sngl(rx(i))
c               ryc(npl)=sngl(ry(i))
c               rrotc(npl)=sngl(rrot(i))

c               vxc(npl)=sngl(vx(i))
c               vyc(npl)=sngl(vy(i))
c               vrotc(npl)=sngl(vrot(i))
               
c               rdc(npl)=sngl(rd(i))
c               idgdc(npl)=idgd(i)

c            enddo

c     initialize the minimum and maximum radius
            ddmin=sngl(rd(nl+1))
            ddmax=sngl(rd(nl+1))

            do i=(nl+2),np
               ddmin=amin1(ddmin,sngl(rd(i)))
               ddmax=amax1(ddmax,sngl(rd(i)))
            enddo
            call gmv_file()  
         enddo
      endif

      close(11) 
      close(12)
      close(13)

      stop '--- Data processing terminated!'

      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c-----Associated routines

      include '../LIB/io.f'
      include '../DRAW/gmv_file.f'


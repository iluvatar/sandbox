
c     SR gmv_file
c     create a GMV file

      subroutine gmv_file()
      implicit none       
      include '../VAR/gpglob'

      integer i

      matcar(1)='fn_ne'
      matcar(2)='fn_po'
      matcar(3)='ft_ne'
      matcar(4)='ft_po'
      matcar(5)='M_ne'
      matcar(6)='M_po'
      matcar(7)='ft_max'
      matcar(8)='M_max'
      matcar(9)='ft_sli'
      matcar(10)='M_rol'
      matcar(11)='net'
      matcar(12)='net_st'
      matcar(13)='net_we'
      matcar(14)='net_te'
      matcar(15)='net_co'
      matcar(16)='net_in'
      matcar(17)='net_re'
      matcar(18)='net_sli'
      matcar(19)='net_rol'

      npoly=19

      rxw=rx(4)-rx(2)  
      ryw=ry(3)-ry(1)
	  rxw2=rxw/2.0

      do i=1,npoly
        nmatc(i)=matcar(i)
c        write(*,*) nmatc(i)
      enddo

      call pcnum()

c     write surfaces from nodes

      call gwnode()

c     write file with velocities of surfaces (disks)

      call gwsvar()

c     write polygons indicating forces in contacts 

      call gwpoly()

c     copy the different files into one GMV file

      call gfilet()

      return
      end

      include '../DRAW/gfilet.f'

c***************************************
      subroutine gwnode()
c     calculate coordinates of nodes,
c     associate to nodes material corresponding to color
c     calculate disks from nodes

      implicit none    
      include '../VAR/gpglob'

      integer i,j,nodp,ncol,ncount,cnod(50)
      real xpar,ypar,rpar,apar,alfa,xnod,ynod,znod

c      open and create node file

       open(1,file='./display/xynode',status='unknown')
       open(2,file='./display/manode',status='unknown')
       open(3,file='./display/sunode',status='unknown')

       write(3,*) 'surface   ',nd

       nodt=0
       znod=0.d0

c      calculate total number of nodes in the disks 'nodt'

      do i=nl+1,np
        if (live(i).eq.1) then
        rpar=rd(i)
c        nodp = number of nodes in disk 'i'
         nodp=nodmin+int(rpar)
         nodt=nodt+nodp
        endif
      enddo

       write(1,40) 'nodev',nodt
c       write(*,*)nodp,rpar
 40    format(A8,I8)

       ncount=0

      do i=nl+1,np
       if (live(i).eq.1) then
c       calculate coord. of nodes in each disk
        xpar=rx(i)
        ypar=ry(i)
        rpar=rd(i)
        nodp=nodmin+int(rpar)
        apar=2.d0*sngl(pi)/float(nodp)
        ncol=(p_nc(i)+1)

        do j=1,nodp
          cnod(j)=ncol
          alfa=float(j)*apar
          xnod=xpar+rpar*cos(alfa)
          ynod=ypar+rpar*sin(alfa)
          write(1,*) xnod,ynod,znod
        enddo
c        write color of nodes in particle 'i'
         write(2,*)(cnod(j),j=1,nodp)
c        write nodes in each surface corresponding to particle 'i'
         write(3,*) nodp
         write(3,*)(j,j=ncount+1,ncount+nodp)
         ncount=ncount+nodp
       endif

      enddo

       close(1)
       close(2)
       close(3)

      return
      end

c***************************************
      subroutine gwsvar()
c     write velocities of surfaces (disks)

      implicit none    
      include '../VAR/gpglob'

      integer i

c      open and create node file

       open(1,file='./display/survar',status='unknown')

       write(1,*) 'surfmats'

       write(1,*) ((p_nc(i)+1),i=nl+1,np)

       write(1,*) 'surfvel'

       write(1,*) (vx(i),i=nl+1,np)
       write(1,*) (vy(i),i=nl+1,np)
       write(1,*) (0.0,i=nl+1,np)

       write(1,*) 'surfvars'

       write(1,*) 'x-displ'
       write(1,*) (rx(i)-rxo(i),i=nl+1,np)

       write(1,*) 'y-displ'
       write(1,*) (ry(i)-ryo(i),i=nl+1,np)

       write(1,*) 't-displ'
       write(1,*) (sqrt((rx(i)-rxo(i))**2+(ry(i)-rroto(i))**2)
     &             ,i=nl+1,np)

       write(1,*) 'rotate'
       write(1,*) (rrot(i),i=nl+1,np)

       write(1,*) 'mark'
       write(1,*) (idgd(i),i=nl+1,np)

       write(1,*) 'radius'
       write(1,*) (rd(i),i=nl+1,np)

       write(1,*) 'p_nc'
       write(1,*) (p_nc(i),i=nl+1,np)

       write(1,*) 'endsvars'

       close(1)

      return
      end

c***************************************
      subroutine gwpoly()
c     write polygons indicating informations at the contacts 

c     polygons are defined according to the original arrays of particles

      implicit none    
      include '../VAR/gpglob'
      integer i

       wmvpoly(1)='21   4'
       wmvpoly(2)='22   4'
       wmvpoly(3)='23   4'
       wmvpoly(4)='24   4'
       wmvpoly(5)='25   4'
       wmvpoly(6)='26   4'
       wmvpoly(7)='27   4'
       wmvpoly(8)='28   4'
       wmvpoly(9)='29   4'
       wmvpoly(10)='30   4'
       wmvpoly(11)='31   4'
       wmvpoly(12)='32   4'
       wmvpoly(13)='33   4'
       wmvpoly(14)='34   4'
       wmvpoly(15)='35   4'
       wmvpoly(16)='36   4'
       wmvpoly(17)='37   4'
       wmvpoly(18)='38   4'
       wmvpoly(19)='39   4'

      do i=1,npoly ! for the 16 kinds of polygons
c       write(*,*) i,nmatc(i)
         open(i+20,file='./display/'//nmatc(i),status='unknown')
      enddo

      call postforce()

      do i=1,npoly
         close(i+20)
      enddo

      return
      end

c==============================================================
c     SR postforce
c-----Calculate scaling parameters on the screen

      subroutine postforce()
      implicit none
      include '../VAR/gpglob'

c-----local variables

      integer i,j,k,js
      integer jbeg,jend,jnab
      integer igu,jgu,impr,jmpr
      real*8 rxi,ryi,rdi,rxj,ryj,rdj
      real*8 nxij,nyij,wpl,rxij
      real*8 mudd,mu_i,mu_j,ftsli
      real*8 sldd,sl_i,sl_j,fsrol
      real*8 exldd,exl_i,exl_j
      real*8 xpl(4),ypl(4),zpl(4)
      real*8 fnmean,nc_fn

c-----Contact forces between particles are displayed 

      zpl(1)=0.
      zpl(2)=0.
      zpl(3)=0.
      zpl(4)=0.

c        cov=5.d0/ovlapmax

c calculate the mean normal force:

      fnmean = 0.
      nc_fn = 0.

      do i= 1,np
         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1  
         if (jbeg.le.jend) then
            do jnab = jbeg,jend
			   if (fn(jnab).ge.0.d0) then ! only for compressive forces
                  fnmean = fnmean + fn(jnab)
                  nc_fn = nc_fn + 1.
			   endif
            enddo
         endif
      enddo

      fnmean = fnmean/nc_fn
 
      do i = 1,np
         rxi = rx(i)
         ryi = ry(i)
         rdi = rd(i)

         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1       

         if (jbeg.le.jend) then

            do jnab = jbeg,jend
						
c        contact list for every particle is analysed
               j = clist(jnab) 
c        rxj, ryj = screen coordinates of the centre of disk j
               rxj = rx(j)
               ryj = ry(j)
               rdj = rd(j)
               rxij = rxi - rxj
c        nxij, nyij = coords. of unitary vector from j to i. This vector is
c        perpendicular to the contact surface between particles i and j

c        note that distance between particles ought to be corrected 
c        if overlap is not small
               if (i.le.nl) then
                  if (i.eq.1) then
c          normal to the base of the lid
                     nxij=0.0d0
                     nyij=-1.0d0
                     rxi=rxj
			      endif
                  if (i.eq.2) then
c           normal to the left vertical wall
				     nxij=-1.0d0
                     nyij=0.0d0
                     ryi=ryj
                  endif
                  if (i.eq.3) then
c           normal to the horizontal top wall
				     nxij=0.0d0
                     nyij=1.0d0
                     rxi=rxj
                  endif
                  if (i.eq.4) then
c            normal to the right vertical wall
                     nxij=1.0d0
                     nyij=0.0d0
                     ryi=ryj
                  endif
			   else
                  if (speri.and.(DABS(rxij).gt.rxw2)) then
                     if (rxij.gt.0.d0) then
                        rxij=rxij-rxw
                     else
                        rxij=rxij+rxw
                     endif
				  endif
                  nxij = rxij/(rdi+rdj)
                  nyij = (ryi-ryj)/(rdi+rdj)     
			   endif

c choose the mechanical parameters that gouvern the behavior of
c the contact

               igu=idgd(i)
               jgu=idgd(j)

               if (csta(jnab)) then
c          initial behavior law
			      js = 1
               else 
c          residual behavior law
			      js = 2
               endif

               impr = gdmpr(igu,js,ks)
               jmpr = gdmpr(jgu,js,ks)

c     assign the mechanical parameters for particle i
               mu_i=mul(impr)
               if (cstr(impr).eq.1) then ! mechanical parametrers are real quantities
			      exl_i=exlr(impr)
                  sl_i=sllr(impr)
               elseif (cstr(impr).eq.2) then ! mechanical parametrers are defined as 
c                                   normalized quantities
                  exl_i=exln(impr)*(rdi+rdj)
                  sl_i=slln(impr)*(rdi+rdj)
               endif

c     assign the mechanical parameters for particle j
               mu_j=mul(jmpr)
               if (cstr(jmpr).eq.1) then ! mechanical parametrers are real quantities
                  exl_j=exlr(jmpr)
                  sl_j=sllr(jmpr)
			   elseif (cstr(jmpr).eq.2) then ! mechanical parametrers are defined as 
c                                   normalized quantities
                  exl_j=exln(jmpr)*(rdi+rdj)
                  sl_j=slln(jmpr)*(rdi+rdj)
               endif

               if (igu.eq.jgu) then ! both disks belong to the same geological
c                             domain
                  mudd=mu_i
                  exldd=exl_i
                  sldd=sl_i
               else
                  if (ilayc(ks)) then ! inter-layer cohesion is present
                     if (ilaymp(ks).eq.1) then
                        exldd=dmin1(exl_i,exl_j)
                     elseif (ilaymp(ks).eq.2) then
                        exldd=(exl_i+exl_j)/2.d0
					 elseif (ilaymp(ks).eq.3) then
                        exldd=dmax1(exl_i,exl_j)
                     endif
                  elseif (.not.ilayc(ks)) then ! inter-layer cohesion is not present
                     exldd=0.d0
                  endif         
                  if (ilayf(ks)) then ! inter-layer friction is present
				     if (ilaymp(ks).eq.1) then
                        mudd=dmin1(mu_i,mu_j)
                        sldd=dmin1(sl_i,sl_j)
				     elseif (ilaymp(ks).eq.2) then
                        mudd=(mu_i+mu_j)/2.d0
                        sldd=(sl_i+sl_j)/2.d0
                     elseif (ilaymp(ks).eq.3) then
					    mudd=dmax1(mu_i,mu_j)
                        sldd=dmax1(sl_i,sl_j)
                     endif
				  elseif (.not.ilayc(ks)) then ! inter-layer friction is not present
                     mudd=0.d0
                     sldd=0.d0
                  endif
               endif

               ftsli=(0.9999d0)*mudd*(exldd + fn(jnab))
               fsrol=(0.9999d0)*sldd*(exldd + fn(jnab))

c calculate the scale factors and draw the polygons

c               if (i.gt.nl) then ! doing this, we do not include contacts
c with walls			   
			   do k=1,npoly

c normal forces (negative and positive)

                  if ((fn(jnab).lt.0.d0).and.
     &                (nmatc(k).eq.'fn_ne')) then
                     wpl=sfn*DABS(fn(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if ((fn(jnab).ge.0.d0).and.(nmatc(k).eq.'fn_po')) then
                     wpl=sfn*DABS(fn(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c tangential forces (negative and positive)

                  if ((nmatc(k).eq.'ft_ne').and.
     &                (ft(jnab).lt.0.d0)) then
                     wpl=sft*DABS(ft(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if ((nmatc(k).eq.'ft_po').and.
     &                (ft(jnab).gt.0.)) then
				     wpl=sft*DABS(ft(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c torques (negative and positive)

                  if ((nmatc(k).eq.'M_ne').and.
     &                (fs(jnab).lt.0.)) then
                     wpl=sfs*DABS(fs(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if ((nmatc(k).eq.'M_po').and.
     &                (fs(jnab).gt.0.)) then
                     wpl=sfs*DABS(fs(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c maximum tangential force and torque that can be supported by a contact

                  if (nmatc(k).eq.'ft_max') then
                     wpl=sft*mudd*DABS(fn(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if (nmatc(k).eq.'M_max') then
                     wpl=sfs*sldd*DABS(fn(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c sliding contacts

                  if ((nmatc(k).eq.'ft_sli').and.
     &                ((ft(jnab).le.-ftsli).or.
     &                (ft(jnab).ge.ftsli))) then
				     wpl=sft*DABS(ft(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c rolling contacts

                  if ((nmatc(k).eq.'M_rol').and.
     &                ((fs(jnab).le.-fsrol).or.
     &                (fs(jnab).ge.fsrol))) then
                     wpl=sfs*DABS(fs(jnab))
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c contact network

c                  if (nmatc(k).eq.'net') then
c                     wpl=snet
c                     call polydef(k,rxi,ryi,rxj,ryj,
c     &                            nxij,nyij,wpl,xpl,ypl,zpl)
c                  endif

c not mobile contact network

                  if ((nmatc(k).eq.'net').and.
     &                (p_nc(i).ne.0).and.
     &                (p_nc(j).ne.0)) then
                     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c strong and weak contact networks

                  if ((nmatc(k).eq.'net_st').and.
     &                (fn(jnab).ge.fnmean)) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if ((nmatc(k).eq.'net_we').and.
     &                (fn(jnab).lt.fnmean)) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c tensile and compresive contact networks

                  if ((nmatc(k).eq.'net_te').and.
     &                (fn(jnab).lt.0.d0)) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if ((nmatc(k).eq.'net_co').and.
     &                (fn(jnab).ge.0.d0)) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c initial and residual contact networks

                  if ((nmatc(k).eq.'net_in').and.
     &                (js.eq.1)) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if ((nmatc(k).eq.'net_re').and.
     &                (js.eq.2)) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

c sliding and rolling contact networks

                  if ((nmatc(k).eq.'net_sli').and.
     &                ((ft(jnab).le.-ftsli).or.
     &                (ft(jnab).ge.ftsli))) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif

                  if ((nmatc(k).eq.'net_rol').and.
     &                ((fs(jnab).le.-fsrol).or.
     &                (fs(jnab).ge.fsrol))) then
				     wpl=snet
                     call polydef(k,rxi,ryi,rxj,ryj,
     &                            nxij,nyij,wpl,xpl,ypl,zpl)
                  endif
				  
               enddo
c			   endif
		    enddo
         endif
      enddo        

      return
      end

c========1=========2=========3=========4=========5=========6=========7==
c     SR polydef
c-----Calculate coordinates and write polygons on corresponding files 

      subroutine polydef(k,rxi,ryi,rxj,ryj,nxij,nyij,wpl,xpl,ypl,zpl)
      implicit none
      include '../VAR/gpglob'

c-----local variables

      integer k,l
      real*8 rxi,ryi,rxj,ryj
      real*8 nxij,nyij,wpl,rxij
      real*8 xpl(4),ypl(4),zpl(4)

c-----Contact forces between particles are displayed 

      if (wpl.gt.0.) then
           rxij = rxi - rxj
           if (speri.and.(DABS(rxij).gt.rxw2)) then
c           if (DABS(rxij).gt.rxw2) then
              if (rxij.gt.0.d0) then
                  rxi=rxi-rxw
              else
                  rxi=rxi+rxw
              endif
           endif 
        
          xpl(1) = rxi + wpl*nyij
          ypl(1) = ryi - wpl*nxij
          xpl(2) = rxi - wpl*nyij
          ypl(2) = ryi + wpl*nxij
          xpl(3) = rxj - wpl*nyij
          ypl(3) = ryj + wpl*nxij
          xpl(4) = rxj + wpl*nyij
          ypl(4) = ryj - wpl*nxij

          write(20+k,*) wmvpoly(k)

          write(20+k,100) (xpl(l),l=1,4)
          write(20+k,100) (ypl(l),l=1,4)
          write(20+k,100) (zpl(l),l=1,4)

      endif

 100  format(f16.8,3(1x,f16.8))

      return
      end

c========1=========2=========3=========4=========5=========6=========7==
c     SR pcnum
c-----Calculate number of contacts on a particle

      subroutine pcnum()
      implicit none
      include '../VAR/gpglob'

c-----local variables

      integer i,j
      integer jbeg,jend,jnab,worms
      real cdist

      do i=1,np
        p_nc(i)= 0.d0
      enddo

      do i = 1,np

        jbeg = cpoint(i)  
        jend = cpoint(i+1) - 1       

        if (jbeg.le.jend) then
          do jnab = jbeg,jend

            j = clist(jnab)
            p_nc(i)=p_nc(i)+1
            p_nc(j)=p_nc(j)+1

          enddo
        endif
      enddo

c      worms = 1

c      do while (worms.gt.0)

c        do i = 1,np

c          jbeg = cpoint(i)  
c          jend = cpoint(i+1) - 1       

c          if (jbeg.le.jend) then
c            do jnab = jbeg,jend

c              j = clist(jnab)

c              if (((p_nc(i).eq.1).or.(p_nc(j).eq.1)).and.
c     &           (p_nc(i).gt.0).and.
c     &           (p_nc(j).gt.0)) then
c                p_nc(i)=p_nc(i)-1
c                p_nc(j)=p_nc(j)-1
c              endif

c            enddo
c          endif
c        enddo

c        worms = 0

c        do i=1,np
c          if (p_nc(i).eq.1) then
c            worms = worms + 1
c          endif
c        enddo

c        write (*,*) 'the number of worms is= ',worms

c      enddo

      return 
      end 
	  
c========1=========2=========3=========4=========5=========6=========7==
c     SR pcnum2
c-----See if all the contacts of a particle are critical

      subroutine pcnum2()
      implicit none
      include '../VAR/gpglob'

c-----local variables

      integer i,j,js
      integer jbeg,jend,jnab
      integer igu,jgu,impr,jmpr
      real*8 rdi,rdj
      real*8 mudd,mu_i,mu_j,ftsli
      real*8 sldd,sl_i,sl_j,fsrol
      real*8 exldd,exl_i,exl_j,fnrup
	  logical sli,rol,rupten

      do i=1,np
        p_nc(i)= 0.d0
      enddo

      do i = 1,np

	    rdi = rd(i)

        jbeg = cpoint(i)  
        jend = cpoint(i+1) - 1       

        if (jbeg.le.jend) then
          do jnab = jbeg,jend

            j = clist(jnab)
			
			rdj = rd(j)
			
c choose the mechanical parameters that gouvern the behavior of
c the contact

            igu=idgd(i)
			jgu=idgd(j)

			if (csta(jnab)) then
c          initial behavior law
			   js = 1
			else 
c          residual behavior law
			   js = 2
			endif

			impr = gdmpr(igu,js,ks)
			jmpr = gdmpr(jgu,js,ks)

c     assign the mechanical parameters for particle i
			mu_i=mul(impr)
			if (cstr(impr).eq.1) then ! mechanical parametrers are real quantities
			   exl_i=exlr(impr)
			   sl_i=sllr(impr)
			elseif (cstr(impr).eq.2) then ! mechanical parametrers are defined as 
c                                   normalized quantities
			   exl_i=exln(impr)*(rdi+rdj)
			   sl_i=slln(impr)*(rdi+rdj)
            endif

c     assign the mechanical parameters for particle j
			mu_j=mul(jmpr)
			if (cstr(jmpr).eq.1) then ! mechanical parametrers are real quantities
			   exl_j=exlr(jmpr)
			   sl_j=sllr(jmpr)
			elseif (cstr(jmpr).eq.2) then ! mechanical parametrers are defined as 
c                                   normalized quantities
			   exl_j=exln(jmpr)*(rdi+rdj)
			   sl_j=slln(jmpr)*(rdi+rdj)
			endif

			if (igu.eq.jgu) then ! both disks belong to the same geological
c                             domain
			   mudd=mu_i
			   exldd=exl_i
			   sldd=sl_i
            else
			   if (ilayc(ks)) then ! inter-layer cohesion is present
			      if (ilaymp(ks).eq.1) then
				     exldd=dmin1(exl_i,exl_j)
				  elseif (ilaymp(ks).eq.2) then
				     exldd=(exl_i+exl_j)/2.d0
				  elseif (ilaymp(ks).eq.3) then
				     exldd=dmax1(exl_i,exl_j)
				  endif
			   elseif (.not.ilayc(ks)) then ! inter-layer cohesion is not present
			      exldd=0.d0
			   endif         
			   if (ilayf(ks)) then ! inter-layer friction is present
			      if (ilaymp(ks).eq.1) then
				     mudd=dmin1(mu_i,mu_j)
					 sldd=dmin1(sl_i,sl_j)
				  elseif (ilaymp(ks).eq.2) then
				     mudd=(mu_i+mu_j)/2.d0
				     sldd=(sl_i+sl_j)/2.d0
				  elseif (ilaymp(ks).eq.3) then
				     mudd=dmax1(mu_i,mu_j)
				     sldd=dmax1(sl_i,sl_j)
			      endif
			   elseif (.not.ilayc(ks)) then ! inter-layer friction is not present
			      mudd=0.d0
				  sldd=0.d0
			   endif
			endif

			fnrup=(0.9999d0)*exldd
			ftsli=(0.9999d0)*mudd*(exldd + fn(jnab))
			fsrol=(0.9999d0)*sldd*(exldd + fn(jnab))
			
c      if the contact is locked (i.e., not critic) then increment the number
c      of contacts of the two particles
			
			sli=.FALSE.
			rol=.FALSE.
			rupten=.FALSE.
			
            if (((ft(jnab).le.-ftsli).or.
     &         (ft(jnab).ge.ftsli))) then
               sli=.TRUE.
			endif
			
            if (((fs(jnab).le.-fsrol).or.
     &         (fs(jnab).ge.fsrol))) then
			   rol=.TRUE.
			endif
			   
            if (fn(jnab).le.(-fnrup)) then
			   rupten=.TRUE.
			endif

c            write(*,*) 'sli:',sli,' rol:',rol,' rupten:',rupten

            if ((.not.sli).and.(.not.rol).and.(.not.rupten)) then

               p_nc(i)=p_nc(i)+1
               p_nc(j)=p_nc(j)+1

            endif

          enddo
        endif
      enddo

      return 
      end 

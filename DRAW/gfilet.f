********************************

c
c     SR gfilet
c     create a GMV file

      subroutine gfilet()
      implicit none    

      include '../VAR/gpglob'

      character*30 f_nam  
      character*30 f_gmv  
      integer i

c     Create GMV output file

      f_gmv='./display/post.gmv.'//namhfr(kfr)

      open(1,file=f_gmv,status='unknown')

      write(1,15) 'gmvinput ascii'

 15   format(T1,A14)

c     Write file with nodes

      f_nam='xynode'
      call crunch(f_nam)

c     write cells information

      write (1,*) 'cells    0'

c     write materials

      write (1,*) 'material  ',20+npoly,'   1'

      write (1,*) '0c' 
      write (1,*) '1c'
      write (1,*) '2c' 
      write (1,*) '3c'
      write (1,*) '4c' 
      write (1,*) '5c'
      write (1,*) '6c' 
      write (1,*) 'disk08'   
      write (1,*) 'disk09' 
      write (1,*) 'disk10' 
      write (1,*) 'disk11' 
      write (1,*) 'disk12'
      write (1,*) 'disk13' 
      write (1,*) 'disk14'
      write (1,*) 'disk15' 
      write (1,*) 'disk16'
      write (1,*) 'disk17' 
      write (1,*) 'disk18'   
      write (1,*) 'disk19' 
      write (1,*) 'disk20'

      do i=1,npoly
        write (1,*) nmatc(i)   ! material number 20+i
      enddo

c     Write file with material of nodes

      f_nam='manode'
      call crunch(f_nam)

c     Write file with surfaces of the disks

      f_nam='sunode'
      call crunch(f_nam)

c     Write file with surface variables of the disks

      f_nam='survar'
      call crunch(f_nam)

c     write polygons indicating forces in the contacts

      write(1,*) 'polygons'

      do i=1,npoly     
c       contact parameters
        f_nam=nmatc(i)
        call crunch(f_nam)
      enddo

      write(1,*) 'endpoly'

c     end line in file GMV

      write(1,16) 'endgmv'

 16   format(T1,A6)
      close(1)

      return
      end
       
c************************************
      subroutine crunch(f_nam)

      implicit none    

      character*30 f_nam
      character*80 chardat

      open(2,file='./display/'//f_nam,status='unknown')
c     read data from f_dat file
 10   read(2,30,END=20) chardat
c     write data in gmv file
      write(1,30) chardat
      goto 10

 20   continue

 30   format(A80)

      return

      end



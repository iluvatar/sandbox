!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR contacts_sli_rol()
! Here, we find the status (sliding, rolling, non-sliding, non-rolling) 
! for each contact
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine contacts_sli_rol()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: con !counter for the contacts
   integer :: fra !counter for the frames
   integer :: di, dj !id of the two particles in contact
   double precision :: rdij !summ of the radii of the two particles
   double precision :: thresh_s, thresh_t, thresh_r ! sliding, tension, and rolling thresholds

   do fra=1,nfr ! for all the frames

      do con=1,nc(fra) ! for all the contacts
	     		 
	     if (cstatus(con,fra)) then ! initial contact
	  
            di=d_id_i(con,fra)
            dj=d_id_j(con,fra)

            rdij = dr(di) + dr(dj)

	        thresh_s = (1-tol_s) * mu_s * ((adh*rdij) + cfn(con,fra))
		    thresh_t = -(1-tol_t) * adh * rdij
		    thresh_r = (1-tol_r) * mu_r * rdij * (adh*rdij + cfn(con,fra))

! find out if the contact is at the sliding threshold

            if ((cft(con,fra) <= -thresh_s) .or. (cft(con,fra) >= thresh_s)) then
               sliding(con,fra) = .TRUE.
            else
               sliding(con,fra) = .FALSE.
            endif

! find out if the contact is at the tension threshold

            if (cfn(con,fra) > thresh_t) then
               rup_ten(con,fra) = .FALSE.
            else
               rup_ten(con,fra) = .TRUE.
            endif

! find out if the contact is at the rolling threshold

            if ((cfs(con,fra) <= -thresh_r) .or. (cfs(con,fra) >= thresh_r)) then
               rolling(con,fra) = .TRUE.
            else
               rolling(con,fra) = .FALSE.
            endif

	     elseif (.not. cstatus(con,fra)) then ! residual contact
	  
            di=d_id_i(con,fra)
            dj=d_id_j(con,fra)

            rdij = dr(di) + dr(dj)

	        thresh_s = (1-tol_s) * mu_s * cfn(con,fra)
		    thresh_t = 0
		    thresh_r = (1-tol_r) * mu_r * rdij * cfn(con,fra)

! find out if the contact is at the sliding threshold

            if ((cft(con,fra) <= -thresh_s) .or. (cft(con,fra) >= thresh_s)) then
               sliding(con,fra) = .TRUE.
            else
               sliding(con,fra) = .FALSE.
            endif

! find out if the contact is at the tension threshold

			rup_ten(con,fra) = .FALSE.

! find out if the contact is at the rolling threshold

            if ((cfs(con,fra) <= -thresh_r) .or. (cfs(con,fra) >= thresh_r)) then
               rolling(con,fra) = .TRUE.
            else
               rolling(con,fra) = .FALSE.
            endif
			
         else
		 
		    write(*,*) 'atención, contacto ni inicial ni residual, raro !!!'
		 
		 endif

      enddo

   enddo

   return
end subroutine contacts_sli_rol

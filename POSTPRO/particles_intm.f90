!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR particles_intm()
! Here, we calculate the internal moment of each particle.
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine particles_intm()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: con ! counter for the contacts
   integer :: par ! counter for the particles
   integer :: fra ! counter for the frames
   integer :: di, dj ! disks identifiers
   double precision :: m_inertia

   do fra=1,nfr ! for all the frames

! Initialize the components of the tensor of internal moment. We will initialize them with the second
! part of the expression used to calculate the tensor of internal moment (i.e. the one that takes into
! account the moment of intertia of the particle)

      do par=1, 4 !for all the walls

         d_intm_xx(par,fra) = 0
         d_intm_xy(par,fra) = 0
         d_intm_yx(par,fra) = 0
         d_intm_yy(par,fra) = 0

      enddo

      do par=5, np ! for all the disks

         m_inertia = (pi * rho * dr(par)**4)/2

         d_intm_xx(par,fra) = (-1) * (m_inertia * (dvrot(par,fra))**2)/2
         d_intm_xy(par,fra) = 0
         d_intm_yx(par,fra) = 0
         d_intm_yy(par,fra) = (-1) * (m_inertia * (dvrot(par,fra))**2)/2

      enddo

! calculate the contribution of the contact forces to the tensor of internal moment of the particles

      do con=1,nc(fra) ! for all the contacts
         if (c_valid(con,fra)) then

! Assign the particles identifiers for the two particles

            di=d_id_i(con,fra)
            dj=d_id_j(con,fra)

! partilce i

! these expressions where derived using the definition presented in the article of L. Staron, F. Radjai,
! and J.P. Vilotte "Multi-scale analysis of the stress state in a granular slope in transition to
! failure"

            d_intm_xx(di,fra) = d_intm_xx(di,fra) &
               & + (dr(di) * cfn(con,fra) * rxij_u(con,fra)**2) &
               & + (dr(di) * cft(con,fra) * rxij_u(con,fra) * ryij_u(con,fra))

            d_intm_xy(di,fra) = d_intm_xy(di,fra) &
               & + (dr(di) * cfn(con,fra) * rxij_u(con,fra) * ryij_u(con,fra)) &
               & - (dr(di) * cft(con,fra) * rxij_u(con,fra)**2)

            d_intm_yx(di,fra) = d_intm_yx(di,fra) &
               & + (dr(di) * cfn(con,fra) * rxij_u(con,fra) * ryij_u(con,fra)) &
               & + (dr(di) * cft(con,fra) * ryij_u(con,fra)**2)

            d_intm_yy(di,fra) = d_intm_yy(di,fra) &
               & + (dr(di) * cfn(con,fra) * ryij_u(con,fra)**2) &
               & - (dr(di) * cft(con,fra) * rxij_u(con,fra) * ryij_u(con,fra))

! partilce j

            d_intm_xx(dj,fra) = d_intm_xx(dj,fra) &
               & + (dr(dj) * cfn(con,fra) * rxij_u(con,fra)**2) &
               & + (dr(dj) * cft(con,fra) * rxij_u(con,fra) * ryij_u(con,fra))

            d_intm_xy(dj,fra) = d_intm_xy(dj,fra) &
               & + (dr(dj) * cfn(con,fra) * rxij_u(con,fra) * ryij_u(con,fra)) &
               & - (dr(dj) * cft(con,fra) * rxij_u(con,fra)**2)

            d_intm_yx(dj,fra) = d_intm_yx(dj,fra) &
               & + (dr(dj) * cfn(con,fra) * rxij_u(con,fra) * ryij_u(con,fra)) &
               & + (dr(dj) * cft(con,fra) * ryij_u(con,fra)**2)

            d_intm_yy(dj,fra) = d_intm_yy(dj,fra) &
               & + (dr(dj) * cfn(con,fra) * ryij_u(con,fra)**2) &
               & - (dr(dj) * cft(con,fra) * rxij_u(con,fra) * ryij_u(con,fra))

         endif
      enddo

   enddo

   return
end subroutine particles_intm


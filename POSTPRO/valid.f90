!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR valid()
! Here, we decide if a contact (or a particle) is valid or not
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine valid()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: con !counter for the contacts
   integer :: par !counter for the particles
   integer :: fra !counter for the frames

!   do fra=1,nfr ! for all the frames
!      ncfn0 = 0
!      do con=1,nc(fra) ! for all the contacts between disks (not with the walls)
!         if (cfn(con,fra) == 0.) then
!            write(*,*) 'cfn(con,fra)=',cfn(con,fra)
!            read(*,*)
!             ncfn0 = ncfn0 + 1
!         endif
!      enddo
!      write(*,*) 'ncfn0=',ncfn0,' nc(fra)=',nc(fra),' ncfn0/nc(fra)=',DBLE(ncfn0)/DBLE(nc(fra))
!   enddo
!   read(*,*)

! decide if a contact is valid or not

   do fra=1,nfr ! for all the frames

      nc_valid(fra) = 0
      nd_valid(fra) = 0

      do con=1,nc(fra) ! for all the contacts between disks (not with the walls)

!         if ((crx(con,fra) >= bx1(1,fra)) &
!            & .and. (crx(con,fra) <= bx2(1,fra)) &
!            & .and. (cry(con,fra) >= by1(1,fra)) &
!            & .and. (cry(con,fra) <= by2(1,fra)) &
!			& .and. (d_id_i(con,fra) > 4)) then

         if ((crx(con,fra) >= bx1(1,fra)) &
            & .and. (crx(con,fra) <= bx2(1,fra)) &
            & .and. (cry(con,fra) >= by1(1,fra)) &
            & .and. (cry(con,fra) <= by2(1,fra)) & !the contact belongs to the box
            & .and. ((dnc_eff(d_id_i(con,fra),fra)) >= 2) &
            & .and. ((dnc_eff(d_id_j(con,fra),fra)) >= 2)) then !the contact does not come
! from a floating particle

!         if ((cfn(con,fra) /= 0.) .and. (crx(con,fra) >= bx1(1,fra)) &
!            & .and. (crx(con,fra) <= bx2(1,fra)) &
!            & .and. (cry(con,fra) >= by1(1,fra)) &
!            & .and. (cry(con,fra) <= by2(1,fra)) & !the contact belongs to the box
!            & .and. ((dnc(d_id_i(con,fra),fra)) >= 1) &
!            & .and. ((dnc(d_id_j(con,fra),fra)) >= 1)) then !the contact does not come
! from a floating particle

            c_valid(con,fra) = .TRUE.
            nc_valid(fra) = nc_valid(fra) + 1

!            if (cfn(con,fra) == 0.) then
!               write(*,*) 'cfn(con,fra)',cfn(con,fra)
!            endif

         else

            c_valid(con,fra) = .FALSE.

         endif

      enddo

! decide if a particle is valid or not

      do par=5, np ! for all the particles

!         if ((drx(par,fra) >= bx1(1,fra)) &
!            & .and. (drx(par,fra) <= bx2(1,fra)) &
!            & .and. (dry(par,fra) >= by1(1,fra)) &
!            & .and. (dry(par,fra) <= by2(1,fra))) then

         if ((drx(par,fra) >= bx1(1,fra)) &
            & .and. (drx(par,fra) <= bx2(1,fra)) &
            & .and. (dry(par,fra) >= by1(1,fra)) &
            & .and. (dry(par,fra) <= by2(1,fra)) &
            & .and. (dnc_eff(par,fra) >= 2)) then !the particle belongs to the box and it
! is not a floating particle

!         if ((drx(par,fra) >= bx1(1,fra)) &
!            & .and. (drx(par,fra) <= bx2(1,fra)) &
!            & .and. (dry(par,fra) >= by1(1,fra)) &
!            & .and. (dry(par,fra) <= by2(1,fra)) &
!            & .and. (dnc(par,fra) >= 2)) then !the particle belongs to the box and it
! is not a floating particle

            d_valid(par,fra) = .TRUE.
            nd_valid(fra) = nd_valid(fra) + 1

         else

            d_valid(par,fra) = .FALSE.

         endif

      enddo

   enddo
   
! write output files

   open(1,file='./postpro/bnc_valid_all',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,nc_valid(fra),nc(fra)
      enddo
   close(1)

   open(1,file='./postpro/bnd_valid_all',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,nd_valid(fra),nd
      enddo
   close(1)

   return
end subroutine valid

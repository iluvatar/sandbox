!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_perc_par()
! Here, we calculate the percentages of XXX particles in each box
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_perc_par()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: par ! counter for the particles
   integer :: fra ! counter for the frames
   integer, dimension (nfr_max) :: nd_float ! counter for the frames
   double precision, dimension (nb_max, nfr_max) :: b_d_volume
   double precision, dimension (nfr_max) :: dr_mean_valid, dr_mean_float
   double precision :: mean, ecart_type

   do fra=1, nfr ! for all the frames

      b_d_volume(1,fra) = 0.
      nd_float(fra) = 0
      dr_mean_valid(fra) = 0.
      dr_mean_float(fra) = 0.

      do par=5, np ! for all the particles

!         if (d_valid(par,fra)) then

         if ((drx(par,fra) >= bx1(1,fra)) &
            & .and. (drx(par,fra) <= bx2(1,fra)) &
            & .and. (dry(par,fra) >= by1(1,fra)) &
            & .and. (dry(par,fra) <= by2(1,fra))) then !the particle belongs to the box
		 
            b_d_volume(1,fra) = b_d_volume(1,fra) + pi*(dr(par)**2.)
!			write(*,*) 'particle=',par,', radius=',dr(par),', vol=',pi*(dr(par)**2)
!			write(*,*) 'b_d_volume(1,fra)=',b_d_volume(1,fra)
!            dr_mean_valid(fra) = dr_mean_valid(fra) + dr(par)

         endif

!         if ((dnc(par,fra) == 0) .or. (dnc(par,fra) == 1)) then
         if (dnc_eff(par,fra) < 2) then
            nd_float(fra) = nd_float(fra) + 1
!            dr_mean_float(fra) = dr_mean_float(fra) + dr(par)
         endif

         if (d_valid(par,fra)) then
            dr_mean_valid(fra) = dr_mean_valid(fra) + dr(par)
         else
            dr_mean_float(fra) = dr_mean_float(fra) + dr(par)
         endif

      enddo

      dr_mean_valid(fra) = dr_mean_valid(fra)/DBLE(nd_valid(fra))
      dr_mean_float(fra) = dr_mean_float(fra)/DBLE(nd_float(fra))
!      write(*,*) 'dr_mean_valid(fra)=',dr_mean_valid(fra),' dr_mean_float(fra)=',dr_mean_float(fra)

   enddo

!   read(*,*)

! Write output files

   open(1,file='./postpro/perc_valid',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
	     write(1,*) fra,DBLE(nd_valid(fra))/DBLE(nd),(DBLE(nd - nd_float(fra)) / DBLE(nd))
      enddo
   close(1)

   open(1,file='./postpro/coordination',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,(DBLE(2.*nc(fra)) / DBLE(nd))
      enddo
   close(1)

   open(1,file='./postpro/coordinationBackbone',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,(DBLE(2.*nc_valid(fra)) / DBLE(nd_valid(fra)))
      enddo
   close(1)

   open(1,file='./postpro/compacity',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,(b_d_volume(1,fra) / (bx_dim(fra) * by_dim(fra)))
      enddo
   close(1)

   open(1,file='./postpro/void_ratio',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,(((bx_dim(fra) * by_dim(fra)) - b_d_volume(1,fra)) /  b_d_volume(1,fra))
      enddo
   close(1)

   open(1,file='./postpro/dr_mean_valid',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
      write(1,*) fra,dr_mean_valid(fra)
      enddo
   close(1)

   open(1,file='./postpro/dr_mean_float',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,dr_mean_float(fra)
      enddo
   close(1)

   return
end subroutine boxes_perc_par


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR contacts_texture()

! Here, we calculate the contribution of each contact to the texture tensor.

! Note that the quantities calculated here are not the components of the texture tensor, 
! at least not yet, since they must be normalized by the number of contacts on the considered volume.
! This operation will be done later in the subroutine calculate_boxes.f90
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

subroutine contacts_texture()
   implicit none 

! global variables
   include './glob_postpro'

! local variables

   integer :: con !counter for the contacts
   integer :: fra !counter for the frames

   do fra=1,nfr ! for all the frames
      do con=1,nc(fra) ! for all the contacts

         c_texture_xx(con,fra) = rxij_u(con,fra)**2
         c_texture_xy(con,fra) = rxij_u(con,fra) * ryij_u(con,fra)
         c_texture_yy(con,fra) = ryij_u(con,fra)**2

      enddo

   enddo

   return
end subroutine contacts_texture

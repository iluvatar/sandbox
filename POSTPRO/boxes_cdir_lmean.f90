!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_cdir_fnmean()
! Here, we calculate probability distribution of intercenter lengths
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_cdir_lmean()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: nalfa_id ! counter for the angular intervals
   integer :: fra ! counter for the frames
   integer, dimension (nfr_max,nalfa_max):: nc_alfa
   double precision :: delta_nalfa ! angular thickness of an angular interval
   double precision :: angle, x, y !angle, length and coordinates for drawing the polar
   double precision, dimension (nalfa_max) :: alfa_lmean_mean
   character*40 :: n_file1 !name of the file to write

! 1) Calculate the size of the angular interval (for the probability density function of the contact
! directions)
! initialize the number of contacts in each angular interval

   delta_nalfa = pi / DBLE(nalfa)

   do fra=1, nfr ! for all the frames

      do nalfa_id=1, nalfa

         alfa_lmean(fra,nalfa_id) = 0.
         nc_alfa(fra,nalfa_id) = 0

      enddo

   enddo

   do fra=1, nfr ! for all the frames

      do con=1, nc(fra) ! for all the contacts

! 1) find if the contact is valid or not

         if (c_valid(con,fra)) then

! 2) Increment the intercenter length and the number of contacts

            nalfa_id = floor(cdir(con,fra)/delta_nalfa) + 1
            nc_alfa(fra,nalfa_id) = nc_alfa(fra,nalfa_id) + 1
            alfa_lmean(fra,nalfa_id) = alfa_lmean(fra,nalfa_id) + &
               cl(con,fra)

         endif

      enddo

   enddo


! 3) Calculate the mean intercenter length in an interval

   do fra = 1, nfr

      do nalfa_id = 1, nalfa

         if (nc_alfa(fra,nalfa_id) == 0) then
            alfa_lmean(fra,nalfa_id) = 0
         else
            alfa_lmean(fra,nalfa_id) = alfa_lmean(fra,nalfa_id) / &
               & (DBLE(nc_alfa(fra,nalfa_id)))
         endif

!   write(*,*) 'alfa_fnmean(fra,nalfa_id)=',alfa_fnmean(fra,nalfa_id)
!   read(*,*)

      enddo

   enddo

! 5) write the mean distribution of contact directions

   do fra = 1, nfr

      n_file1='./postpro/cdir_dist_lmean'//fr_id(fra)
      open(1,file=n_file1,form='formatted',status='unknown')

         do nalfa_id = 1, nalfa
            angle = DBLE(nalfa_id*180)/DBLE(nalfa) - 180./DBLE(2*nalfa)
            x = alfa_lmean(fra,nalfa_id) * cos(angle*pi/180)
            y = alfa_lmean(fra,nalfa_id) * sin(angle*pi/180)
            write(1,*) x, y, angle, alfa_lmean(fra,nalfa_id)
         enddo

         do nalfa_id = 1, nalfa
            angle = 180. + DBLE(nalfa_id*180/nalfa) - 180./DBLE(2*nalfa)
            x = alfa_lmean(fra,nalfa_id) * cos(angle*pi/180)
            y = alfa_lmean(fra,nalfa_id) * sin(angle*pi/180)
            write(1,*) x, y, angle, alfa_lmean(fra,nalfa_id)
         enddo
		 
         angle = DBLE(1*180/nalfa) - 180./DBLE(2*nalfa)
         x = alfa_lmean(fra,1) * cos(angle*pi/180)
         y = alfa_lmean(fra,1) * sin(angle*pi/180)
         write(1,*) x, y, angle, alfa_lmean(fra,1)

	  close(1)
	  
   enddo

   return
end subroutine boxes_cdir_lmean


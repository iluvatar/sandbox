!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR r_postpro_parameters()
! Here, we read the information contained in the info_i file.
! This information is necessary as it specifies the execution
! parameters.
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

subroutine r_postpro_parameters()
   implicit none

! global variables
   include './glob_postpro'

! local variables
   integer :: fra ! counter for the frames

   open(1,file='./input/postpro_parameters',status='unknown')

   read(1,*)     !introductory line 
   read(1,*)     
   read(1,*) nfr_total     !total number of frames saved during execution
   read(1,*) par_name     !name of the file containing the particles characteristics
   read(1,*) pos_name     !name of the file containing the particles positions
   read(1,*) con_name     !name of the file containing the contacts information
   read(1,*)   
   read(1,*) rho     !2600     density of the particles in kg/m3
   read(1,*)   
   read(1,*) mu_s     !0.1     sliding friction
   read(1,*) adh     !0.     adhesion
   read(1,*) mu_r     !0.1     rolling friction
   read(1,*)   
   read(1,*) tol_s     !0.01     tolerance upon sliding threshold
   read(1,*) tol_t     !0.01     tolerance upon tension threshold
   read(1,*) tol_r     !0.01     tolerance upon rolling threshold
   read(1,*)   
   read(1,*) nalfa     !18     number of increments for the contacts direction distribution
   read(1,*)   
   read(1,*) b_type     !0     type of window (0=authomatic, 1=specified)
   read(1,*)     !specified window parameters
   read(1,*) spe_x1     !0     x initial
   read(1,*) spe_x2     !20     x final
   read(1,*) spe_y1     !0     Y initial
   read(1,*) spe_y2     !20     y final
   read(1,*)    
   read(1,*) nfr     !5     number of files

   !files identifiers
   do fra=1,nfr
      read(1,*) fr(fra),fr_id(fra)
   enddo

   close(1)

   write(*,*)
   write(*,*)'%%%%% Surface properties %%%%%'
   write(*,*)'sliding friction coefficient = ',mu_s
   write(*,*)'adhesion = ',adh
   write(*,*)'rolling friction coefficient = ',mu_r
   write(*,*)
   write(*,*)'%%%%% Tolerances %%%%%'
   write(*,*)'tolerance upon sliding threshold = ',tol_s
   write(*,*)'tolerance upon tension threshold = ',tol_t
   write(*,*)'tolerance upon rolling threshold = ',tol_r
   write(*,*)

   return
end subroutine r_postpro_parameters

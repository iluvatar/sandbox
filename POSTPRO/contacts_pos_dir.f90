!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR contacts_pos_dir()
! Here, we calculate the unitary vectors, the poral distances, the positions,
! and the directions of the contacts.
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine contacts_pos_dir()
   implicit none 

! global variables
   include './glob_postpro'

! local variables

   integer :: par !counter for the particles
   integer :: con !counter for the contacts
   integer :: fra !counter for the frames
   integer :: jbeg, jend !counters for the contact list
   integer :: di, dj !id of the two particles in contact
   double precision :: drx12, dry12 !vector from particle 1 to
! particle 2 and unitari vector with the same direction
   double precision :: ndr12 !norm of the same vector

! find the particles 1 and 2 associated to each contact 'con':

! 1) Identifie the id of the two particles for each contact

   do fra=1,nfr
      do par=1,(np-1)
         jbeg=cpoint(par,fra)
         jend=cpoint(par+1,fra)
         if (jbeg <= jend) then
            do con=jbeg,(jend-1)
               d_id_i(con,fra)=par
               d_id_j(con,fra)=clist(con,fra)
            enddo
         endif
      enddo
   enddo

! For all the contacts we must calculate:
!   a) the unitary vector
!   b) the poral distance
!   c) the contact position
!   d) the contact direction

   do fra=1,nfr ! for all the frames
      do con=1,nc(fra) ! for all the contacts

! Assign the particles identifiers for the two particles

         di=d_id_i(con,fra)
         dj=d_id_j(con,fra)

! 2.1) Contact between a disk and the down wall

         if (di == 1) then

! 2.1.1) unitary vector r12_u oriented from the center of the particle #1 to the center of the particle #2

            rxij_u(con,fra)=0
            ryij_u(con,fra)=1

! 2.1.2) poral distance

            cpore(con,fra)=(dry(dj,fra)-dry(1,fra)-dr(dj))/dr(dj)

! 2.1.3) contact position

            if (cpore(con,fra) >= 0) then
               crx(con,fra)=drx(dj,fra)
               cry(con,fra)=cpore(con,fra)/2
            else ! particles interpenetrate
               crx(con,fra)=drx(dj,fra)
               cry(con,fra)=0
            endif

! 2.1.4) contact direction

            cdir(con,fra)=pi/2

! 2.2) Contact between a disk and the up wall:

         else if (di == 3) then !contact with the up wall

! 2.2.1) unitary vector r12_u oriented from the center of the particle #1 to the center of the particle #2

            rxij_u(con,fra)=0
            ryij_u(con,fra)=-1

! 2.2.2) poral distance

            cpore(con,fra)=(dry(3,fra)-dry(dj,fra)-dr(dj))/dr(dj)

! 2.2.3) contact position

            if (cpore(con,fra) >= 0) then
               crx(con,fra)=drx(dj,fra)
               cry(con,fra)=dry(3,fra)-cpore(con,fra)/2
            else ! particles interpenetrate
               crx(con,fra)=drx(dj,fra)
               cry(con,fra)=dry(3,fra)
            endif

! 2.2.4) contact direction

            cdir(con,fra)=pi/2

! 2.3) Contact between two disks. Here there are two posibilities:
!   a) normal contacts
!   b) contacts occurring at the semi-periodic boundaries

         else ! disk-disk contact

! 2.3.1) unitary vector r12_u oriented from the center of the particle #1 to the center of the particle #2

            drx12=drx(dj,fra)-drx(di,fra)
            dry12=dry(dj,fra)-dry(di,fra)

! We must calculate a special vector for the contacts ocurring at the semiperiodic boundaries

            if (abs(drx12) > (drx(4,fra)/2)) then
               if (drx12 > 0) then
                  drx12=drx12-(drx(4,fra)-drx(2,fra))
               else
                  drx12=drx12+(drx(4,fra)-drx(2,fra))
               endif
            endif

! The norm of the vector dr12 is

            ndr12=(drx12**2 + dry12**2)**0.5
            cl(con,fra)=ndr12
            rxij_u(con,fra)=drx12/ndr12
            ryij_u(con,fra)=dry12/ndr12

! 2.3.2) poral distance

            cpore(con,fra)=2*(ndr12-dr(di)-dr(dj))/(dr(di)+dr(dj))

! 2.3.3) contact position

            if (cpore(con,fra) >=0) then
               crx(con,fra)=drx(di,fra)+(dr(di)+cpore(con,fra)/2)*rxij_u(con,fra)
               cry(con,fra)=dry(di,fra)+(dr(di)+cpore(con,fra)/2)*ryij_u(con,fra)
            else
               crx(con,fra)=drx(di,fra)+(dr(di)-cpore(con,fra)/2)*rxij_u(con,fra)
               cry(con,fra)=dry(di,fra)+(dr(di)-cpore(con,fra)/2)*ryij_u(con,fra)
            endif

! we must assign a special possition to the contacts occurring outside the box (this is due to the
! semiperiodic boundaries)

            if (crx(con,fra) > drx(4,fra)) then
               crx(con,fra) = crx(con,fra) - drx(4,fra)
            else if (crx(con,fra) < 0) then
               crx(con,fra) = crx(con,fra) + drx(4,fra)
            endif

! 2.3.4) contact direction

            cdir(con,fra)=atan(ryij_u(con,fra)/rxij_u(con,fra))

            if (cdir(con,fra) < 0) then
               cdir(con,fra)=pi+cdir(con,fra)
            endif

         endif

      enddo

   enddo

   return
end subroutine contacts_pos_dir


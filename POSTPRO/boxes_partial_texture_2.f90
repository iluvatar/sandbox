!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_cdir_dist_small()
! Here, we calculate probability distribution of contact directions
! for all the contacts in the small forces
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_partial_texture_2()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: i
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: fra ! counter for the frames
   integer :: nalfa_id ! counter for the angular intervals
   integer, dimension (nfr_max) :: bnc_small ! number of contacts in the box with a small force
   integer, dimension (nfr_max,nalfa_max):: nc_alfa
   character*40 :: n_file1, n_file2, n_file3 !name of the file to write
   character*3 :: id ! identifier
   double precision :: delta_nalfa ! angular thickness of an angular interval
   double precision :: psi
   double precision :: suma
   double precision :: angle, x, y !angle, length and coordinates for drawing the polar graphs
   double precision, dimension (nfr_max,nalfa_max) ::p_nc_alfa_small
   double precision, dimension (nalfa_max) :: p_nc_alfa_mean
   double precision :: partial_anis, partial_dir, threshold, lamda_1, lamda_2
   double precision :: partial_anis_bk, zero

! 1) Calculate the size of the angular interval (for the probability density function of the contact
! directions)
! initialize the number of contacts in each angular interval

   n_file1='./dir_dist/partial_anis'
   open(1,file=n_file1,form='formatted',status='unknown')

   n_file2='./dir_dist/partial_dir'
   open(2,file=n_file2,form='formatted',status='unknown')

   n_file3='./dir_dist/zero'
   open(4,file=n_file3,form='formatted',status='unknown')

!   zero = 0.
   partial_anis_bk = 0.

   do i=1, 100

   delta_nalfa = pi / DBLE(nalfa)
   threshold = DBLE(i*10)/DBLE(100)

   write(*,*) 'control 1 ... OK'

      do fra=1, nfr ! for all the frames

         do nalfa_id=1, nalfa
            nc_alfa(fra,nalfa_id) = 0
         enddo
         bnc_small(fra) = 0

      enddo

      do fra=1, nfr ! for all the frames

         do con=1, nc(fra) ! for all the contacts

            psi=cfn(con,fra)/fn_mean(fra)

! 1) find if the contact belons to the box or not

            if ((c_valid(con,fra)) .and. (psi < threshold)) then

! 2) Calculate the number of contacts in each angular interval

               nalfa_id = floor(cdir(con,fra)/delta_nalfa) + 1
               nc_alfa(fra,nalfa_id) = nc_alfa(fra,nalfa_id) + 1
               bnc_small(fra) = bnc_small(fra) + 1

            endif

         enddo

      enddo

      write(*,*) 'control 2 ... OK'

! 3) Calculate the probablility of occurrence of a contact in a given direction

      do fra = 1, nfr

!         suma = 0

         do nalfa_id = 1, nalfa

            p_nc_alfa_small(fra,nalfa_id) = DBLE(nc_alfa(fra,nalfa_id)) / &
               & (2*delta_nalfa*DBLE(bnc_small(fra)))

!            suma = suma + p_nc_alfa_small(fra,nalfa_id)*delta_nalfa

! here we must normalize by 2 * real(bnc(1,fra)) for the summ of all probabilities to be 1

         enddo

!         write(*,*)'fra ',fra,' suma ',suma

      enddo

! 4) calculate and write the mean distribution of contact directions

      do nalfa_id = 1, nalfa

         p_nc_alfa_mean(nalfa_id) = 0

         do fra = 101, nfr

            p_nc_alfa_mean(nalfa_id) = p_nc_alfa_mean(nalfa_id) + &
               & p_nc_alfa_small(fra,nalfa_id)/DBLE(nfr-100)

         enddo

      enddo

      write(*,*) 'control 3 ... OK'

! 5) write the mean distribution of contact directions

      lamda_1 = p_nc_alfa_mean(27)
      lamda_2 = p_nc_alfa_mean(9)

!      write(*,*) 'fra ',fra
!      write(*,*) 'lamda_1 ',lamda_1
!      write(*,*) 'lamda_2 ',lamda_2

      partial_anis = (lamda_1 - lamda_2)/(lamda_1 + lamda_2)

      if (partial_anis <= 0.) then
         partial_dir = pi/4
      else
         partial_dir = 3*pi/4
      endif

      write(1,*) DBLE(i*10)/DBLE(100),partial_anis
      write(2,*) DBLE(i*10)/DBLE(100),partial_dir

      if ((partial_anis_bk <= 0.).and.(partial_anis > 0.)) then
         zero = DBLE(i*10)/DBLE(100) - (DBLE(10)/DBLE(100)) &
            & *partial_anis/(partial_anis - partial_anis_bk)
      endif

      partial_anis_bk = partial_anis

   enddo

   write(4,*) 'zero ',zero
   write(*,*) 'zero ',zero

   close(4)

   return
end subroutine boxes_partial_texture_2


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_ftmean()
! Here, we calculate tensor of mean tangential forces of a box
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_ftmean()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: fra ! counter for the frames
   integer :: nalfa_id ! counter for the angular intervals
   double precision :: a, b, c, d ! four components of a 2x2 matrix (e.g. the stress or ftmean tensor)
   double precision :: lamda_1, lamda_2, lamda_3, lamda_4 ! eigenvalues of a 2x2 matrix (e.g. the stress or ftmean tensor)
   double precision :: lamda_1_x, lamda_1_y, lamda_2_x, lamda_2_y ! components of the eigenvectors
   double precision :: nx, ny, tx, ty ! components defining the direction of each angular interval
   double precision :: delta_nalfa, nalfa_dir
   double precision :: mean, ecart_type

   delta_nalfa = pi/DBLE(nalfa)

   do fra=1, nfr ! for all the frames

! 1) initialize the components of the ftmean tensor

      b_ftmean_xx(1,fra) = 0
      b_ftmean_xy(1,fra) = 0
      b_ftmean_yx(1,fra) = 0
      b_ftmean_yy(1,fra) = 0

! 2) calculate the contribution of each angular interval to the tensor of mean normal forces

      do nalfa_id=1, nalfa

         nalfa_dir = nalfa_id * delta_nalfa - delta_nalfa/2.

         nx = cos(nalfa_dir)
         ny = sin(nalfa_dir)
         tx = -sin(nalfa_dir)
         ty = cos(nalfa_dir)

         b_ftmean_xx(1,fra) = b_ftmean_xx(1,fra) + &
            & alfa_ftmean(fra,nalfa_id)*nx*tx*delta_nalfa
         b_ftmean_xy(1,fra) = b_ftmean_xy(1,fra) + &
            & alfa_ftmean(fra,nalfa_id)*nx*ty*delta_nalfa
         b_ftmean_yx(1,fra) = b_ftmean_yx(1,fra) + &
            & alfa_ftmean(fra,nalfa_id)*ny*tx*delta_nalfa
         b_ftmean_yy(1,fra) = b_ftmean_yy(1,fra) + &
            & alfa_ftmean(fra,nalfa_id)*ny*ty*delta_nalfa

      enddo

   enddo

   do fra = 1, nfr ! for all the frames

! 3) Calculate the principal components of the ftmean tensor and their directions

      a = b_ftmean_xx(1,fra)
      b = b_ftmean_xy(1,fra)
      c = b_ftmean_yx(1,fra)
      d = b_ftmean_yy(1,fra)

      if (b == 0) then ! a and d are the eigenvalues of the tensor

         if (a >= d) then

            b_ftmean_eigv11(1,fra) = a
            b_ftmean_eigv33(1,fra) = d

            b_dir_anisotropy_ftmean(1,fra) = 0

            b_anisotropy_ftmean(1,fra) = 10

         else

            b_ftmean_eigv11(1,fra) = d
            b_ftmean_eigv33(1,fra) = a

            b_dir_anisotropy_ftmean(1,fra) = pi/2

            b_anisotropy_ftmean(1,fra) = 10

         endif

      else

         lamda_1 = (a+d)/2 + (((4*b*c) + (a-d)**2)**0.5)/2
         lamda_2 = (a+d)/2 - (((4*b*c) + (a-d)**2)**0.5)/2

!         if (fra == 175) then
!            write(*,*)'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
!            write(*,*)'fra ',fra
!            write(*,*)'b_ftmean_xx(1,fra) ',b_ftmean_xx(1,fra)
!            write(*,*)'b_ftmean_xy(1,fra) ',b_ftmean_xy(1,fra)
!            write(*,*)'b_ftmean_yx(1,fra) ',b_ftmean_yx(1,fra)
!            write(*,*)'b_ftmean_yy(1,fra) ',b_ftmean_yy(1,fra)
!            write(*,*)'a ',a
!            write(*,*)'b ',b
!            write(*,*)'c ',c
!            write(*,*)'d ',d
!            write(*,*)'lamda_1 ',lamda_1,'lamda_2 ',lamda_2
!            write(*,*)'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
!         endif

         lamda_1_y = 1
         lamda_1_x = b / (lamda_1 - a)

         b_ftmean_eigv11(1,fra) = lamda_1
         b_ftmean_eigv33(1,fra) = lamda_2
         lamda_3 = b_fnmean_eigv11(1,fra)
         lamda_4 = b_fnmean_eigv33(1,fra)

         b_anisotropy_ftmean(1,fra) = 2 * (lamda_1 - lamda_2)/(lamda_3 + lamda_4)
         b_dir_anisotropy_ftmean(1,fra) = atan(lamda_1_y / lamda_1_x)

         if (b_dir_anisotropy_ftmean(1,fra) < 0) then

            b_dir_anisotropy_ftmean(1,fra) = pi + b_dir_anisotropy_ftmean(1,fra)

         endif

      endif

   enddo

   open(1,file='./postpro/ftmean_tensor',form='formatted',status='unknown')
   open(2,file='./postpro/anisotropy_ftmean_amplitude',form='formatted',status='unknown')
   open(3,file='./postpro/anisotropy_ftmean_direction',form='formatted',status='unknown')

   do fra=1, nfr ! for all the frames

         write(1,*) fra,b_ftmean_xx(1,fra),b_ftmean_xy(1,fra),b_ftmean_yy(1,fra)
	  
         write(2,*) fra,b_anisotropy_ftmean(1,fra)
	  
         write(3,*) fra,b_dir_anisotropy_ftmean(1,fra)*180/pi
	  
   enddo

   close(1)
   close(2)
   close(3)

   return
end subroutine boxes_ftmean


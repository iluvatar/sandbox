!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!     SR r_c()
! Here, we read and assign the information contained in the
! 'connam' file
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine r_contacts()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: con ! counter for the contacts
   integer :: par ! counter for the particles
   integer :: k, fra !counters for the frames
   integer :: a, nc_provisional, np_provisional
   double precision :: b
   logical :: c

! read the 'con_name' file
   open(1,file=con_name,form='formatted',status='old')

   fra = 1

   do k = 1, nfr_total

      if (k == fr(fra)) then !only save the information in the frames
! that have been specified by the user.

         read(1,*) nl, nd, np
         read(1,*) nc(fra), sn(fra), tm(fra)
         read(1,*) (cpoint(par,fra), par=1, np)
         read(1,*) (clist(con,fra), con=1, nc(fra))
         read(1,*) (cfn(con,fra), con=1, nc(fra))
         read(1,*) (cft(con,fra), con=1, nc(fra))
         read(1,*) (cfs(con,fra), con=1, nc(fra))
         read(1,*) (cfel(con,fra), con=1, nc(fra))
         read(1,*) (cstatus(con,fra), con=1, nc(fra))
         read(1,*) (dani(con,fra), con=1, nc(fra))
         read(1,*) (danj(con,fra), con=1, nc(fra))
         read(1,*) (cpore(con,fra), con=1, nc(fra)) !this is recalculated in c1.f
         read(1,*) (cax(con,fra), con=1, nc(fra))
         read(1,*) (cax_n(con,fra), con=1, nc(fra))
         read(1,*) (cax_t(con,fra), con=1, nc(fra))
         read(1,*)

         write(*,*) 'read the contacts info, frame # ',k,' --> OK!'
         write(*,*) 'save frame # ',fra,' --> history file # ',fr(fra)

         fra = fra+1

      else

         read(1,*) a, a, np_provisional ! a is an integer, b is a double precision
         read(1,*) nc_provisional, a, b
         read(1,*) (a, par=1, np_provisional)
         read(1,*) (a, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*) (c, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional) !this is recalculated in c1.f
         read(1,*) (c, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*) (b, con=1, nc_provisional)
         read(1,*)

         write(*,*) 'read the contacts info, frame # ',k,' --> OK!'

      endif

   enddo

   close(1)

   return
end subroutine r_contacts

!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR particles_nc()
! Here, we calculate the number of contacts per disc.
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine particles_nc()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: i ! counter
   integer :: con ! counter for the contacts
   integer :: par ! counter for the particles
   integer :: fra ! counter for the frames
   integer :: di, dj ! disks identifiers
   integer :: nc_1c
   integer :: worms ! number of worm's ends
   integer, dimension (11,nfr_max) :: np_Xc
   integer, dimension (11,nfr_max) :: np_Xc_eff
   double precision :: mean
   double precision :: fn_1c

   do fra=1,nfr ! for all the frames
   
	  write (*,*) 'the number of worms in the frame # ',fr(fra),' is = '

      do i=1, 11
         np_Xc(i,fra) = 0
         np_Xc_eff(i,fra) = 0
      enddo

      do par=1, np
         dnc(par,fra)=0
         dnc_eff(par,fra)=0
      enddo

      do con=1,nc(fra) ! for all the contacts
!      if (cfn(con,fra) /= 0.) then

! Assign the particles identifiers for the two particles

         di=d_id_i(con,fra)
         dj=d_id_j(con,fra)

! Calculate the number of contacts per particle

         dnc(di,fra) = dnc(di,fra) + 1
         dnc(dj,fra) = dnc(dj,fra) + 1

!      endif
      enddo

      do par=5,np

         if (dnc(par,fra) == 0) then
            np_Xc(1,fra) = np_Xc(1,fra) + 1
         endif

         if (dnc(par,fra) == 1) then
            np_Xc(2,fra) = np_Xc(2,fra) + 1
         endif

         if (dnc(par,fra) == 2) then
            np_Xc(3,fra) = np_Xc(3,fra) + 1
         endif

         if (dnc(par,fra) == 3) then
            np_Xc(4,fra) = np_Xc(4,fra) + 1
         endif

         if (dnc(par,fra) == 4) then
            np_Xc(5,fra) = np_Xc(5,fra) + 1
         endif

         if (dnc(par,fra) == 5) then
            np_Xc(6,fra) = np_Xc(6,fra) + 1
         endif

         if (dnc(par,fra) == 6) then
            np_Xc(7,fra) = np_Xc(7,fra) + 1
         endif

         if (dnc(par,fra) == 7) then
            np_Xc(8,fra) = np_Xc(8,fra) + 1
         endif

         if (dnc(par,fra) == 8) then
            np_Xc(9,fra) = np_Xc(9,fra) + 1
         endif

         if (dnc(par,fra) == 9) then
            np_Xc(10,fra) = np_Xc(10,fra) + 1
         endif

         if (dnc(par,fra) >= 10) then
            np_Xc(11,fra) = np_Xc(11,fra) + 1
         endif

      enddo

! calculate the effective number of contacts for each particle. By effective
! we mean that he doesn't belong to a floating chain (or worm). This is done
! by eliminating all the particles with one effective contact until the number
! of worms is zero. Actually, "worms" is not the number of worms but the number
! of particles that form the worm's ends.

      do par=1, np

         dnc_eff(par,fra) = dnc(par,fra)

      enddo

      worms = 1

      do while (worms > 0)

         do con=1,nc(fra) ! for all the contacts

! Assign the particles identifiers for the two particles

            di=d_id_i(con,fra)
            dj=d_id_j(con,fra)

! reduce the number of effective contacts of the two particles in contact
! if one of the particles forms a worm's end

            if (((dnc_eff(di,fra)==1) .or. (dnc_eff(dj,fra)==1)) .and. &
               & (dnc_eff(di,fra)>0) .and. &
               & (dnc_eff(dj,fra)>0)) then

               dnc_eff(di,fra) = dnc_eff(di,fra) - 1
               dnc_eff(dj,fra) = dnc_eff(dj,fra) - 1

            endif

         enddo

! calculate the new number of worms

         worms = 0

         do par=5, np
            if (dnc_eff(par,fra) == 1) then
               worms = worms + 1
            endif
         enddo

         write (*,*) '     ',worms

      enddo

      do par=5,np

         if (dnc_eff(par,fra) == 0) then
            np_Xc_eff(1,fra) = np_Xc_eff(1,fra) + 1
         endif

         if (dnc_eff(par,fra) == 1) then
            np_Xc_eff(2,fra) = np_Xc_eff(2,fra) + 1
         endif

         if (dnc_eff(par,fra) == 2) then
            np_Xc_eff(3,fra) = np_Xc_eff(3,fra) + 1
         endif

         if (dnc_eff(par,fra) == 3) then
            np_Xc_eff(4,fra) = np_Xc_eff(4,fra) + 1
         endif

         if (dnc_eff(par,fra) == 4) then
            np_Xc_eff(5,fra) = np_Xc_eff(5,fra) + 1
         endif

         if (dnc_eff(par,fra) == 5) then
            np_Xc_eff(6,fra) = np_Xc_eff(6,fra) + 1
         endif

         if (dnc_eff(par,fra) == 6) then
            np_Xc_eff(7,fra) = np_Xc_eff(7,fra) + 1
         endif

         if (dnc_eff(par,fra) == 7) then
            np_Xc_eff(8,fra) = np_Xc_eff(8,fra) + 1
         endif

         if (dnc_eff(par,fra) == 8) then
            np_Xc_eff(9,fra) = np_Xc_eff(9,fra) + 1
         endif

         if (dnc_eff(par,fra) == 9) then
            np_Xc_eff(10,fra) = np_Xc_eff(10,fra) + 1
         endif

         if (dnc_eff(par,fra) >= 10) then
            np_Xc_eff(11,fra) = np_Xc_eff(11,fra) + 1
         endif
      enddo

   enddo

   open(3,file='./postpro/%p_Xc',form='formatted',status='unknown')
   do fra=1, nfr
      write(3,*) fra, (np_Xc(i,fra)/DBLE(nd), i=1, 11)
   enddo
   close(3)

   open(3,file='./postpro/%p_Xc_eff',form='formatted',status='unknown')
   do fra=1, nfr
      write(3,*) fra, (np_Xc_eff(i,fra)/DBLE(nd), i=1, 11)
   enddo
   close(3)

   write(*,*)

   return
end subroutine particles_nc


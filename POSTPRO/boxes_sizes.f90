!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_sizes()
! Here, we calculate the sizes of the boxes and the initial and final positions
! of the boxes
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_sizes()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: lin ! counter for the lines
   integer :: col ! counter for the columns
   integer :: box ! counter for the boxes
   integer :: fra ! counter for the frames

! 1) Calculate the number of boxes

   nb = 1 ! in this version there is only one box

! 2) Calculate the size and the positions of the boxes

   if (b_type == 0) then ! authomatic box

      do fra=1, nfr

!         by_dim(fra) = (dry(3,fra)-0.2)

!         bx_dim(fra) = (drx(4,fra) - drx(2,fra) - 0.04)/3.
!         by_dim(fra) = (dry(3,fra) - dry(1,fra) - 0.04)/3.

! 0.2 m es la distancia que me alejo del muro, esto es algo que debe decidirse
! para cada ensayo.

         box = 1  ! in this version there is only one box

         bx1(1,fra) = drx(2,fra)
         by1(1,fra) = dry(1,fra)
         bx2(1,fra) = drx(4,fra)
         by2(1,fra) = dry(3,fra)

!         bx1(1,fra) = drx(2,fra)
!         by1(1,fra) = dry(1,fra) + 2.667
!         bx2(1,fra) = drx(4,fra)
!         by2(1,fra) = dry(3,fra) - 2.667

!         bx1(1,fra) = drx(2,fra) + 0.02 + bx_dim(fra)
!         bx2(1,fra) = bx1(1,fra) + bx_dim(fra)
!         by2(1,fra) = dry(3,fra) - 0.02 - 2.*by_dim(fra)
!         by1(1,fra) = by2(1,fra) - by_dim(fra)

!      write(*,*)'frame # ',fra,' upper wall position = ',dry(3,fra)
!      write(*,*)'bx1 = ',bx1(1,fra),'by1 = ',by1(1,fra)
!      write(*,*)'bx2 = ',bx2(1,fra),'by2 = ',by2(1,fra)

         bx_dim(fra) = bx2(1,fra) - bx1(1,fra)
         by_dim(fra) = by2(1,fra) - by1(1,fra)

         bx1(1,fra) = 0.
         bx2(1,fra) = bx_dim(fra)
         by1(1,fra) = by1(1,fra)
         by2(1,fra) = by2(1,fra)

         bx_dim(fra) = bx2(1,fra) - bx1(1,fra)
         by_dim(fra) = by2(1,fra) - by1(1,fra)

         write(*,*) 'frame ',fra
         write(*,*) 'bx1 ',bx1(1,fra)
         write(*,*) 'bx2 ',bx2(1,fra)
         write(*,*) 'by1 ',by1(1,fra)
         write(*,*) 'by2 ',by2(1,fra)
         write(*,*) 'bx_dim ',bx_dim(fra)
         write(*,*) 'by_dim ',by_dim(fra)
         write(*,*) 'by_dim/bx_dim ',by_dim(fra)/bx_dim(fra)

enddo

   else if (b_type == 1) then ! specified box

      do fra=1, nfr

         box = 1 ! in the specified box option there is only one box

         bx_dim(fra) = spe_x2 - spe_x1
         by_dim(fra) = spe_y2 - spe_y1

         bx1(1,fra) = spe_x1
         by1(1,fra) = spe_y1
         bx2(1,fra) = spe_x2
         by2(1,fra) = spe_y2

      enddo

   endif

   return
end subroutine boxes_sizes


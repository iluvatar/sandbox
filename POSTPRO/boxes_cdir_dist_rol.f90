!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_cdir_dist_rol()
! Here, we calculate probability distribution of contact directions
! for the rolling contacts
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_cdir_dist_rol()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: nalfa_id ! counter for the angular intervals
   integer :: fra ! counter for the frames
   character*40 :: n_file1 !name of the file to write
   integer, dimension (nfr_max,nalfa_max):: nc_alfa
   double precision :: delta_nalfa ! angular thickness of an angular interval
   double precision :: suma
   double precision :: angle, x, y !angle, length and coordinates for drawing the polar graphs
   double precision, dimension (nalfa_max) :: p_nc_alfa_mean
   double precision, dimension (nfr_max,nalfa_max):: p_nc_alfa_ten

! 1) Calculate the size of the angular interval (for the probability density function of the contact
! directions)
! initialize the number of contacts in each angular interval

   delta_nalfa = pi / DBLE(nalfa)

   do fra=1, nfr ! for all the frames

      do nalfa_id=1, nalfa

         nc_alfa(fra,nalfa_id) = 0

      enddo

   enddo

   delta_nalfa = pi / real(nalfa)

   do fra=1, nfr ! for all the frames

      do con=1, nc(fra) ! for all the contacts

! 1) find if the contact belons to the box or not

         if ((c_valid(con,fra)) .and. (rolling(con,fra))) then

! 2) Calculate the number of contacts in each angular interval

            nalfa_id = floor(cdir(con,fra)/delta_nalfa) + 1
            nc_alfa(fra,nalfa_id) = nc_alfa(fra,nalfa_id) + 1

         endif

      enddo

   enddo


! 3) Calculate the probablility of occurrence of a contact in a given direction

   do fra = 1, nfr

!      suma = 0

      do nalfa_id = 1, nalfa

         p_nc_alfa_ten(fra,nalfa_id) = DBLE(nc_alfa(fra,nalfa_id)) / &
            & (delta_nalfa*DBLE(bnc_ten(fra)))

!         suma = suma + p_nc_alfa_ten(fra,nalfa_id)*delta_nalfa

! here we must normalize by real(bnc(1,fra)) for the summ of all probabilities to be 1

      enddo

!      write(*,*)'fra ',fra,' suma ',suma

   enddo


! 5) write the mean distribution of contact directions

   do fra = 1, nfr

      n_file1='./postpro/cdir_dist_rol'//fr_id(fra)
      open(1,file=n_file1,form='formatted',status='unknown')

         do nalfa_id = 1, nalfa
            angle = DBLE(nalfa_id*180)/DBLE(nalfa) - 180./DBLE(2*nalfa)
            x = p_nc_alfa_ten(fra,nalfa_id) * cos(angle*pi/180)
            y = p_nc_alfa_ten(fra,nalfa_id) * sin(angle*pi/180)
            write(1,*) x, y, angle, p_nc_alfa_ten(fra,nalfa_id)
         enddo

         do nalfa_id = 1, nalfa
            angle = 180. + DBLE(nalfa_id*180/nalfa) - 180./DBLE(2*nalfa)
            x = p_nc_alfa_ten(fra,nalfa_id) * cos(angle*pi/180)
            y = p_nc_alfa_ten(fra,nalfa_id) * sin(angle*pi/180)
            write(1,*) x, y, angle, p_nc_alfa_ten(fra,nalfa_id)
         enddo
		 
         angle = DBLE(1*180/nalfa) - 180./DBLE(2*nalfa)
         x = p_nc_alfa_ten(fra,1) * cos(angle*pi/180)
         y = p_nc_alfa_ten(fra,1) * sin(angle*pi/180)
         write(1,*) x, y, angle, p_nc_alfa_ten(fra,1)

	  close(1)
	  
   enddo

   return
end subroutine boxes_cdir_dist_rol


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR contacts_network()
! Here, we find, for each contact, if it belons to thes trong or the weak
! network.
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine contacts_network()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: con !counter for the contacts
   integer :: fra !counter for the frames
   integer :: nc_comp, nc_ten
   double precision :: sum_fn, sum_fn_comp, sum_fn_ten !summ of all the normal forces
   double precision :: sum_ft !summ of all the tangential forces
   double precision :: sum_l !summ of all intercenter lengths
   double precision :: average_fn ! average normal force (time average)
   double precision :: average_ft ! average tangential force (time average)

   write(*,*) '%%%%% Mean forces %%%%%'

   do fra=1,nfr ! for all the frames

      sum_fn = 0.
      sum_fn_comp = 0.
      sum_fn_ten = 0.
      sum_ft = 0.
      sum_l = 0.
      fn_mean(fra) = 0.
      fn_mean_comp(fra) = 0.
      fn_mean_ten(fra) = 0.
      ft_mean(fra) = 0.
      l_mean(fra) = 0.
	  nc_comp = 0
	  nc_ten = 0

      do con=1,nc(fra) ! for all the contacts
         if (c_valid(con,fra)) then
            sum_fn = sum_fn + cfn(con,fra)
            sum_ft = sum_ft + cft(con,fra)
            sum_l = sum_l + cl(con,fra)
			if (cfn(con,fra) >= 0.) then
			   sum_fn_comp = sum_fn_comp + cfn(con,fra)
			   nc_comp = nc_comp + 1
			elseif (cfn(con,fra) < 0.) then
			   sum_fn_ten = sum_fn_ten + cfn(con,fra)
			   nc_ten = nc_ten + 1
			endif
         endif
      enddo

      fn_mean(fra) = sum_fn / DBLE(nc_valid(fra))
      fn_mean_comp(fra) = sum_fn_comp / DBLE(nc_comp)
      fn_mean_ten(fra) = sum_fn_ten / DBLE(nc_ten)
      ft_mean(fra) = sum_ft / DBLE(nc_valid(fra))
      l_mean(fra) = sum_l / DBLE(nc_valid(fra))

!	  write(*,*) 'nc_valid(fra) ',nc_valid(fra),', nc_comp ',nc_comp,', nc_ten ',nc_ten,', (nc_comp + nc_ten) ',(nc_comp + nc_ten)

      do con=1,nc(fra) ! for all the contacts

         if (cfn(con,fra) >= fn_mean(fra)) then
!         if (cfn(con,fra) >= 0.061) then
            s_network(con,fra)=.TRUE. ! the contact belonges to the strong network
         else
            s_network(con,fra)=.FALSE. ! the contact belonges to the weak network
         endif

      enddo
	  
      write(*,*) 'frame # = ',fr(fra),', fn_mean(fra) = ',fn_mean(fra), &
         & ', ft_mean(fra) = ',ft_mean(fra),', l_mean(fra) = ',l_mean(fra)

   enddo

   write(*,*)

! write output files

   open(1,file='./postpro/fn_mean',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,fn_mean(fra)
      enddo
   close(1)

   open(1,file='./postpro/fn_mean_comp',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,fn_mean_comp(fra)
      enddo
   close(1)

   open(1,file='./postpro/fn_mean_ten',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,fn_mean_ten(fra)
      enddo
   close(1)
   
   open(1,file='./postpro/fn_ten_over_fn_comp',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,(fn_mean_ten(fra)/fn_mean_comp(fra))
      enddo
   close(1)

   open(1,file='./postpro/ft_mean',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,ft_mean(fra)
      enddo
   close(1)

   open(1,file='./postpro/l_mean',form='formatted',status='unknown')
      do fra=1,nfr ! for all the frames
         write(1,*) fra,l_mean(fra)
      enddo
   close(1)

   return
end subroutine contacts_network

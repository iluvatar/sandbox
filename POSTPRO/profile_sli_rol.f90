!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR profile_sli_rol()
! Here, we write the profile of sliding and rolling contacts
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine profile_sli_rol()
   implicit none 

! global variables
   include './glob_postpro'

! local variables

   integer :: con !counter for the contacts
   integer :: fra !counter for the frames
   integer :: i !counter for the intervals
   integer, dimension(50) :: nc_delta_sli, nc_delta_rol, nc_delta
   double precision :: delta
   double precision, dimension(50) :: p_delta_sli, p_delta_rol
   character*30 :: n_file1 !name of the file to write

   do fra = 1, nfr ! for all the frames

   delta = dry(3,fra)/50.
!   write(*,*) 'delta=',delta
	  
      do i = 1, 50
	     nc_delta(i) = 0 ! initialise the number of contacts
         nc_delta_sli(i) = 0 ! initialise the number of contacts
         nc_delta_rol(i) = 0 ! initialise the number of contacts
      enddo

      do con = 1, nc(fra) ! for all the contacts
         if (c_valid(con,fra)) then
            i = floor(cry(con,fra)/delta) + 1
			nc_delta(i) = nc_delta(i) + 1
			if (sliding(con,fra)) then
			   nc_delta_sli(i) = nc_delta_sli(i) + 1
			endif
			if (rolling(con,fra)) then
			   nc_delta_rol(i) = nc_delta_rol(i) + 1
			endif
			if ((d_id_i(con,fra) == 1) .or. (d_id_i(con,fra) == 3) &
			    & .or. (d_id_j(con,fra) == 1) .or. (d_id_j(con,fra) == 3)) then
			   write(*,*) 'ojo, contactos con el muro'
			endif
         endif
      enddo
	  
      do i= 1, 50
         p_delta_sli(i) = DBLE(nc_delta_sli(i))/DBLE(nc_delta(i))
         p_delta_rol(i) = DBLE(nc_delta_rol(i))/DBLE(nc_delta(i))
      enddo

      n_file1='./postpro/profile_sli_rol'//fr_id(fra) ! open a file for each frame
      open(3,file=n_file1,form='formatted',status='unknown')
      do i= 1, 50
!	     write(*,*) 'fra=',fra,' i=',i,' dry(3,fra)',dry(3,fra)
	     write(3,*) ((DBLE(i)*delta)-(delta/2.)),&
		            & p_delta_sli(i),p_delta_rol(i),nc_delta(i)
	  enddo
      close(3)

   enddo
   
   return
end subroutine profile_sli_rol

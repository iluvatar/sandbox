!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_fs_pdf()
! Here, we find the pdf of contact torques
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_fs_pdf()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: i !counter for the force intervals
   integer :: con !counter for the contacts
   integer :: fra !counter for the frames
   integer :: box_nc ! number of contacts in the box
   integer :: di, dj !id of the two particles in contact
   integer, dimension(501) :: nc_delta ! number of contacts in a force interval
   double precision :: delta ! size of the force interval
   double precision :: rdij !summ of the radii of the two particles
   double precision :: thresh_r ! rolling threshold
   double precision, dimension(501,nfr_max) :: p_delta ! probability of ocurrence
! of a contact in a given force interval
   double precision, dimension(501) :: p_delta_mean ! mean probability
! of ocurrence of a contact in a given force interval
   character*40 :: n_file1 !name of the file to write

   delta = 1./100. ! calculate the size of the force interval

   do fra = 1, nfr ! for all the frames

      box_nc = 0 ! initialise the number of contacts in the box
	  
      do i = 1, 201
         nc_delta(i) = 0 ! initialise the number of contacts in the force interval
!		 write(*,*) 'i = ',i,', nc_delta(i) = ',nc_delta(i),', box_nc = ',box_nc
      enddo

      do con = 1, nc(fra) ! for all the contacts

         if (c_valid(con,fra)) then

            di=d_id_i(con,fra)
            dj=d_id_j(con,fra)

            rdij = dr(di) + dr(dj)

		 thresh_r = mu_r * rdij * (adh*rdij + cfn(con,fra))

!            i = floor(cfn(con,fra)/(fn_mean(fra)*delta)) + 1
            i = (cfn(con,fra)/DABS(cfn(con,fra)))*(floor(DABS(cfs(con,fra))/(thresh_r * delta))) + 1
			
!            write(*,*) 'cfn(con,fra) = ',cfn(con,fra),', i = ',i
            if ((i <= 100).or.(i >= -100)) then

               nc_delta(i+101) = nc_delta(i+101) + 1 ! increment the number of contacts in the force interval
               box_nc = box_nc +1 !  increment the number of contacts in the box

            endif

         endif

      enddo

      do i= 1, 201
         p_delta(i,fra) = DBLE(nc_delta(i))/(DBLE(box_nc)*delta) ! calculate the probability of occurrence

! for each angular interval
      enddo

      n_file1='./postpro/pdf_fs_'//fr_id(fra) ! open a file for each frame
      open(3,file=n_file1,form='formatted',status='unknown')
      do i= 1, 201
	     write(3,*) ((DBLE(i-101)*delta)-(delta/2.)), p_delta(i,fra),nc_delta(i),box_nc
	  enddo
      close(3)

      write(*,*) 'nfr = ',nfr,', control, frame = ',fra

   enddo

   return
end subroutine boxes_fs_pdf

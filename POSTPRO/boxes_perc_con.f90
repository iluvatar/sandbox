!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_perc_con()
! Here, we calculate the percentages of XXX contacts in each box
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_perc_con()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: par ! counter for the particles
   integer :: fra ! counter for the frames
   double precision :: deq

! initialize

   do fra=1, nfr ! for all the frames

      bnc(1,fra) = 0
	  
      bnc_snet(fra) = 0
      bnc_wnet(fra) = 0
	  
      bnc_comp(fra) = 0
      bnc_ten(fra) = 0

      bnc_sli(fra) = 0
      bnc_rol(fra) = 0
      bnc_sli_rol(fra) = 0
      bnc_sli_nrol(fra) = 0
      bnc_nsli_rol(fra) = 0
      bnc_nsli_nrol(fra) = 0
	  
      bnc_rup_ten(fra) = 0
	  bnc_ini(fra) = 0
	  
      bnc_sli_snet(fra) = 0
      bnc_rol_snet(fra) = 0
	  bnc_comp_snet(fra) = 0
	  bnc_ten_snet(fra) = 0
      bnc_sli_rol_snet(fra) = 0
      bnc_sli_nrol_snet(fra) = 0
      bnc_nsli_rol_snet(fra) = 0
      bnc_nsli_nrol_snet(fra) = 0
      bnc_rup_ten_snet(fra) = 0

      bnc_sli_wnet(fra) = 0
      bnc_rol_wnet(fra) = 0
	  bnc_comp_wnet(fra) = 0
	  bnc_ten_wnet(fra) = 0
      bnc_sli_rol_wnet(fra) = 0
      bnc_sli_nrol_wnet(fra) = 0
      bnc_nsli_rol_wnet(fra) = 0
      bnc_nsli_nrol_wnet(fra) = 0
      bnc_rup_ten_wnet(fra) = 0

   enddo

   do fra=1, nfr ! for all the frames

      do con=1, nc(fra) ! for all the contacts

! 1) find if the contact is a valid one or not or not

         if (c_valid(con,fra)) then

! 2) Calculate the number of contacts in the box

            bnc(1,fra) = bnc(1,fra) + 1

! 3a) calculate the number of contacts in the strong and weak networks

            if (s_network(con,fra)) then
               bnc_snet(fra) = bnc_snet(fra) + 1
            else
               bnc_wnet(fra) = bnc_wnet(fra) + 1
            endif
			
! 3b) calculate the number of compressive and tensile contacts

            if (cfn(con,fra) >= 0.) then
               bnc_comp(fra) = bnc_comp(fra) + 1
            else
               bnc_ten(fra) = bnc_ten(fra) + 1
            endif

! 4) calculate the number of sliding and not-sliding contacts

            if (sliding(con,fra)) then
               bnc_sli(fra) = bnc_sli(fra) + 1
            else
               bnc_nsli(fra) = bnc_nsli(fra) + 1
            endif

! 5) calculate the number of rolling and not-rolling contacts

            if (rolling(con,fra)) then
               bnc_rol(fra) = bnc_rol(fra) + 1
            else
               bnc_nrol(fra) = bnc_nrol(fra) + 1
            endif

! 6) calculate the number of sliding and rolling contacts

            if (sliding(con,fra) .and. rolling(con,fra)) then
               bnc_sli_rol(fra) = bnc_sli_rol(fra) + 1
            endif

! 7) calculate the number of sliding and not-rolling contacts

            if (sliding(con,fra) .and. (.not.rolling(con,fra))) then
               bnc_sli_nrol(fra) = bnc_sli_nrol(fra) + 1
            endif

! 8) calculate the number of non-sliding and rolling contacts

            if ((.not.sliding(con,fra)) .and. rolling(con,fra)) then
               bnc_nsli_rol(fra) = bnc_nsli_rol(fra) + 1
            endif

! 9a) calculate the number of non-sliding and non-rolling contacts

            if ((.not.sliding(con,fra)) .and. (.not.rolling(con,fra))) then
               bnc_nsli_nrol(fra) = bnc_nsli_nrol(fra) + 1
            endif

! 9b) calculate the number of contacts whose tensile force is at the tensile threshold

            if (rup_ten(con,fra)) then
               bnc_rup_ten(fra) = bnc_rup_ten(fra) + 1
            endif

! 9c) calculate the number of initial (i.e., not residual) contacts

            if (cstatus(con,fra)) then
               bnc_ini(fra) = bnc_ini(fra) + 1
            endif

! 10) calculate the same things for the strong network 

           if (s_network(con,fra)) then

              if (sliding(con,fra)) then
                 bnc_sli_snet(fra) = bnc_sli_snet(fra) + 1
              else
                 bnc_nsli_snet(fra) = bnc_nsli_snet(fra) + 1
              endif

              if (rolling(con,fra)) then
                 bnc_rol_snet(fra) = bnc_rol_snet(fra) + 1
              else
                 bnc_nrol_snet(fra) = bnc_nrol_snet(fra) + 1
              endif

              if (cfn(con,fra) >= 0.) then
                 bnc_comp_snet(fra) = bnc_comp_snet(fra) + 1
              else
                 bnc_ten_snet(fra) = bnc_ten_snet(fra) + 1
              endif

              if (sliding(con,fra) .and. rolling(con,fra)) then
                 bnc_sli_rol_snet(fra) = bnc_sli_rol_snet(fra) + 1
              endif

              if (sliding(con,fra) .and. (.not.rolling(con,fra))) then
                 bnc_sli_nrol_snet(fra) = bnc_sli_nrol_snet(fra) + 1
              endif

              if ((.not.sliding(con,fra)) .and. rolling(con,fra)) then
                 bnc_nsli_rol_snet(fra) = bnc_nsli_rol_snet(fra) + 1
              endif

              if ((.not.sliding(con,fra)) .and. (.not.rolling(con,fra))) then
                 bnc_nsli_nrol_snet(fra) = bnc_nsli_nrol_snet(fra) + 1
              endif
			  
              if (rup_ten(con,fra)) then
                 bnc_rup_ten_snet(fra) = bnc_rup_ten_snet(fra) + 1
              endif

           endif

! 11) calculate the same things for the weak network

           if (.not.s_network(con,fra)) then

              if (sliding(con,fra)) then
                 bnc_sli_wnet(fra) = bnc_sli_wnet(fra) + 1
              else
                 bnc_nsli_wnet(fra) = bnc_nsli_wnet(fra) + 1
              endif

              if (rolling(con,fra)) then
                 bnc_rol_wnet(fra) = bnc_rol_wnet(fra) + 1
              else
                 bnc_nrol_wnet(fra) = bnc_nrol_wnet(fra) + 1
              endif

              if (cfn(con,fra) >= 0.) then
                 bnc_comp_wnet(fra) = bnc_comp_wnet(fra) + 1
              else
                 bnc_ten_wnet(fra) = bnc_ten_wnet(fra) + 1
              endif

              if (sliding(con,fra) .and. rolling(con,fra)) then
                 bnc_sli_rol_wnet(fra) = bnc_sli_rol_wnet(fra) + 1
              endif

              if (sliding(con,fra) .and. (.not.rolling(con,fra))) then
                 bnc_sli_nrol_wnet(fra) = bnc_sli_nrol_wnet(fra) + 1
              endif

              if ((.not.sliding(con,fra)) .and. rolling(con,fra)) then
                 bnc_nsli_rol_wnet(fra) = bnc_nsli_rol_wnet(fra) + 1
              endif

              if ((.not.sliding(con,fra)) .and. (.not.rolling(con,fra))) then
                 bnc_nsli_nrol_wnet(fra) = bnc_nsli_nrol_wnet(fra) + 1
              endif
			  
              if (rup_ten(con,fra)) then
                 bnc_rup_ten_wnet(fra) = bnc_rup_ten_wnet(fra) + 1
              endif

           endif

         endif
              
      enddo

   enddo

   do fra=1, nfr ! for all the frames

! 12) Calculate the contacts percentages in the box:

! a1. contacts belonging to the strong network
      if (nc_valid(fra) /= 0.) perc_snet(fra) = real(bnc_snet(fra))/real(nc_valid(fra))
! b1. contacts belonging to the weak network
      if (nc_valid(fra) /= 0.) perc_wnet(fra) = real(bnc_wnet(fra))/real(nc_valid(fra))
! a2. compressive contacts
      if (nc_valid(fra) /= 0.) perc_comp(fra) = real(bnc_comp(fra))/real(nc_valid(fra))
! b2. tensile contacts
      if (nc_valid(fra) /= 0.) perc_ten(fra) = real(bnc_ten(fra))/real(nc_valid(fra))
! c. sliding contacts
      if (nc_valid(fra) /= 0.) perc_sli(fra) = real(bnc_sli(fra))/real(nc_valid(fra))
! d. not-sliding contacts
      if (nc_valid(fra) /= 0.) perc_nsli(fra) = real(bnc_nsli(fra))/real(nc_valid(fra))
! e. rolling contacts
      if (nc_valid(fra) /= 0.) perc_rol(fra) = real(bnc_rol(fra))/real(nc_valid(fra))
! f. not-rolling contacts
      if (nc_valid(fra) /= 0.) perc_nrol(fra) = real(bnc_nrol(fra))/real(nc_valid(fra))
! g. sliding and rolling contacts
      if (nc_valid(fra) /= 0.) perc_sli_rol(fra) = real(bnc_sli_rol(fra))/real(nc_valid(fra))
! h. sliding and not-rolling contacts
      if (nc_valid(fra) /= 0.) perc_sli_nrol(fra) = real(bnc_sli_nrol(fra))/real(nc_valid(fra))
! i. not-sliding and rolling contacts
      if (nc_valid(fra) /= 0.) perc_nsli_rol(fra) = real(bnc_nsli_rol(fra))/real(nc_valid(fra))
! j1. not-sliding and not-rolling contacts
      if (nc_valid(fra) /= 0.) perc_nsli_nrol(fra) = real(bnc_nsli_nrol(fra))/real(nc_valid(fra))
! j2. contacts in the tensile threshold
      if (nc_valid(fra) /= 0.) perc_rup_ten(fra) = real(bnc_rup_ten(fra))/real(nc_valid(fra))
! j3. initial (i.e., not residual) contacts
      if (nc_valid(fra) /= 0.) perc_ini(fra) = real(bnc_ini(fra))/real(nc_valid(fra))

! k1. compressive contacts
      if (bnc_comp(fra) /= 0.) perc_comp_snet(fra) = real(bnc_comp_snet(fra))/real(nc_valid(fra))
! k2. tensile contacts
      if (bnc_ten(fra) /= 0.) perc_ten_snet(fra) = real(bnc_ten_snet(fra))/real(nc_valid(fra))
! k3. sliding contacts
      if (bnc_sli(fra) /= 0.) perc_sli_snet(fra) = real(bnc_sli_snet(fra))/real(nc_valid(fra))
! l. not-sliding contacts
      if (bnc_nsli(fra) /= 0.) perc_nsli_snet(fra) = real(bnc_nsli_snet(fra))/real(nc_valid(fra))
! m. rolling contacts
      if (bnc_rol(fra) /= 0.) perc_rol_snet(fra) = real(bnc_rol_snet(fra))/real(nc_valid(fra))
! n. not-rolling contacts
      if (bnc_nrol(fra) /= 0.) perc_nrol_snet(fra) = real(bnc_nrol_snet(fra))/real(nc_valid(fra))
! o. sliding and rolling contacts
      if (bnc_sli_rol(fra) /= 0.) perc_sli_rol_snet(fra) = real(bnc_sli_rol_snet(fra))/real(nc_valid(fra))
! p. sliding and not-rolling contacts
      if (bnc_sli_nrol(fra) /= 0.) perc_sli_nrol_snet(fra) = real(bnc_sli_nrol_snet(fra))/real(nc_valid(fra))
! q. not-sliding and rolling contacts
      if (bnc_nsli_rol(fra) /= 0.) perc_nsli_rol_snet(fra) = real(bnc_nsli_rol_snet(fra))/real(nc_valid(fra))
! r1. not-sliding and not-rolling contacts
      if (bnc_nsli_nrol(fra) /= 0.) perc_nsli_nrol_snet(fra) = real(bnc_nsli_nrol_snet(fra))/real(nc_valid(fra))
! r2. contacts in the tensile threshold
      if (bnc_rup_ten(fra) /= 0.) perc_rup_ten_snet(fra) = real(bnc_rup_ten_snet(fra))/real(nc_valid(fra))

! s1. compressive contacts
      if (bnc_comp(fra) /= 0.) perc_comp_wnet(fra) = real(bnc_comp_wnet(fra))/real(nc_valid(fra))
! s2. tensile contacts
      if (bnc_ten(fra) /= 0.) perc_ten_wnet(fra) = real(bnc_ten_wnet(fra))/real(nc_valid(fra))
! s3. sliding contacts
      if (bnc_sli(fra) /= 0.) perc_sli_wnet(fra) = real(bnc_sli_wnet(fra))/real(nc_valid(fra))
! t. not-sliding contacts
      if (bnc_nsli(fra) /= 0.) perc_nsli_wnet(fra) = real(bnc_nsli_wnet(fra))/real(nc_valid(fra))
! u. rolling contacts
      if (bnc_rol(fra) /= 0.) perc_rol_wnet(fra) = real(bnc_rol_wnet(fra))/real(nc_valid(fra))
! v. not-rolling contacts
      if (bnc_nrol(fra) /= 0.) perc_nrol_wnet(fra) = real(bnc_nrol_wnet(fra))/real(nc_valid(fra))
! w. sliding and rolling contacts
      if (bnc_sli_rol(fra) /= 0.) perc_sli_rol_wnet(fra) = real(bnc_sli_rol_wnet(fra))/real(nc_valid(fra))
! x. sliding and not-rolling contacts
      if (bnc_sli_nrol(fra) /= 0.) perc_sli_nrol_wnet(fra) = real(bnc_sli_nrol_wnet(fra))/real(nc_valid(fra))
! y. not-sliding and rolling contacts
      if (bnc_nsli_rol(fra) /= 0.) perc_nsli_rol_wnet(fra) = real(bnc_nsli_rol_wnet(fra))/real(nc_valid(fra))
! z. not-sliding and not-rolling contacts
      if (bnc_nsli_nrol(fra) /= 0.) perc_nsli_nrol_wnet(fra) = real(bnc_nsli_nrol_wnet(fra))/real(nc_valid(fra))
! r2. contacts in the tensile threshold
      if (bnc_rup_ten(fra) /= 0.) perc_rup_ten_wnet(fra) = real(bnc_rup_ten_wnet(fra))/real(nc_valid(fra))

! k1. compressive contacts
!      if (bnc_comp(fra) /= 0.) perc_comp_snet(fra) = real(bnc_comp_snet(fra))/real(bnc_comp(fra))
! k2. tensile contacts
!      if (bnc_ten(fra) /= 0.) perc_ten_snet(fra) = real(bnc_ten_snet(fra))/real(bnc_ten(fra))
! k3. sliding contacts
!      if (bnc_sli(fra) /= 0.) perc_sli_snet(fra) = real(bnc_sli_snet(fra))/real(bnc_sli(fra))
! l. not-sliding contacts
!      if (bnc_nsli(fra) /= 0.) perc_nsli_snet(fra) = real(bnc_nsli_snet(fra))/real(bnc_nsli(fra))
! m. rolling contacts
!      if (bnc_rol(fra) /= 0.) perc_rol_snet(fra) = real(bnc_rol_snet(fra))/real(bnc_rol(fra))
! n. not-rolling contacts
!      if (bnc_nrol(fra) /= 0.) perc_nrol_snet(fra) = real(bnc_nrol_snet(fra))/real(bnc_nrol(fra))
! o. sliding and rolling contacts
!      if (bnc_sli_rol(fra) /= 0.) perc_sli_rol_snet(fra) = real(bnc_sli_rol_snet(fra))/real(bnc_sli_rol(fra))
! p. sliding and not-rolling contacts
!      if (bnc_sli_nrol(fra) /= 0.) perc_sli_nrol_snet(fra) = real(bnc_sli_nrol_snet(fra))/real(bnc_sli_nrol(fra))
! q. not-sliding and rolling contacts
!      if (bnc_nsli_rol(fra) /= 0.) perc_nsli_rol_snet(fra) = real(bnc_nsli_rol_snet(fra))/real(bnc_nsli_rol(fra))
! r1. not-sliding and not-rolling contacts
!      if (bnc_nsli_nrol(fra) /= 0.) perc_nsli_nrol_snet(fra) = real(bnc_nsli_nrol_snet(fra))/real(bnc_nsli_nrol(fra))
! r2. contacts in the tensile threshold
!      if (bnc_rup_ten(fra) /= 0.) perc_rup_ten_snet(fra) = real(bnc_rup_ten_snet(fra))/real(bnc_rup_ten(fra))

! s1. compressive contacts
!      if (bnc_comp(fra) /= 0.) perc_comp_wnet(fra) = real(bnc_comp_wnet(fra))/real(bnc_comp(fra))
! s2. tensile contacts
!      if (bnc_ten(fra) /= 0.) perc_ten_wnet(fra) = real(bnc_ten_wnet(fra))/real(bnc_ten(fra))
! s3. sliding contacts
!      if (bnc_sli(fra) /= 0.) perc_sli_wnet(fra) = real(bnc_sli_wnet(fra))/real(bnc_sli(fra))
! t. not-sliding contacts
!      if (bnc_nsli(fra) /= 0.) perc_nsli_wnet(fra) = real(bnc_nsli_wnet(fra))/real(bnc_nsli(fra))
! u. rolling contacts
!      if (bnc_rol(fra) /= 0.) perc_rol_wnet(fra) = real(bnc_rol_wnet(fra))/real(bnc_rol(fra))
! v. not-rolling contacts
!      if (bnc_nrol(fra) /= 0.) perc_nrol_wnet(fra) = real(bnc_nrol_wnet(fra))/real(bnc_nrol(fra))
! w. sliding and rolling contacts
!      if (bnc_sli_rol(fra) /= 0.) perc_sli_rol_wnet(fra) = real(bnc_sli_rol_wnet(fra))/real(bnc_sli_rol(fra))
! x. sliding and not-rolling contacts
!      if (bnc_sli_nrol(fra) /= 0.) perc_sli_nrol_wnet(fra) = real(bnc_sli_nrol_wnet(fra))/real(bnc_sli_nrol(fra))
! y. not-sliding and rolling contacts
!      if (bnc_nsli_rol(fra) /= 0.) perc_nsli_rol_wnet(fra) = real(bnc_nsli_rol_wnet(fra))/real(bnc_nsli_rol(fra))
! z. not-sliding and not-rolling contacts
!      if (bnc_nsli_nrol(fra) /= 0.) perc_nsli_nrol_wnet(fra) = real(bnc_nsli_nrol_wnet(fra))/real(bnc_nsli_nrol(fra))
! r2. contacts in the tensile threshold
!      if (bnc_rup_ten(fra) /= 0.) perc_rup_ten_wnet(fra) = real(bnc_rup_ten_wnet(fra))/real(bnc_rup_ten(fra))


! 13) Calculate the same statistics but only for the contacts in the strong network:

! c. sliding contacts
!      SN_perc_sli(fra) = real(bnc_sli_snet(fra))/real(bnc_snet(fra))
! d. not-sliding contacts
!      SN_perc_nsli(fra) = real(bnc_nsli_snet(fra))/real(bnc_snet(fra))
! e. rolling contacts
!      SN_perc_rol(fra) = real(bnc_rol_snet(fra))/real(bnc_snet(fra))
! f. not-rolling contacts
!      SN_perc_nrol(fra) = real(bnc_nrol_snet(fra))/real(bnc_snet(fra))
! g. sliding and rolling contacts
!      SN_perc_sli_rol(fra) = real(bnc_sli_rol_snet(fra))/real(bnc_snet(fra))
! h. sliding and not-rolling contacts
!      SN_perc_sli_nrol(fra) = real(bnc_sli_nrol_snet(fra))/real(bnc_snet(fra))
! i. not-sliding and rolling contacts
!      SN_perc_nsli_rol(fra) = real(bnc_nsli_rol_snet(fra))/real(bnc_snet(fra))
! j. not-sliding and not-rolling contacts
!      SN_perc_nsli_nrol(fra) = real(bnc_nsli_nrol_snet(fra))/real(bnc_snet(fra))

! 14) Calculate the same statistics but only for the contacts in the weak network:

! c. sliding contacts
!      WN_perc_sli(fra) = real(bnc_sli_wnet(fra))/real(bnc_wnet(fra))
! d. not-sliding contacts
!      WN_perc_nsli(fra) = real(bnc_nsli_wnet(fra))/real(bnc_wnet(fra))
! e. rolling contacts
!      WN_perc_rol(fra) = real(bnc_rol_wnet(fra))/real(bnc_wnet(fra))
! f. not-rolling contacts
!      WN_perc_nrol(fra) = real(bnc_nrol_wnet(fra))/real(bnc_wnet(fra))
! g. sliding and rolling contacts
!      WN_perc_sli_rol(fra) = real(bnc_sli_rol_wnet(fra))/real(bnc_wnet(fra))
! h. sliding and not-rolling contacts
!      WN_perc_sli_nrol(fra) = real(bnc_sli_nrol_wnet(fra))/real(bnc_wnet(fra))
! i. not-sliding and rolling contacts
!      WN_perc_nsli_rol(fra) = real(bnc_nsli_rol_wnet(fra))/real(bnc_wnet(fra))
! j. not-sliding and not-rolling contacts
!      WN_perc_nsli_nrol(fra) = real(bnc_nsli_nrol_wnet(fra))/real(bnc_wnet(fra))

   enddo

! >>>>>>>>>>>>>>>>>>>>
! write output data

   open(1,file='./postpro/perc_snet_wnet',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_snet(fra),perc_wnet(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_comp_ten',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_comp(fra),perc_ten(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_sli_rol',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
		 deq = 2. * (((dry(3,fra) - dry(3,1))/(2. * dry(3,1)))**2. &
			   & + (drx(3,fra)/(2. * dry(3,1)))**2.)**0.5
         write(1,*) deq,perc_sli(fra),perc_nsli(fra),perc_rol(fra),perc_nrol(fra), &
      	  & perc_sli_rol(fra),perc_sli_nrol(fra),perc_nsli_rol(fra),perc_nsli_nrol(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_critic',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_sli(fra),perc_rol(fra),perc_rup_ten(fra), &
		 & (perc_sli(fra) + perc_rol(fra) + perc_rup_ten(fra))
	  enddo
   close(1)

   open(1,file='./postpro/perc_rup_ten',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_rup_ten(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_ini',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_ini(fra)
	  enddo
   close(1)



   open(1,file='./postpro/perc_comp_ten_snet',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_comp_snet(fra),perc_ten_snet(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_sli_rol_snet',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_sli_snet(fra),perc_nsli_snet(fra),perc_rol_snet(fra),perc_nrol_snet(fra), &
	     & perc_sli_rol_snet(fra),perc_sli_nrol_snet(fra),perc_nsli_rol_snet(fra),perc_nsli_nrol_snet(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_rup_ten_snet',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_rup_ten_snet(fra)
	  enddo
   close(1)



   open(1,file='./postpro/perc_comp_ten_wnet',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_comp_wnet(fra),perc_ten_wnet(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_sli_rol_wnet',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_sli_wnet(fra),perc_nsli_wnet(fra),perc_rol_wnet(fra),perc_nrol_wnet(fra), &
	     & perc_sli_rol_wnet(fra),perc_sli_nrol_wnet(fra),perc_nsli_rol_wnet(fra),perc_nsli_nrol_wnet(fra)
	  enddo
   close(1)

   open(1,file='./postpro/perc_rup_ten_wnet',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,perc_rup_ten_wnet(fra)
	  enddo
   close(1)

! >>>>>>>>>>>>>>>>>>>>


   return
end subroutine boxes_perc_con


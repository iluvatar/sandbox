!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_fn_pdf()
! Here, we find the pdf of normal forces
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_fn_pdf()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: i !counter for the force intervals
   integer :: con !counter for the contacts
   integer :: fra !counter for the frames
   integer :: box_nc ! number of contacts in the box
   integer, dimension(501) :: nc_delta ! number of contacts in a force interval
   double precision :: delta ! size of the force interval
   double precision, dimension(501,nfr_max) :: p_delta ! probability of ocurrence
! of a contact in a given force interval
   double precision, dimension(501) :: p_delta_mean ! mean probability
! of ocurrence of a contact in a given force interval
   character*40 :: n_file1 !name of the file to write

   delta = 4./100. ! calculate the size of the force interval

   do fra = 1, nfr ! for all the frames

      box_nc = 0 ! initialise the number of contacts in the box
	  
      do i = 1, 501
         nc_delta(i) = 0 ! initialise the number of contacts in the force interval
!		 write(*,*) 'i = ',i,', nc_delta(i) = ',nc_delta(i),', box_nc = ',box_nc
      enddo

      do con = 1, nc(fra) ! for all the contacts

         if (c_valid(con,fra)) then

            i = floor(cfn(con,fra)/(fn_mean(fra)*delta)) + 1
!            i = floor(cfn(con,fra)/(30.*delta)) + 1
			
!            write(*,*) 'cfn(con,fra) = ',cfn(con,fra),', i = ',i
            if (i <= 501) then
!            if ((i <= 100).or.(i >= -100)) then

               nc_delta(i) = nc_delta(i) + 1 ! increment the number of contacts in the force interval
!               nc_delta(i+101) = nc_delta(i+101) + 1 ! increment the number of contacts in the force interval
               box_nc = box_nc +1 !  increment the number of contacts in the box

            endif

         endif

      enddo

      do i= 1, 501
         p_delta(i,fra) = DBLE(nc_delta(i))/(DBLE(box_nc)*delta) ! calculate the probability of occurrence

! for each angular interval
      enddo

      n_file1='./postpro/pdf_fn_'//fr_id(fra) ! open a file for each frame
      open(3,file=n_file1,form='formatted',status='unknown')
      do i= 1, 501
	     write(3,*) ((DBLE(i)*delta)-(delta/2.)), p_delta(i,fra),nc_delta(i),box_nc
!	     write(3,*) ((DBLE(i-101)*delta)-(delta/2.)), p_delta(i,fra),nc_delta(i),box_nc
	  enddo
      close(3)

      write(*,*) 'nfr = ',nfr,', control, frame = ',fra

   enddo

   return
end subroutine boxes_fn_pdf

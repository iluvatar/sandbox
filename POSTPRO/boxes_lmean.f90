!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_lmean()
! Here, we calculate tensor of mean intercenter lengths of a box
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_lmean()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: fra ! counter for the frames
   integer :: nalfa_id ! counter for the angular intervals
   double precision :: a, b, c, d ! four components of a 2x2 matrix (e.g. the stress or fnmean tensor)
   double precision :: lamda_1, lamda_2 ! eigenvalues of a 2x2 matrix (e.g. the stress or fnmean tensor)
   double precision :: lamda_1_x, lamda_1_y, lamda_2_x, lamda_2_y ! components of the eigenvectors
   double precision :: nx, ny ! components defining the direction of each angular interval
   double precision :: delta_nalfa, nalfa_dir
   double precision :: mean, ecart_type

   delta_nalfa = pi/DBLE(nalfa)

   do fra=1, nfr ! for all the frames

! 1) initialize the components of the fnmean tensor

      b_lmean_xx(1,fra) = 0
      b_lmean_xy(1,fra) = 0
      b_lmean_yy(1,fra) = 0

! 2) calculate the contribution of each angular interval to the tensor of mean normal forces

      do nalfa_id=1, nalfa

         nalfa_dir = nalfa_id * delta_nalfa - delta_nalfa/2.

         nx = cos(nalfa_dir)
         ny = sin(nalfa_dir)

         b_lmean_xx(1,fra) = b_lmean_xx(1,fra) + &
            & alfa_lmean(fra,nalfa_id)*nx*nx*delta_nalfa
         b_lmean_xy(1,fra) = b_lmean_xy(1,fra) + &
            & alfa_lmean(fra,nalfa_id)*nx*ny*delta_nalfa
         b_lmean_yy(1,fra) = b_lmean_yy(1,fra) + &
            & alfa_lmean(fra,nalfa_id)*ny*ny*delta_nalfa

!   write(*,*) 'nalfa=',nalfa
!   write(*,*) 'nx=',nx
!   write(*,*) 'ny=',ny
!   write(*,*) 'b_fnmean_xx(1,fra)=',b_fnmean_xx(1,fra)
!   write(*,*) 'b_fnmean_xy(1,fra)=',b_fnmean_xy(1,fra)
!   write(*,*) 'b_fnmean_yy(1,fra)=',b_fnmean_yy(1,fra)
!   write(*,*) 'alfa_fnmean(fra,nalfa_id)=',alfa_fnmean(fra,nalfa_id)
!   read(*,*)

      enddo

   enddo

   do fra = 1, nfr ! for all the frames

! 3) Calculate the principal components of the fnmean tensor and their directions

      a = b_lmean_xx(1,fra)
      b = b_lmean_xy(1,fra)
      c = b_lmean_xy(1,fra)
      d = b_lmean_yy(1,fra)

      if (b == 0) then ! a and d are the eigenvalues of the tensor

         if (a >= d) then

            b_lmean_eigv11(1,fra) = a
            b_lmean_eigv33(1,fra) = d

            b_dir_anisotropy_lmean(1,fra) = 0

            b_anisotropy_lmean(1,fra) = 2 * (a - d)/(a + d)

         else

            b_lmean_eigv11(1,fra) = d
            b_lmean_eigv33(1,fra) = a

            b_dir_anisotropy_lmean(1,fra) = pi/2

            b_anisotropy_lmean(1,fra) = 2 * (d - a)/(d + a)

         endif

      else

         lamda_1 = (a+d)/2 + (((4*b*c) + (a-d)**2)**0.5)/2
         lamda_2 = (a+d)/2 - (((4*b*c) + (a-d)**2)**0.5)/2

         lamda_1_y = 1
         lamda_1_x = b / (lamda_1 - a)

         b_lmean_eigv11(1,fra) = lamda_1
         b_lmean_eigv33(1,fra) = lamda_2
         b_anisotropy_lmean(1,fra) = 2 * (lamda_1 - lamda_2)/(lamda_1 + lamda_2)
         b_dir_anisotropy_lmean(1,fra) = atan(lamda_1_y / lamda_1_x)

         if (b_dir_anisotropy_lmean(1,fra) < 0) then

            b_dir_anisotropy_lmean(1,fra) = pi + b_dir_anisotropy_lmean(1,fra)

         endif

      endif

   enddo

   open(1,file='./postpro/lmean_tensor',form='formatted',status='unknown')
   open(2,file='./postpro/anisotropy_lmean_amplitude',form='formatted',status='unknown')
   open(3,file='./postpro/anisotropy_lmean_direction',form='formatted',status='unknown')

   do fra=1, nfr ! for all the frames

      write(1,*) fra,b_lmean_xx(1,fra),b_lmean_xy(1,fra),b_lmean_yy(1,fra)
	  
	  write(2,*) fra,b_anisotropy_lmean(1,fra)
	  
	  write(3,*) fra,b_dir_anisotropy_lmean(1,fra)*180/pi
	  
   enddo

   close(1)
   close(2)
   close(3)

   return
end subroutine boxes_lmean


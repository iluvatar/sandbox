!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_partial_texture()
! Here, we calculate the partial texture tensor of a box
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_partial_texture()
   implicit none 

! global variables
   include './glob_postpro'

! local variables

   integer :: i
   integer :: con ! counter for the contacts
   integer :: fra ! counter for the frames
   integer :: x, y ! truncated positions of an object
   double precision :: a, b, c, d ! four components of a 2x2 matrix (e.g. the stress or texture tensor)
   double precision :: lamda_1, lamda_2 ! eigenvalues of a 2x2 matrix (e.g. the stress or texture tensor)
   double precision :: lamda_1_x, lamda_1_y, lamda_2_x, lamda_2_y ! components of the eigenvectors
   double precision :: p_texture_xx, p_texture_xy, p_texture_yy ! components of the
! partial texture tensor
   double precision :: p_texture_eigv11, p_texture_eigv33 ! eigenvalues of the texture tensor
   double precision, dimension (100,nfr_max) :: p_anisotropy, p_dir_anisotropy ! anisotropy and direction of
! the anisotropy
   double precision, dimension (100) :: a_mean, dir_mean
   integer :: p_nc ! partial number of contacts
   character*40 :: n_file1 !name of the file to write
   double precision :: threshold, threshold_bk


   do fra=1, nfr ! for all the frames

      n_file1='./postpro/partial_texture_'//fr_id(fra) ! open a file for each frame

      open(3,file=n_file1,form='formatted',status='unknown')

      do i=1, 100 ! for a certain number of points

! initialize the components of the texture tensor

         threshold = -2. + DBLE(i*4.)/DBLE(100)
         threshold_bk = threshold - 4./100.

         p_texture_xx = 0
         p_texture_xy = 0
         p_texture_yy = 0

         p_nc = 0

         do con=1, nc(fra) ! for all the contacts

! 1) find if the contact belons to the box or not and if the normalized normal
! force (fn(con)/<fn>) is less than (i*6/100)

            if ((c_valid(con,fra)) &
               & .and. ((cfn(con,fra)/30.) <= threshold) &
			   & .and. ((cfn(con,fra)/30.) > threshold_bk)) then

!            if ((c_valid(con,fra)) &
!               & .and. ((cfn(con,fra)/30.)<threshold)) then

!            if ((c_valid(con,fra)) &
!               & .and. ((cfn(con,fra)/fn_mean(fra))<threshold)) then

! 2) Calculate the contribution of each contact to the texture tensor.
! Note that the quantities calculated here are not the components of the texture tensor, 
! at least not yet, since they must be normalized by the number of contacts in each box.

               p_texture_xx = p_texture_xx + rxij_u(con,fra)**2
               p_texture_xy = p_texture_xy + rxij_u(con,fra) * ryij_u(con,fra)
               p_texture_yy = p_texture_yy + ryij_u(con,fra)**2

               p_nc = p_nc + 1

            endif

         enddo

! 3) Normalize the texture tensor for each box (as mentioned earlier, the components texture tensor
! must be normalized by the number of contacts in the box)

         p_texture_xx = p_texture_xx / DBLE(p_nc)
         p_texture_xy = p_texture_xy / DBLE(p_nc)
         p_texture_yy = p_texture_yy / DBLE(p_nc)

! 4) Calculate the principal components of the texture tensor and their directions

         a = p_texture_xx
         b = p_texture_xy
         c = p_texture_xy
         d = p_texture_yy

         if (b == 0) then ! a and d are the eigenvalues of the tensor

            if (a >= d) then

               p_texture_eigv11 = a
               p_texture_eigv33 = d

               p_dir_anisotropy(i,fra) = 0

               p_anisotropy(i,fra) = 2 * (a - d)

            else

               p_texture_eigv11 = d
               p_texture_eigv33 = a

               p_dir_anisotropy(i,fra) = pi/2

               p_anisotropy(i,fra) = 2 * (d - a)

            endif

         else

            lamda_1 = (a+d)/2 + (((4*b*c) + (a-d)**2)**0.5)/2
            lamda_2 = (a+d)/2 - (((4*b*c) + (a-d)**2)**0.5)/2

            lamda_1_y = 1
            lamda_1_x = b / (lamda_1 - a)

            p_texture_eigv11 = lamda_1
            p_texture_eigv33 = lamda_2
            p_anisotropy(i,fra) = 2 * (lamda_1 - lamda_2)
            p_dir_anisotropy(i,fra) = atan(lamda_1_y / lamda_1_x)

            if (p_dir_anisotropy(i,fra) < 0.) then
               p_dir_anisotropy(i,fra) = p_dir_anisotropy(i,fra) + pi
            endif

            if (p_dir_anisotropy(i,fra) < (pi/2.)) then
			   p_anisotropy(i,fra) = -p_anisotropy(i,fra)
            endif

         endif

! write one more point of the graph of partial texture values

         write(3,*) (threshold - 2./100.),p_anisotropy(i,fra),&
            &(p_dir_anisotropy(i,fra)*180/pi),p_nc

      enddo

      close(3)

   enddo

   return
end subroutine boxes_partial_texture


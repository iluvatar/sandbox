!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_stress()
! Here, we calculate stress tensor of a box
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_stress()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: par ! counter for the particles
   integer :: fra ! counter for the frames
   integer :: x, y ! truncated positions of an object
   double precision :: a, b, c, d ! four components of a 2x2 matrix (e.g. the stress or texture tensor)
   double precision :: lamda_1, lamda_2 ! eigenvalues of a 2x2 matrix (e.g. the stress or texture tensor)
   double precision :: lamda_1_x, lamda_1_y, lamda_2_x, lamda_2_y ! components of the eigenvectors
   double precision :: ry1_init, rx2_init, ry3_init, rx4_init
   double precision :: q, p, q2, p2, dex, dey, dep, deq, dep_k, deq_k, qoverp
   double precision :: p_mean, q_mean, p_max, p_min, q_max, q_min, mag
   double precision :: mean, ecart_type
   double precision, dimension (nb_max, nfr_max) :: b_intm_xx, b_intm_xy, b_intm_yx, b_intm_yy 
! components of the tensor of internal moment of a box

   do fra=1, nfr ! for all the frames

! initialize the components of the tensor of internal moment

      b_intm_xx(1,fra) = 0
      b_intm_xy(1,fra) = 0
      b_intm_yx(1,fra) = 0
      b_intm_yy(1,fra) = 0

      do par=5, np ! for all the particles

! 1) find if the particle is a valid one or not

         if (d_valid(par,fra)) then

! 2) Calculate the contribution of each particle to the tensor of internal moment of the box

            b_intm_xx(1,fra) = b_intm_xx(1,fra) + d_intm_xx(par,fra)
            b_intm_xy(1,fra) = b_intm_xy(1,fra) + d_intm_xy(par,fra)
            b_intm_yx(1,fra) = b_intm_yx(1,fra) + d_intm_yx(par,fra)
            b_intm_yy(1,fra) = b_intm_yy(1,fra) + d_intm_yy(par,fra)

         endif

      enddo

   enddo

   do fra = 1, nfr ! for all the frames

! 3) Calculate the stress tensor of the box

      b_stress_xx(1,fra) = b_intm_xx(1,fra) / (bx_dim(fra) * by_dim(fra))
      b_stress_xy(1,fra) = b_intm_xy(1,fra) / (bx_dim(fra) * by_dim(fra))
      b_stress_yx(1,fra) = b_intm_yx(1,fra) / (bx_dim(fra) * by_dim(fra))
      b_stress_yy(1,fra) = b_intm_yy(1,fra) / (bx_dim(fra) * by_dim(fra))

! 4) Calculate the principal stresses and their directions

      a = b_stress_xx(1,fra)
      b = b_stress_xy(1,fra)
      c = b_stress_yx(1,fra)
      d = b_stress_yy(1,fra)

      if ((b == 0) .or. (c == 0)) then ! a and d are the eigenvalues of the tensor

         if (a <= d) then

            b_stress_eigv11(1,fra) = d
            b_stress_eigv33(1,fra) = a

            b_dir_stress_eigv11(1,fra) = pi/2

         else

            b_stress_eigv11(1,fra) = a
            b_stress_eigv33(1,fra) = d

            b_dir_stress_eigv11(1,fra) = 0

         endif

      else

         lamda_1 = (a+d)/2 + (((4*b*c) + (a-d)**2)**0.5)/2
         lamda_2 = (a+d)/2 - (((4*b*c) + (a-d)**2)**0.5)/2

         lamda_1_y = 1
         lamda_1_x = b / (lamda_1 - a)

         lamda_2_y = 1
         lamda_2_x = b / (lamda_2 - a)

         b_stress_eigv11(1,fra) = lamda_1
         b_stress_eigv33(1,fra) = lamda_2
         b_dir_stress_eigv11(1,fra) = atan(lamda_1_y / lamda_1_x)

         if (b_dir_stress_eigv11(1,fra) < 0) then

            b_dir_stress_eigv11(1,fra) = pi + b_dir_stress_eigv11(1,fra)

         endif

         b_dir_stress_eigv33(1,fra) = b_dir_stress_eigv11(1,fra) - pi/2

      endif

   enddo

! write output files

   open(1,file='./postpro/stress_tensor',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,b_stress_xx(1,fra),b_stress_xy(1,fra),b_stress_yx(1,fra) &
		            & ,b_stress_yy(1,fra)
      enddo
   close(1)
	  
   open(1,file='./postpro/stress_tensor_eigv',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,b_stress_eigv11(1,fra),b_stress_eigv33(1,fra)
      enddo
   close(1)
	  
   open(1,file='./postpro/stress_tensor_dir_eigv',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
         write(1,*) fra,b_dir_stress_eigv11(1,fra)*180/pi,b_dir_stress_eigv33(1,fra)*180/pi
      enddo
   close(1)
	  
   open(1,file='./postpro/fi',form='formatted',status='unknown')
      do fra=1, nfr ! for all the frames
	     q = (b_stress_eigv11(1,fra)-b_stress_eigv33(1,fra))/2.
	     p = (b_stress_eigv11(1,fra)+b_stress_eigv33(1,fra))/2.
	     qoverp = (b_stress_eigv11(1,fra)-b_stress_eigv33(1,fra))/ &
		          & (b_stress_eigv11(1,fra)+b_stress_eigv33(1,fra))
         write(1,*) fra,atan(qoverp)*180/pi
      enddo
   close(1)

  open(1,file='./postpro/q_p_qoverp',form='formatted',status='unknown')
    do fra=1, nfr ! for all the frames
        q = (b_stress_eigv11(1,fra)-b_stress_eigv33(1,fra))/2.
        p = (b_stress_eigv11(1,fra)+b_stress_eigv33(1,fra))/2.
        qoverp = (b_stress_eigv11(1,fra)-b_stress_eigv33(1,fra))/ &
                & (b_stress_eigv11(1,fra)+b_stress_eigv33(1,fra))
        write(1,*) fra,qoverp,q,p,drx(3,fra)
    enddo
  close(1)

   open(1,file='./postpro/dep_deq_p_q',form='formatted',status='unknown')

      ry1_init = dry(1,1)
      rx2_init = drx(2,1)
      ry3_init = dry(3,1)
      rx4_init = drx(4,1)

      do fra=1, nfr ! for all the frames

         dey = ((ry3_init - ry1_init) - (dry(3,fra) - dry(1,fra)))/(ry3_init - ry1_init)
         dex = ((rx4_init - rx2_init) - (drx(4,fra) - drx(2,fra)))/(rx4_init - rx2_init)

	     dep = dey + dex
		 deq = dey - dex

	     p = (b_stress_eigv11(1,fra)+b_stress_eigv33(1,fra))/2.
	     q = (b_stress_eigv11(1,fra)-b_stress_eigv33(1,fra))/2.

         p2 = (b_stress_yy(1,fra) + b_stress_xx(1,fra))/2
         q2 = (b_stress_yy(1,fra) - b_stress_xx(1,fra))/2

         write(1,*) dep,deq,p,q,p2,q2
      enddo
   close(1)

   open(1,file='./postpro/fluencia',form='formatted',status='unknown')

      p_mean = 0.
      q_mean = 0.
      p_max = (b_stress_yy(1,nfr) + b_stress_xx(1,nfr))/2
      p_min = (b_stress_yy(1,nfr) + b_stress_xx(1,nfr))/2
      q_max = (b_stress_yy(1,nfr) - b_stress_xx(1,nfr))/2
      q_min = (b_stress_yy(1,nfr) - b_stress_xx(1,nfr))/2

      do fra= (nfr-19), nfr ! for all the frames

         p2 = (b_stress_yy(1,fra) + b_stress_xx(1,fra))/2
         q2 = (b_stress_yy(1,fra) - b_stress_xx(1,fra))/2

         p_mean = p_mean + p2
         q_mean = q_mean + q2

         if (p2 > p_max) p_max = p2
         if (p2 < p_min) p_min = p2
         if (q2 > q_max) q_max = q2
         if (p2 < q_min) q_min = q2

      enddo
	  
      dey = ((ry3_init - ry1_init) - (dry(3,nfr) - dry(1,nfr)))/(ry3_init - ry1_init)
      dex = ((rx4_init - rx2_init) - (drx(4,nfr) - drx(2,nfr)))/(rx4_init - rx2_init)

	  dep = dey + dex
	  deq = dey - dex

	  mag = (dep**2. + deq**2.)**0.5

	  dep = dep/mag
	  deq = deq/mag

      p_mean = p_mean/20.
      q_mean = q_mean/20.

      write(1,*) 'p_mean,q_mean,dep,deq,error_p,error_q'
      write(1,*) p_mean,q_mean,dep,deq,(p_max-p_min)/p_mean,(q_max-q_min)/q_mean

   close(1)

   return
end subroutine boxes_stress


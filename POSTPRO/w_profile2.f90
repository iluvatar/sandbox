!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR w_profile()
! Here, we write last xtot profile.
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine w_profile2()
   implicit none 

! global variables
   include './glob_postpro'

! local variables

   integer :: par !counter for the particles
   integer :: fra !counter for the frames
   integer :: delta_id
   integer, dimension(100) :: nd_delta
   double precision :: delta
   double precision :: drxtot_max
   double precision, dimension(100) :: mean_drxtot
   character*30 :: n_file1 !name of the file to write

   drxtot_max = 0.

   do fra = 1, nfr ! for all the frames
	  
      do delta_id=1, 100
         mean_drxtot(delta_id) = 0.
         nd_delta(delta_id) = 0
      enddo

      delta = dry(3,fra)/100.

      write (*,*) 'fra: ',fra
      write (*,*) 'drx(3,fra): ',drx(3,fra)
      write (*,*) 'dry(3,fra): ',dry(3,fra)
      write (*,*) 'delta: ',delta

      do par=5, np
         delta_id = floor(dry(par,fra)/delta) + 1
         mean_drxtot(delta_id) = mean_drxtot(delta_id) + drxtot(par,fra)
         nd_delta(delta_id) = nd_delta(delta_id) + 1
!      if (drxtot(par,fra) >= drxtot_max) then
!         drxtot_max = drxtot(par,fra)
!      endif
      enddo

      do delta_id=1, 100
         mean_drxtot(delta_id) = mean_drxtot(delta_id)/DBLE(nd_delta(delta_id))
!      if (mean_drxtot(delta_id) >= drxtot_max) then
!         drxtot_max = mean_drxtot(delta_id)
!      endif
!	  write(*,*) 'delta_id=',delta_id,', nd_delta(delta_id)=',nd_delta(delta_id)
!	  write(*,*) '   mean_drxtot(delta_id)=',mean_drxtot(delta_id),', drxtot_max=',drxtot_max
      enddo

      n_file1='./postpro/profile'//fr_id(fra) ! open a file for each frame

      open(1,file=n_file1,form='formatted',status='unknown')

!   write(1,*) 'drxtot_max ', drxtot_max
      write(1,*) 0., 0., drx(3,fra)
      do delta_id=1, 100
!      write(1,*) mean_drxtot(delta_id)/drxtot_max, (delta_id-0.5)/100, drx(3,fra)
         write(1,*) mean_drxtot(delta_id)/drx(3,fra), (delta_id-0.5)/100., drx(3,fra)
      enddo
!   write(1,*) drx(3,fra)/drxtot_max, 1., drx(3,fra)
      write(1,*) 1., 1., drx(3,fra)
!   write(*,*) 'drxtot_max',drxtot_max

      close(1)

   enddo

   return
end subroutine w_profile2

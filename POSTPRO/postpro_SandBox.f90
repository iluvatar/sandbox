!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!  postpro_SandBox.f
!  Date    : octover 2008
!  Authors  : P. Aguilar, N. Estrada
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

program postpro_SandBox
   implicit none 

! main program

   write(*,*)'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
   write(*,*)'%%%%%%%%%% postpro_SandBox %%%%%%%%%%%%%%%%%%%%%%%'
   write(*,*)'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
   write(*,*)

! read the input parameters in postpro_parameters
   call r_postpro_parameters()
   write(*,*) '1. read the postpro_parameters file --> OK!'
   write(*,*)

! read the contacts information contained in the 'connam' file
   call r_contacts()
   write(*,*) '2. read the contacts information --> OK!'
   write(*,*)

! read the particles (disks and walls) information  contained
! in the 'parnam' and the 'posnam' files
   call r_particles()
!   call r_particles2()
   write(*,*) '3. read the particles information --> OK!'
   write(*,*)

! calculate the size of the boxes
   call boxes_sizes()
   write(*,*) '4. calculate the size of the boxes --> OK!'
   write(*,*)

! calculate the contacts positions and directions
   call contacts_pos_dir()
   write(*,*) '5. calculate the contacts positions and directions --> OK!'
   write(*,*)

! calculate the number of contacts on each particle
   call particles_nc()
   write(*,*) '6. calculate the number of contacts on each particle --> OK!'
   write(*,*)

! decide if a contact (or a particle) is valid or not
   call valid()
   write(*,*) '7. decide if a contact (or a particle) is valid or not --> OK!'
   write(*,*)

! calculate the contacts contribution to the texture tensor
   call contacts_texture()
   write(*,*) '8. calculate the contacts contribution to the texture tensor --> OK!'
   write(*,*)

! find, for each contact, if it belons to thes strong or the weak network
   call contacts_network()
   write(*,*) '9. find, for each contact, if it belons to thes strong or the weak network --> OK!'
   write(*,*)

! find the status (sliding, rolling, non-sliding, non-rolling) for each contact
   call contacts_sli_rol()
   write(*,*) '10. find the status (sliding, rolling, non-sliding, non-rolling) for each contact --> OK!'
   write(*,*)

! calculate the internal moment of each particle
   call particles_intm()
   write(*,*) '11. calculate the internal moment of each particle --> OK!'
   write(*,*)

! calculate the percentages of XXX contacts in each box
   call boxes_perc_con()
   write(*,*) '12. calculate the percentages of XXX contacts in each box --> OK!'
   write(*,*)

! calculate the percentages of XXX particles in each box
   call boxes_perc_par()
   write(*,*) '13. calculate the percentages of XXX particles in each box --> OK!'
   write(*,*)

! calculate the probability distribution of contact directions in each box
   call boxes_cdir_dist()
   write(*,*) '14. calculate the probability distribution of contact directions --> OK!'
   write(*,*)

! calculate the probability distribution of contact directions
! for the compressive forces
!   call boxes_cdir_dist_comp()
!   write(*,*) '15. calculate the probability distribution of contact directions (compressive forces)--> OK!'
!   write(*,*)

! calculate the probability distribution of contact directions in each box
! for the tensile forces
!   call boxes_cdir_dist_ten()
!   write(*,*) '16. calculate the probability distribution of contact directions (tensile forces)--> OK!'
!   write(*,*)

! calculate the probability distribution of contact directions in each box
! for the strong network
   call boxes_cdir_dist_snet()
   write(*,*) '17. calculate the probability distribution of contact directions (strong network)--> OK!'
   write(*,*)

! calculate the probability distribution of contact directions in each box
! for the weak network
   call boxes_cdir_dist_wnet()
   write(*,*) '18. calculate the probability distribution of contact directions (weak network)--> OK!'
   write(*,*)

! calculate the probability distribution of contact directions in each box
! for the critical contacts
!   call boxes_cdir_dist_sli()
!   call boxes_cdir_dist_rol()
!   call boxes_cdir_dist_critic()
!   write(*,*) '18. calculate the probability distribution of contact directions (critical contacts)--> OK!'
!   write(*,*)

! calculate the mean normal force distributions
   call boxes_cdir_fnmean()
!   call boxes_cdir_fnmean_comp()
!   call boxes_cdir_fnmean_ten()
   write(*,*) '19. calculate the mean normal force distributions--> OK!'
   write(*,*)

! calculate the mean normal force distributions
   call boxes_cdir_lmean()
!   call boxes_cdir_fnmean_comp()
!   call boxes_cdir_fnmean_ten()
   write(*,*) '19. calculate the mean intercenter length distributions--> OK!'
   write(*,*)

! calculate the mean tangential force distributions
   call boxes_cdir_ftmean()
   write(*,*) '20. calculate the mean tangential force distributions--> OK!'
   write(*,*)

! calculate the texture tensor of each box
   call boxes_texture()
   write(*,*) '21. calculate the texture tensor of each box --> OK!'
   write(*,*)

! calculate the texture tensor of each box for the strong network
   call boxes_texture_snet()
   write(*,*) '22. calculate the texture tensor of each box (strong network)--> OK!'
   write(*,*)

! calculate the texture tensor of each box for the weak network
   call boxes_texture_wnet()
   write(*,*) '23. calculate the texture tensor of each box (weak network)--> OK!'
   write(*,*)

! calculate the tensor of mean normal forces
   call boxes_fnmean()
   write(*,*) '24. calculate the tensor of mean normal forces--> OK!'
   write(*,*)

! calculate the tensor of mean normal forces
   call boxes_lmean()
   write(*,*) '24. calculate the tensor of mean intercenter lengths--> OK!'
   write(*,*)

! calculate the tensor of mean tangential forces
   call boxes_ftmean()
   write(*,*) '25. calculate the tensor of mean tangential forces--> OK!'
   write(*,*)

! calculate the partial texture of each box
   call boxes_partial_texture()
   write(*,*) '26. calculate the partial texture of each box--> OK!'
   write(*,*)

! calculate the partial texture of each box
!   call boxes_partial_texture_2()
!   write(*,*) 'calculate the partial texture_2 of each box--> OK!'

! calculate the stress tensor of each box
   call boxes_stress()
   write(*,*) '27. calculate the stress tensor of each box --> OK!'
   write(*,*)

! calculate the pdf of normal forces of the box
   call boxes_fn_pdf()
   write(*,*) '28. calculate the pdf of normal forces of the box --> OK!'
   write(*,*)

! calculate the pdf of tangential forces of the box
!   call boxes_ft_pdf()
   write(*,*) '29. calculate the pdf of tangential forces of the box --> OK!'
   write(*,*)

! calculate the pdf of contact torques of the box
!   call boxes_fs_pdf()
   write(*,*) '30. calculate the pdf of contact torques of the box --> OK!'
   write(*,*)

! write the contacts information
!   call w_contacts()
!   write(*,*) 'write the contacts information --> OK!'

! write the particles information
!   call w_particles()
!   write(*,*) 'write the particles information --> OK!'

! write the boxes information
!   call w_boxes()
!   write(*,*) 'write the boxes information --> OK!'

! write the output files
!   call w_output()
!   write(*,*) 'write the output files --> OK!'

! write the last xtot profile
   call w_profile()
!   call w_profile2()
!   write(*,*) 'write the last xtot profile --> OK!'
!   call profile_sli_rol()

! write the directional distributions
!   call w_cdir_dist()
!   write(*,*) 'write the directional distributions --> OK!'

   stop '%%%%% tout va bien ... !! %%%%%'

end

include './r_postpro_parameters.f90'
include './r_contacts.f90'
include './r_particles.f90'
include './r_particles2.f90'
include './contacts_pos_dir.f90'
include './contacts_texture.f90'
include './contacts_network.f90'
include './contacts_sli_rol.f90'
include './particles_nc.f90'
include './particles_intm.f90'
include './boxes_sizes.f90'
include './valid.f90'
include './boxes_perc_con.f90'
include './boxes_perc_par.f90'
include './boxes_cdir_dist.f90'
include './boxes_cdir_dist_comp.f90'
include './boxes_cdir_dist_ten.f90'
include './boxes_cdir_dist_snet.f90'
include './boxes_cdir_dist_wnet.f90'
include './boxes_cdir_dist_sli.f90'
include './boxes_cdir_dist_rol.f90'
include './boxes_cdir_dist_critic.f90'
include './boxes_cdir_fnmean.f90'
include './boxes_cdir_lmean.f90'
include './boxes_cdir_fnmean_comp.f90'
include './boxes_cdir_fnmean_ten.f90'
include './boxes_cdir_ftmean.f90'
include './boxes_texture.f90'
include './boxes_texture_snet.f90'
include './boxes_texture_wnet.f90'
include './boxes_fnmean.f90'
include './boxes_lmean.f90'
include './boxes_ftmean.f90'
include './boxes_partial_texture.f90'
!include './boxes_partial_texture_2.f90'
include './boxes_stress.f90'
include './boxes_fn_pdf.f90'
include './boxes_ft_pdf.f90'
include './boxes_fs_pdf.f90'
!include './w_output.f90'
include './w_profile.f90'
include './w_profile2.f90'
include './profile_sli_rol.f90'
!include './w_cdir_dist.f90'

! include './w_contacts.f90'
! include './w_particles.f90'
! include './w_boxes.f90'


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR boxes_texture_wnet()
! Here, we calculate texture tensor of a box for the strong network
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine boxes_texture_wnet()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: box ! counter for the boxes
   integer :: con ! counter for the contacts
   integer :: fra ! counter for the frames
   integer :: x, y ! truncated positions of an object
   double precision :: a, b, c, d ! four components of a 2x2 matrix (e.g. the stress or texture tensor)
   double precision :: lamda_1, lamda_2 ! eigenvalues of a 2x2 matrix (e.g. the stress or texture tensor)
   double precision :: lamda_1_x, lamda_1_y, lamda_2_x, lamda_2_y ! components of the eigenvectors


   do fra=1, nfr ! for all the frames

! initialize the components of the texture tensor

      b_texture_xx_wnet(1,fra) = 0
      b_texture_xy_wnet(1,fra) = 0
      b_texture_yy_wnet(1,fra) = 0

      do con=1, nc(fra) ! for all the contacts

! 1) find if the contact belons to the box or not

         if ((c_valid(con,fra)) .and. (.not.(s_network(con,fra)))) then

! 2) Calculate the contribution of each contact to the texture tensor.
! Note that the quantities calculated here are not the components of the texture tensor, 
! at least not yet, since they must be normalized by the number of contacts in each box.

               b_texture_xx_wnet(1,fra) = b_texture_xx_wnet(1,fra) + c_texture_xx(con,fra)
               b_texture_xy_wnet(1,fra) = b_texture_xy_wnet(1,fra) + c_texture_xy(con,fra)
               b_texture_yy_wnet(1,fra) = b_texture_yy_wnet(1,fra) + c_texture_yy(con,fra)

         endif

      enddo

   enddo

   do fra = 1, nfr ! for all the frames

! 3) Normalize the texture tensor for each box (as mentioned earlier, the components texture tensor
! must be normalized by the number of contacts in the box)

      b_texture_xx_wnet(1,fra) = b_texture_xx_wnet(1,fra) / DBLE(bnc_wnet(fra))
      b_texture_xy_wnet(1,fra) = b_texture_xy_wnet(1,fra) / DBLE(bnc_wnet(fra))
      b_texture_yy_wnet(1,fra) = b_texture_yy_wnet(1,fra) / DBLE(bnc_wnet(fra))

! 4) Calculate the principal components of the texture tensor and their directions

      a = b_texture_xx_wnet(1,fra)
      b = b_texture_xy_wnet(1,fra)
      c = b_texture_xy_wnet(1,fra)
      d = b_texture_yy_wnet(1,fra)

      if (b == 0) then ! a and d are the eigenvalues of the tensor

         if (a >= d) then

            b_texture_eigv11_wnet(1,fra) = a
            b_texture_eigv33_wnet(1,fra) = d

            b_dir_anisotropy_wnet(1,fra) = 0

            b_anisotropy(1,fra) = 2 * (a - d)

         else

            b_texture_eigv11_wnet(1,fra) = d
            b_texture_eigv33_wnet(1,fra) = a

            b_dir_anisotropy_wnet(1,fra) = pi/2

            b_anisotropy(1,fra) = 2 * (d - a)

         endif

      else

         lamda_1 = (a+d)/2 + (((4*b*c) + (a-d)**2)**0.5)/2
         lamda_2 = (a+d)/2 - (((4*b*c) + (a-d)**2)**0.5)/2

         lamda_1_y = 1
         lamda_1_x = b / (lamda_1 - a)

         b_texture_eigv11_wnet(1,fra) = lamda_1
         b_texture_eigv33_wnet(1,fra) = lamda_2
         b_anisotropy_wnet(1,fra) = 2 * (lamda_1 - lamda_2)
         b_dir_anisotropy_wnet(1,fra) = atan(lamda_1_y / lamda_1_x)

         if (b_dir_anisotropy_wnet(1,fra) < 0) then

            b_dir_anisotropy_wnet(1,fra) = pi + b_dir_anisotropy_wnet(1,fra)

         endif

      endif

   enddo
   
   open(1,file='./postpro/texture_tensor_wnet',form='formatted',status='unknown')
   open(2,file='./postpro/anisotropy_amplitude_wnet',form='formatted',status='unknown')
   open(3,file='./postpro/anisotropy_direction_wnet',form='formatted',status='unknown')

   do fra=1, nfr ! for all the frames

      write(1,*) fra,b_texture_xx_wnet(1,fra),b_texture_xy_wnet(1,fra),b_texture_yy_wnet(1,fra)
	  
      write(2,*) fra,b_anisotropy_wnet(1,fra)
	  
      write(3,*) fra,b_dir_anisotropy_wnet(1,fra)*180/pi
	  
   enddo

   close(1)
   close(2)
   close(3)

   return
end subroutine boxes_texture_wnet


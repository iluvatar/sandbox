!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! SR r_particles()
! Here, we read and assign the particles information registred in the
! 'parnam' and 'posnam' files.
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
subroutine r_particles2()
   implicit none 

! global variables
   include './glob_postpro'

! local variables
   integer :: par ! counter for the particles
   integer :: k, fra ! counters for the frames
   integer :: a, np_provisional
   double precision :: b

! Read the 'parnam' file. Normally, this is not a hystoric file since
! the number of particles is constant during all the the simulation.
   open(1, file=par_name, form='formatted', status='old')

   read(1,*) nl, nd, np
   read(1,*) 
   read(1,*) (ptype(par), par=1, np)
   read(1,*) (dr(par), par=1, np)
   read(1,*) (dgdom(par), par=1, np)

   close(1)

!   write(*,*) 'read the ',par_name,' file --> OK!'

! read the 'distot' file. This file contains the total (accumulated) x
! displacements of each particle, it has the same structure of a posh file

   open(1, file='./output/distot', form='formatted', status='old')

   fra = nfr_total
   k = fra
   
!   do k=1,nfr_total

!      if (k.eq.fr(fra)) then !only save the information in the frames
! that have been specified by the user.

         read(1,*)
         read(1,*) 
         read(1,*) (drxtot(par,fra), par=1, np)
         read(1,*) (drytot(par,fra), par=1, np)
         read(1,*) (drottot(par,fra), par=1, np)
         read(1,*)

         write(*,*) 'read the distot info, frame # ',k,' --> OK!'
         write(*,*) 'save frame # ',fra,' --> history file # ',fr(fra)

!         fra = fra + 1

!      else

!         read(1,*) 
!         read(1,*) 
!         read(1,*) (b, par=1, np)
!         read(1,*) (b, par=1, np)
!         read(1,*) (b, par=1, np)
!         read(1,*)

!         write(*,*) 'read the "distot" info, frame # ',k,' --> OK!'

!      endif

!   enddo

   close(1)


! read the 'pos_name' file

   open(1, file=pos_name, form='formatted', status='old')

   fra=1

   do k=1,nfr_total

      if (k.eq.fr(fra)) then !only save the information in the frames
! that have been specified by the user.

         read(1,*)
         read(1,*) 
         read(1,*) (drx(par,fra), par=1, np)
         read(1,*) (dry(par,fra), par=1, np)
         read(1,*) (drot(par,fra), par=1, np)
         read(1,*) (dvx(par,fra), par=1, np)
         read(1,*) (dvy(par,fra), par=1, np)
         read(1,*) (dvrot(par,fra), par=1, np)
         read(1,*) (dlive(par,fra), par=1, np)
         read(1,*)

         write(*,*) 'read the particles info, frame # ',k,' --> OK!'
         write(*,*) 'save frame # ',fra,' --> history file # ',fr(fra)

         fra = fra + 1

      else

         read(1,*) a, a, np_provisional
         read(1,*) a, b
         read(1,*) (b, par=1, np_provisional)
         read(1,*) (b, par=1, np_provisional)
         read(1,*) (b, par=1, np_provisional)
         read(1,*) (b, par=1, np_provisional)
         read(1,*) (b, par=1, np_provisional)
         read(1,*) (b, par=1, np_provisional)
         read(1,*) (a, par=1, np_provisional)
         read(1,*)

         write(*,*) 'read the particles info, frame # ',k,' --> OK!'

      endif

   enddo

   close(1)

   return
end subroutine r_particles2

SandBox2

FOLDERS STRUCTURE

./Examples/
     /PRE_Example
     /SHEAR_Example
./LIB/
./MAIN/
./POST/
./PRE/
./SHEAR/
./VAR/

HISTORY OF THIS VERSION

(01-10-2008) En esta versi�n tratamos de simplificar los archivos de entrada al programa y de volver m�s general la imposici�n de grados de libertad de los muros. Tambi�n eliminamos lo de las tres etapas de corte y lo volvimos una sola. Tambi�n suprimimos los programas GEO y BIAX y solamente dejamos SHEAR que es el que nos interesa para el trabajo con los medios cementados.

(05-08-2008) This is an arranged version for Ana Paulina Aguilar. In this version I deactivated all the instructions whose function was to visualize the results using the graphic libraries "vogl". The reason is that the vogl libraries do not seem to me to be very portable. Thus, the visualization of the results must be done using GMV.

(before 06-2008) In the novogl option i turned off the vogl visualisation (in the execution and post-processing phases) in order to make the program work in a 64 bit machine.

The only differences between /SabdBox_1a/ and /SabdBox_1b_NEM/ must be in the SHEAR program

The main diferences between /SabdBox_1a/ and /Ncody/ are:

-the folders structure
-SHEAR
-the option pre in the preprocessor

all programs compile with the Makefile file "Makefile_g77_xxb" (depends on the machine)

-all PRE options function correctly (options pre16, 17, 18, 19, 20 and 21).
-BIAX functions correctly when running stages 1 and 3 (stage 2 is bugged but it's not necesary, one can perfectly pass from stage 1 to stage 3).
-GEO funcions correctly.
-SHEAR functions correctly.
-BPOST functions correctly.
-GPOST functions correctly.
-SPOST functions correctly.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR newconld(i,jgd,j,ccr,cst,exc,kc,rdj,rxji,ryji,nxji,nyji,rrotj)

c     This routine creates a new contact according to the poral distance
c     between the a line and a disk

c>>>>>>>>1>>>>>>>>>2>>>>>>>>>3>>>>>>>>>4>>>>>>>>>5>>>>>>>>>6>>>>>>>>>7>>

      subroutine newconld(i,jgd,j,ccr,cst,exc,kc,
     &                    rdj,rxji,ryji,nxij,nyij,rrotj)

      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer i,j,js,jgd,inco,jnc,kcd,kc
      real*8 rdj,rrotj,rxji,ryji,rji,nxij,nyij
      real*8 dji,dnewji,alfaj
      logical cst,exc,ccr

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c       
      if (cst) then
c        initial behavior law
         js = 1
      else 
c        residual behavior law
         js = 2
      endif

c     when dealing with a contact between a wall and a disk, the
c     surface properties of the contact will always be those of the
c     wall. In pervious versions, a variable existed who decided which
c     properties (disk or wall) should be chosen. The name of this variable
c     was cwad(i,ks). In this version, we eliminated this variable.
      inco = wanc(i,js,ks)
      kcd = wacd(i,js,ks)
c         case structure
c         distance criteriun is defined by means of constant value
      if (newc(inco).eq.1) dnewji=dnewr(inco)
c         distance criteriun is defined by means of normalised value
      if (newc(inco).eq.2) dnewji=dnewn(inco)*rdj
      
        rji=dabs(nxij*rxji+nyij*ryji)
c       dji = distance between particle and wall
        dji = rji-rdj

c         write(*,*) 'En la lista de verlet'
c         write(*,*) 'i=',i,', j=',j,', dji=',dji,', dnewji=',dnewji
c         write(*,*) 'rx(i)=',rx(i),', ry(i)=',ry(i)
c         write(*,*) 'rx(j)=',rx(j),', ry(j)=',ry(j)

      if (dji.le.dnewji) then
c         create a new contact
          nc=nc+1
          if (nc.gt.ncmax) stop 'contact list too small'
          clist(nc)=j  
c         ccr = logical variable indicating if the contact is created
c               in the new contact list
          ccr=.TRUE.
          csta(nc)=cst
          nx(nc)=nxij
          ny(nc)=nyij

          dpor(nc) = dmax1(0.d0,dji)
		  
c         write(*,*) 'Se crea el contacto'
c         write(*,*) 'i=',i,', j=',j,', dji=',dji,', dnewji=',dnewji

c         locate  the contact point in wall 'i' and disk 'j' by means of
c         2 parameters: 
c         alfai = , and 'alfaj'. 
c         (nxij,nyij) is oriented from disk j to wall i

c         dani(nc) = distance between the origin of the wall and the
c                     contact point with the disk
        if ((i.eq.1).or.(i.eq.3)) then
            dani(nc)=rxji  ! horizontal distance
        else
            dani(nc)=ryji  ! vertical distance
        endif

          call angle(alfaj,nxij,nyij,1.d0)

          danj(nc)= alfaj-rrotj

c         calculation of cohesion distance ellipse

        if (neli(kcd).eq.0) then
            cax(nc) =.FALSE.
        else 
            cax(nc) =.TRUE.
c           case structure, neli(kcd)
          if (neli(kcd).eq.1) then
              axn(nc)=axnr(kcd)
              axt(nc)=axtr(kcd)
          endif
          if (neli(kcd).eq.2) then
c             the size of the ellipse is normalised only according to disk 'j'
              axn(nc)=axnn(kcd)*rdj
              axt(nc)=axtn(kcd)*rdj
          endif
        endif

        if (exc) then
c           save forces at existing contact
            fn(nc)=fnk(kc)
            ft(nc)=ftk(kc)
            fs(nc)=fsk(kc)
            if (ielas.eq.1) fel(nc)=felk(kc)       
        else
c           new contact in the list (gained)
            fn(nc)=0.D0
            ft(nc)=0.D0
            fs(nc)=0.D0
            if (ielas.eq.1) fel(nc)=0.D0
        endif
                             
      else

          ccr =.FALSE.

      endif

      return
      end                

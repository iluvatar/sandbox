c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR contact()

c     Update the contact list, local frames, relative velocities and
c     elastic forces linked to overlaping

c     This routine is modified in relation to routine 'gcontact.f'. 
c     It takes into account the history of persistent contacts:

c     If a contact already existed (in the previous contact list 
c     specified by kpoint and klist), then it will be lost when the interstitial
c     distance is greater than 'dcper'.
c     New contact only form when the two particles touch (delta < dcnew)

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine contact()
      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer i,j,jbeg,jend,jn,ti,tj
      integer k,kbeg,kend,kc,igd,jgd
      real*8 rxi,ryi,rroti,rxj,ryj,rrotj,rxcj,rycj,rxpcj,rypcj,rcj
      real*8 rdi,rdj,rdij,rxji,ryji,rji
      real*8 dtn,reli,relo,drel,rncj,rtcj,axti
      real*8 nxij,nyij,txij,tyij,vnij,vtij,vsij,vyij,vxij
      real*8 alfik,alfjk,alfji,alfel,calfi,salfi
      logical exc,ccr

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
        nc=0                      ! contact counter
      	  
      do i = 1, np-1
          cpoint(i)=nc+1
        if (live(i).eq.1) then
c           particle 'i' is alive: look for contacts in the Verlet list
            jbeg = ipoint(i)  
            jend = ipoint(i+1) - 1 
c           identification of contacts in the previous contact list    
            kbeg = kpoint(i)  
            kend = kpoint(i+1) - 1 
            k = kbeg
         
          if (jbeg.le.jend) then ! if there are neighbors
              ti=iobj(i)

            if (i.gt.nl) then
c               particle 'i' is a disk:
                igd = idgd(i)
            else
c               case structure: this normal vectors are constant
              if (i.eq.1) then
c                 normal to the base of the lid, oriented toward the disk
                  nxij=0.D0
                  nyij=-1.D0
              endif
              if (i.eq.2) then
c                 normal to the left vertical wall
                  nxij=-1.D0
                  nyij=0.D0
              endif
              if (i.eq.3) then
c                 normal to the horizontal top wall
                  nxij=0.D0
                  nyij=1.D0
              endif
              if (i.eq.4) then
c                 normal to the right vertical wall
                  nxij=1.D0
                  nyij=0.D0
              endif
            endif

c             Verlet list is such that at least particle 'j' is a disk                  
              rxi = rx(i)
              ryi = ry(i)
              rroti=rrot(i)
              rdi = rd(i)            

            do jn = jbeg,jend
                j = list(jn)     ! number of particle in the Verlet list
              if (live(j).eq.1) then
c                 particle 'j' is alive: test contact with 'i'
                  tj=iobj(j)

c                 find out the status of the potential contact:
c                 new contact / existing contact (cohesive or non cohesive)
c                 note that the contact list is a subset of the Verlet list, and
c                 both lists are ordered increaingly for particle 'i'
                  exc =.FALSE. ! contact status is suposed new
                if (j.eq.klist(k).and.k.le.kend) then
                    exc = .TRUE.
                    kc = k
                    k = k+1
                endif
                  
                  rxj = rx(j)
                  ryj = ry(j)
                  rrotj=rrot(j)
                  rdj=rd(j)   
                  rxji = rxj - rxi
c                 in semi-periodic boundary conditions,
c                 the sign of the complementary distance is opposite
                if (speri.and.(dabs(rxji).gt.rxw2)) then
                    if (rxji.gt.0.d0) then
                        rxji=rxji-rxw
                    else
                        rxji=rxji+rxw
                    endif
                endif
                  ryji = ryj - ryi

                  jgd = idgd(j)
                  
                if (ti.eq.1.and.tj.eq.1) then !d-d 
                    rdij=rdi+rdj
                    rji= dsqrt(rxji*rxji + ryji*ryji)
                    nxij =  -rxji / rji  
                    nyij =  -ryji / rji 

                  if (exc) then ! the contact exists in the previous list

                    if (caxk(kc)) then
c                       the contact is associated to a cohesion distance ellipse
c                       alfik, alfjk = anticlockwise angles of material points Mi, Mj 
c                                in disks i and j, associated with the contact
                        alfik=danik(kc)+rroti
                        alfjk=danjk(kc)+rrotj
c                       rxcj, rycj = coords. of vector (Oi,Mj)
                        rxcj=rxji+rdj*dcos(alfjk)
                        rycj=ryji+rdj*dsin(alfjk)
                        calfi=dcos(alfik)
                        salfi=dsin(alfik)
c                       rxpcj, rypcj = coords. of vector in local ref. frame (Oi,Mi)
                        rxpcj=calfi*rxcj+salfi*rycj
                        rypcj=-salfi*rxcj+calfi*rycj
                        rcj=dsqrt(rxpcj*rxpcj+rypcj*rypcj)
c                       alfji = positive angle between line (Oi,Mj) and axis (Oi,Mi)
                        call angle(alfji,rxpcj,rypcj,rcj)
                        if (alfji.gt.pi) alfji=pit2-alfji
c                       relo = distance between Oi and ellipse's origin
c                       dpork(kc) = shift distance of ellipse's origin parallel to (Oi,Mi) 
                        relo=rdi+dpork(kc)
                        alfel=axtk(kc)/relo
                      if (alfji.le.alfel) then
                          dtn=dabs(alfji)/alfel  ! alfel.ne.0
                          reli=relo+axnk(kc)*dsqrt(1.d0-dtn*dtn)
                        if (rcj.le.reli) then
c                           the cohesive contact persists
                            ccr=.TRUE.
                        else
c                           the cohesive contact is broken
                            ccr = .FALSE.
                        endif
                      else
c                         the contact point is outside the ellipse angle zone 
                          ccr = .FALSE.
                      endif

                      if (ccr) then
c                         transfer persistent contact data
                          call cdatrans(j,kc,nxij,nyij) 
                      else
c                         new potential contact is analised according  
c                         to a residual behavior law
                          call newcondd(igd,jgd,j,ccr,.FALSE.,exc,kc,
     &                                  rdij,rji,nxij,nyij,rroti,rrotj)
                      endif
                    else						
c                       contact law is not associated to an ellipse. Note 
c                       that if ccr=.TRUE., the created contact is persistent	  	 

                        call newcondd(igd,jgd,j,ccr,cstak(kc),exc,kc,
     &                                rdij,rji,nxij,nyij,rroti,rrotj)

                      if ( (cstak(kc)).and.(.not.ccr) ) then
c                         the initial contact is broken; thus, test contact 
c                         according to residual law.
c                         Most probably the potential contact is not created
c                         since residual distance is smaller than initial
                          call newcondd(igd,jgd,j,ccr,.FALSE.,exc,kc,
     &                                  rdij,rji,nxij,nyij,rroti,rrotj)
                      endif
					  
                    endif
                  else
c                     the potential contact is new   
                      call newcondd(igd,jgd,j,ccr,.FALSE.,exc,kc,
     &                              rdij,rji,nxij,nyij,rroti,rrotj)
                  endif
				  
                  if (ccr) then
				  
c                     calculate relative velocities at contact point
c                     Relative velocities in the new local frame  
                     
                     txij = nyij
                     tyij = -nxij   
                     
                     vxij = vx(i) - vx(j)
                     vyij = vy(i) - vy(j)
c verificar distancia radial para velocidades relativas (diferente del radio de la particula)
                     vnij = vxij*nxij +vyij*nyij
                     vtij = vxij*txij +vyij*tyij+rdi*vrot(i)+rdj*vrot(j)
                     vsij = vrot(i)-vrot(j)
                     
                     vn(nc)=vnij
                     vt(nc)=vtij   
                     vs(nc)=vsij  
                  endif
                endif

                if (ti.eq.2.and.tj.eq.1) then   ! l-d 
                  if (exc) then ! the contact exists in the previous list

                    if (caxk(kc)) then
c                       the contact is associated to a cohesion distance ellipse
c                       alfjk = anticlockwise angle of material points Mj 
c                                in disk j, associated with the contact
                        alfjk=danjk(kc)+rrotj
c                       rxcj, rycj = coords. of vector (Oi,Mj)
                        rxcj=rxji+rdj*dcos(alfjk)
                        rycj=ryji+rdj*dsin(alfjk)
                      if ((i.eq.1).or.(i.eq.3)) then
                          rxcj=rxcj-danik(kc)  ! shift horizontal distance
                      else
                          rycj=rycj-danik(kc)  ! shift vertical distance
                      endif
c                       rncj,rtcj=coords of (Oi,Mj), normal/parallel to wall i
c                       note that (nxij,nyij) points outward
                        rncj=-rxcj*nxij-rycj*nyij
                        rtcj=dabs(-rxcj*nyij+rycj*nxij)

                        axti=axtk(kc)

                      if (rtcj.le.axti) then
                          dtn=rtcj/axti  ! axti>0
                          drel=axnk(kc)*dsqrt(1.d0-dtn*dtn)
                          reli =dpork(kc)+drel
                        if (rncj.le.reli) then
c                           the cohesive contact persists
                            ccr = .TRUE.
                        else
c                           the cohesive contact is broken
                            ccr = .FALSE.
                        endif
                      else
c                         the contact point is outside the ellipse zone 
                          ccr = .FALSE.
                      endif

                      if (ccr) then
c                         transfer persistent contact data
                          call cdatrans(j,kc,nxij,nyij) 
                      else
c                         new potential contact is analised according  
c                         to a residual behavior law 
                          call newconld(i,jgd,j,ccr,.FALSE.,exc,kc,
     &                              rdj,rxji,ryji,nxij,nyij,rrotj)
                      endif
                    else
c                       contact law is not associated to an ellipse
                        call newconld(i,jgd,j,ccr,cstak(kc),exc,kc,
     &                             rdj,rxji,ryji,nxij,nyij,rrotj)
                      if ( (cstak(kc)).and.(.not.ccr) ) then
c                         the initial contact is broken; thus, test contact 
c                         according to residual law.
c                         Most probably the potential contact is not created
c                         since residual distance is greater than initial
                          call newconld(i,jgd,j,ccr,.FALSE.,exc,kc,
     &                              rdj,rxji,ryji,nxij,nyij,rrotj)
                      endif
                    endif
                  else
c                     the potential contact is new   
                      call newconld(i,jgd,j,ccr,.FALSE.,exc,kc,
     &                              rdj,rxji,ryji,nxij,nyij,rrotj)
                  endif
                  if (ccr) then
c                     calculate relative velocities at contact point
c                     Relative velocities in the new local frame :
c                     note that walls only translate and do not rotate
c                     (line rotation would induce normal velocity
c                     at the contact)
                     
                      txij = nyij
                      tyij = -nxij   
                    
                      vxij = vx(i) - vx(j)
                      vyij = vy(i) - vy(j)
                      vnij = vxij*nxij + vyij*nyij
                      vtij = vxij*txij + vyij*tyij + rdj*vrot(j)
                      vsij = -vrot(j)
                     
                      vn(nc)=vnij
                      vt(nc)=vtij   
                      vs(nc)=vsij  
                  endif
                endif

              endif
            enddo
          endif
        endif
      enddo

        cpoint(np)=nc+1

c       if elastic interaction is not considered then
      if ((ielas.eq.0).and.(ns.eq.nsi+1)) then
c         initialize elastic forces in contacts in the first loop
        do i=1,ncmax
            fel(i)=0.D0
        enddo
      endif

      return
      end
      
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      include '../SANDBOX/cdatrans.f'
      include '../SANDBOX/angle.f'

      

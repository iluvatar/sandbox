c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  walbc.f
  
c  Define wall boundary conditions (position, kinematics, forces,...)
c  for numerical biaxial experiment
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine walbc()
      implicit none
      include '../VAR/mpglob'

c-----Local variables

      integer i
	  real*8 rymax


c       the lid is in contact with the 'highest' disk :
       rymax=0.d0
       do i=nl+1,np
         if (ry(i)+rd(i).gt.rymax) rymax=ry(i)+rd(i)
       enddo
c       ry(3)=rymax   

c      some usefull quantities:

       rxw=rx(4)-rx(2)
       rxw2=rxw/2.d0
       ryw=ry(3)-ry(1)
       rxor=rx(1)
       ryor=ry(1)
      
c     Semiperiodic boundary conditions:

      if (speri) then
c         only the basal plane and the lid are alive - the vertical walls are eliminated
         live(2)=0
         live(4)=0
      elseif (.not.speri) then
         live(1)=1
         live(2)=1
         live(3)=1
         live(4)=1
      endif

c-----Driven degrees of freedom of the walls

c-----Walls are defined as follows:
c		1 : Bottom wall 
c		2 : Left wall
c		3 : Top wall
c		4 : Right wall

c     first, take care of the x direction
      do i=1,4 ! for the four walls
         if (idfx(i).eq.0) then ! the wall is force controlled in x
		    vxe(i)=0.d0
		    if (idfunx(i).eq.0) then ! the forces are calculated using a harmonic function
			   fxe(i)=cnst1x(i)+amp_x(i)*cos(phse_x(i))
            elseif (idfunx(i).eq.1) then ! the forces are calculated using a ramp function
			   if (tmx(i).eq.0.d0) then
			      fxe(i)=cnst3x(i)
			   elseif (tmx(i).ne.0.d0) then
                  fxe(i)=cnst2x(i)
			   endif
			endif
         elseif (idfx(i).eq.1) then ! the wall is velocity controlled in x
		    fxe(i)=0.d0
		    if (idfunx(i).eq.0) then ! the velocities are calculated using a harmonic function
			   vxe(i)=cnst1x(i)+amp_x(i)*cos(phse_x(i))
            elseif (idfunx(i).eq.1) then ! the velocities are calculated using a ramp function
			   if (tmx(i).eq.0.d0) then
			      vxe(i)=cnst3x(i)
			   elseif (tmx(i).ne.0.d0) then
                  vxe(i)=cnst2x(i)
			   endif
			endif
         endif
      enddo
	  
c     then, take care of the y direction
      do i=1,4 ! for the four walls
         if (idfy(i).eq.0) then ! the wall is force controlled in y
		    vye(i)=0.d0
		    if (idfuny(i).eq.0) then ! the forces are calculated using a harmonic function
			   fye(i)=cnst1y(i)+amp_y(i)*cos(phse_y(i))
            elseif (idfuny(i).eq.1) then ! the forces are calculated using a ramp function
			   if (tmy(i).eq.0.d0) then
			      fye(i)=cnst3y(i)
			   elseif (tmy(i).ne.0.d0) then
                  fye(i)=cnst2y(i)
			   endif
			endif
         elseif (idfy(i).eq.1) then ! the wall is velocity controlled in y
		    fye(i)=0.d0
		    if (idfuny(i).eq.0) then ! the velocities are calculated using a harmonic function
			   vye(i)=cnst1y(i)+amp_y(i)*cos(phse_y(i))
            elseif (idfuny(i).eq.1) then ! the velocities are calculated using a ramp function
			   if (tmy(i).eq.0.d0) then
			      vye(i)=cnst3y(i)
			   elseif (tmy(i).ne.0.d0) then
                  vye(i)=cnst2y(i)
			   endif
			endif
         endif
      enddo
	  
c     then, take care of the y direction rotational degrees of freedom, which are velocity controlled
c     for the 4 walls
      do i=1,4
		 idfrot(i)=1
		 frote(i)=0.d0
		 vrote(i)=0.d0
      enddo

      return
      end




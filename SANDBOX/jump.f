c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR jump()

c     Data transfer and updates at run time
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine jump()
      implicit none
      include '../VAR/mpglob'

c-----Local variables
      integer i,j
      real*8 force,E,f_max,f_mean,E_max,E_ref,d_mean
      logical crit
      
c-----Update externally imposed FORCES and Velocities; asign new external
c     velocities to the system.

      call drive()
      
C-----reset flow variables 
      
      if (mod(ns-(nsi+1),nread).eq.0) then
         open(1,file='./input/flow',status='unknown')

         read(1,*)
         read(1,*) irun
         read(1,*) iread,nread
         read(1,*) witer,nwiter

         if(irun.eq.0) then
            stop 'No-execution start mode. Change mode!'      
         end if

         close(1)
         
c-----Check if the stabilization criteria is fullfiled

         f_max=0.
	     f_mean=0.
	     E_max=0.
	     E_ref=0.
	     d_mean=0.
	     crit=.FALSE.

         do i=5, np
	        force=dsqrt((fx(i)**2)+(fy(i)**2))
	        E=mass(i)*((vx(i)**2)+(vy(i)**2))
	        if (force.gt.f_max) then
		       f_max=force
		    endif
		    if (E.gt.E_max) then
		       E_max=E
		    endif
		    d_mean=d_mean+2.D0*rd(i)
	     enddo

         do j=1,nc
	        f_mean=f_mean + dsqrt((fn(j)**2)+(ft(j)**2))
         enddo
	  
	     d_mean=d_mean/DBLE(np-4)
 	     f_mean=f_mean/DBLE(nc)
	     E_ref=f_mean*d_mean 

         write(30,*) tm,f_max,f_mean,f_max/f_mean
	     write(31,*) tm,E_max,E_ref,E_max/E_ref
	     
c	     if((E_max/E_ref).lt.(4.D-4)) then
c	        crit=.TRUE.
c	     endif
	  
c	     if((E_max).lt.(1.75D-5)) then
c	        crit=.TRUE.
c	     endif

c         if((rx(3)).gt.(0.05)) then
c		    crit=.TRUE.
c		 endif
	  
c         if((tm.gt.(tmx(3)+1.D0)).and.(tm.gt.(tmy(3)+1.D0))
c     &       .and.(crit)) then
c            stop 'The stabilization criteria has been fullfiled!'      
c         endif

         if(crit) then
            stop 'The stabilization criteria has been fullfiled!'      
         endif
	  
      endif
      
      return
      end
      
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      include '../SANDBOX/drive.f' 

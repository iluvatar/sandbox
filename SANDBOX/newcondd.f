c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR newcondd(igd,jgd,j,ccr,cst,exc,kexc,rdij,rji,nxij,nyij,rroti,rrotj)

c     This routine creates a new contact according to the poral distance
c     between the 2 disks. 

c>>>>>>>>1>>>>>>>>>2>>>>>>>>>3>>>>>>>>>4>>>>>>>>>5>>>>>>>>>6>>>>>>>>>7>>

      subroutine newcondd(igd,jgd,j,ccr,cst,exc,kc,
     &                    rdij,rji,nxij,nyij,rroti,rrotj)

      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer igd,jgd,j,inco,jnc,icd,jcd,kc,js
      real*8 rdij,rji,nxij,nyij,rroti,rrotj
      real*8 dji,dnewi,dnewj,dnewji,alfai,alfaj
      logical cst,exc,ccr

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c       
      if (cst) then
c        initial behavior law
         js = 1
      else 
c        residual behavior law
         js = 2
      endif

        inco = gdnc(igd,js,ks)
        icd = gdcd(igd,js,ks)
        jnc = gdnc(jgd,js,ks)
        jcd = gdcd(jgd,js,ks)

c       case structure
c       distance criteriun is defined by means of constant value
        if (newc(inco).eq.1) dnewi=dnewr(inco)
c       distance criteriun is defined by means of normalised value
        if (newc(inco).eq.2) dnewi=dnewn(inco)*rdij

c       dji = distance between the two particles, 
        dji = rji-rdij

      if (igd.eq.jgd) then
c         if both particles belong to the same geological domain
          dnewji=dnewi
      else ! particles are located in different geological domains
c         case structure
          if (newc(jnc).eq.1) dnewj=dnewr(jnc)
          if (newc(jnc).eq.2) dnewj=dnewn(jnc)*rdij

c         case structure:
c         ilaymp(ks) = inter-layer mechanical properties
c         ilaymp(ks) = 1 : according to minimum parameter
          if (ilaymp(ks).eq.1) dnewji=dmin1(dnewi,dnewj)
c         ilaymp(ks) = 2 : according to mean parameters
          if (ilaymp(ks).eq.2) dnewji=(dnewi+dnewj)/2.d0
c         ilaymp(ks) = 3 : according to maximum parameter
          if (ilaymp(ks).eq.3) dnewji=dmax1(dnewi,dnewj)

      endif

      if (dji.le.dnewji) then

c         create a new contact
          nc=nc+1
          if (nc.gt.ncmax) stop 'contact list too small'
c         ccr = logical variable indicating if a contact is created
c               in the contact list
          ccr=.TRUE.
          clist(nc)=j  
          csta(nc)=cst
          nx(nc)=nxij
          ny(nc)=nyij

          dpor(nc) = dmax1(0.d0,dji)

c         determine distance parameters for contact elipse
          call celipsdd(igd,jgd,icd,jcd,rdij)

        if (cax(nc)) then ! contact law is associated to an ellipse

c           locate  the contact point in disks 'i' and 'j' by means of
c           2 angles 'alfai', and 'alfaj'. 
c           (nxij,nyij) is oriented from particle j to particle i
            call angle(alfaj,nxij,nyij,1.d0)

          if (alfaj.gt.pi) then
              alfai=alfaj-pi
          else
              alfai=alfaj+pi
          endif
                           
            dani(nc)= alfai-rroti  
            danj(nc)= alfaj-rrotj
        endif
        if (exc) then
c           save forces at existing contact
ccc         These values may be initialised taking account of the contact history
            fn(nc)=fnk(kc)
            ft(nc)=ftk(kc)
            fs(nc)=fsk(kc)
            if (ielas.eq.1) fel(nc)=felk(kc)       
        else
c           new contact in the list (gained)
            fn(nc)=0.D0
            ft(nc)=0.D0
            fs(nc)=0.D0
            if (ielas.eq.1) fel(nc)=0.D0
        endif
             
      else

          ccr =.FALSE.

      endif

      return
      end

      
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      include '../SANDBOX/celipsdd.f'




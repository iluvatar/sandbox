c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
c     SR fconwd()
      
c     Compute contact forces resultants on the 4 walls of the box
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      subroutine fconwd()
      implicit none
      include '../VAR/mpglob'
      
      
c     Local variables
      
      real*8 fxij,fyij,fnij,ftij,muij
      real*8 nxij,nyij,txij,tyij,omdel,opdel   
      integer i,jbeg,jend,jn
      
c-----Contact forces
      
      do i = 1, nl

         fcwx(i)=0.D0
         fcwy(i)=0.D0

         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1     
            
            do jn = jbeg,jend

c              fn(jn), ft(jn) = normal/shear forces applied by disk
c              'j' to wall 'i' in local reference frame. Local frame : (n,t),
c              n points from the disk to the wall, 
               fnij=fn(jn)
               ftij=ft(jn)
               
               nxij=nx(jn)
               nyij=ny(jn)
               txij=nyij
               tyij=-nxij
               
               fxij = fnij * nxij + ftij * txij
               fyij = fnij * nyij + ftij * tyij
               
               fcwx(i)= fcwx(i) + fxij
               fcwy(i)= fcwy(i) + fyij
			   
c			   if (i.eq.3) then
c			      write(*,*) 'jn=',jn,' nx(jn) (0.)=',nx(jn),' ny(jn) (1.)=',ny(jn)
c				  write(*,*) 'fcwx(i)=',fcwx(i)
c			   endif

            enddo             
         
      enddo  


c      calculate information concerning the contacts inside the model
  

         omdel=0.999999d0
         opdel=1.000001d0
         nslcoc=0
         nslcoa=0
         nrococ=0
         nrocoa=0
         ncfn0=0
         fnmax=fn(cpoint(nl+1))
         fnmin=fnmax

      do i = nl+1,np-1

         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1

            
            do jn = jbeg,jend

c              fn(jn), ft(jn) = normal/shear forces applied by disk
c              'j' to wall 'i' in local reference frame. Local frame : (n,t),
c              n points from the disk to the wall, 
               fnij=fn(jn)
               ftij=ft(jn)
               muij=mu(jn)

               fnmax=dmax1(fnmax,fnij)
               fnmin=dmin1(fnmin,fnij)

              if (fnij.ne.0.d0) then

               if ( dabs(ftij).le.(muij*omdel)*fnij ) then
c                  the contact is rolling
                  if (ftij.ge.0.d0) then
c                   shear stress is in the clockwise direction
                    nrococ=nrococ+1
                  else
c                   shear stress is in the anti-clockwise direction
                    nrocoa=nrocoa+1
                  endif
               else
c                  the contact is slipping
                  if (ftij.ge.0.d0) then
                   if ( (fnij.lt.0.d0).or.
     &                 (ftij.le.(muij*opdel)*fnij) ) then
c                      the contact is slipping in clockwise direction (right-lateral)
                       nslcoc=nslcoc+1
                   endif
                  endif

                  if (ftij.lt.0.d0) then
                   if ( (fnij.lt.0.d0).or.
     &                 (ftij.ge.-(muij*opdel)*fnij) ) then
c                     the contact is slipping in anti-clockwise direction (left-lateral)
                      nslcoa=nslcoa+1
                   endif
                  endif

               endif

              else
c                ncfn0 = number of contacts with no normal force
                 ncfn0=ncfn0+1

              endif

            end do             
         
      enddo  
             
      
      return
      end
      

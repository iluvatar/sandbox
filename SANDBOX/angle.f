c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR angle(alfa,x,y,r)

c     calculate the antoclockwise angle [0,2 pi] of a vector with
c     magnitude r and coordinates (x,y)

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine angle(alfa,x,y,r)
      implicit none
      include '../VAR/mpglob'
      
c     Local variables

      real*8 alfa,x,y,r
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      if (dabs(x).le.r) then

          alfa=dacos(x/r)

          if (y.lt.0.d0) alfa=pit2-alfa

      else

        if (x.gt.r) then

            alfa = 0.d0

        else

            alfa = pi

        endif

      endif

      return
      end

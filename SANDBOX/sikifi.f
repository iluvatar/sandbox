c==============================================================
c     SR  sikifi
c-----
c     Set initial kinematic state of the system from  final state in previous stages.
c     Initialize history and stress/strain files. This routine is called in
c     bstand.f. It may be optimized by using structured files (i.e. fortran 90).

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine  sikifi()
 
      implicit none
      include '../VAR/mpglob'

c-----local variables

      integer i

c     read particle information

       open(1,file=f_pari,status='unknown')
       call rpar(1)
       close(1)

c      position and contacts are calculated from final state in previous stage
c      in other words, the program is executed from the beginning
       open(1,file=f_posi,status='unknown')
       call rpos(1)
       close(1)

      if (icon.eq.0) then ! do not read a contacts file --> all contacts will be new
         sol=.TRUE. ! we will call solid() in look()
         do i=1,np
            kpoint(i)=1
         enddo
      else ! icon=1 and an initial contact file must be read
         sol=.FALSE. ! we will not call solid() in look() since all contacts are old
         open(1,file=f_coni,status='unknown')
		 call rcon(1)
         close(1)
c       Make copies of contact list and contact forces
         call savefor()
      endif

c     if elastic interactions are not considered then set elastic forces to zero
      if (ielas.eq.0) then
        do i=1,ncmax
            fel(i)=0.D0
        enddo
      else
c         Elastic parameters
          eta=eoa
          etb=eob
          eabo=eob-eoa
          eab=eabo
          efi=pk*(2.D0*rdmean)**pler/dfloat(nefi) ! elastic force increment
          if (ieoab.eq.3) npef=anint(float(np)*pef)
          write(*,*) ' npef = ',npef,' efi = ',efi
      endif

c-----Initialize history files

      if (ihis.eq.1) then    
         open(12,file=f_posh,status='unknown')
         open(13,file=f_conh,status='unknown')
         open(33,file='./output/distot',status='unknown')   
      endif 

c-----initialize the total displacements of all the particles

      do i=nl+1,np
        xtot(i)=0.
        ytot(i)=0.
        rottot(i)=0.
      enddo

c      open file containning iteration and overlap information 
	  open(16,file='./output/term_output',status='unknown')   
c	  open(17,file='f_ovlp',status='unknown') 

	  open((18),file='./output/wallx1',status='unknown')
	  open((19),file='./output/wallx2',status='unknown')
	  open((20),file='./output/wallx3',status='unknown')
	  open((21),file='./output/wallx4',status='unknown')

	  open((22),file='./output/wally1',status='unknown')
	  open((23),file='./output/wally2',status='unknown')
	  open((24),file='./output/wally3',status='unknown')
	  open((25),file='./output/wally4',status='unknown')

	  open(30,file='./output/forces',status='unknown')   
	  open(31,file='./output/kinetic_energy',status='unknown')   

c        Time and step number initialisation
      nsi=0   ! defined initial number of step

c        ks indicates the number of phase in the numerical experiment
c        For geo.f: 1 / 2 = relaxation / erosion;

      ks=1

c        sol = logical varible indicating if routine solid.f is called
c      sol=.TRUE.

      tm=0.D0 ! this value is used in bstep.f which calls bdrive.f

      return
      end


c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      include '../SANDBOX/savefor.f'

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR mecparld(jn,i,jgd,rdj)

c     This routine determinesthe mechanical parameters at a disk-disk contact:
c     restitution coeficients, extension and slment strength.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine mecparld(jn,i,jgd,rdj)
      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer jn,i,jgd,js,krc,kmp
      real*8 rdj

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      if (csta(jn)) then
c         initial behavior law
          js = 1
      else 
c         residual behavior law
          js = 2
      endif

c     when dealing with a contact between a wall and a disk, the
c     surface properties of the contact will always be those of the
c     wall. In pervious versions, a variable existed who decided which
c     properties (disk or wall) should be chosen. The name of this variable
c     was cwad(i,ks). In this version, we eliminated this variable.
		krc = warc(i,js,ks)
		kmp = wamp(i,js,ks)

        en(jn)=enl(krc)
        et(jn)=etl(krc)
        es(jn)=esl(krc)
        mu(jn)=mul(kmp)

c       strength parameters at the contact are defined according to
      if (cstr(kmp).eq.1) then
c         real (constant) values
          ex(jn)=exlr(kmp)
          sl(jn)=sllr(kmp)
      else
c         normalised values times ( 2 rdj): this may be interpreted
c         in terms of a reflexion of particles in contact with the wall
          ex(jn)=exln(kmp)*2.d0*rdj
          sl(jn)=slln(kmp)*2.d0*rdj
      endif

      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR sit()

c     End the program
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine sit()
      implicit none
      include '../VAR/mpglob'

      integer i
	  
c-----Close history files and others

      if(ihis.ne.0) then
c       close history files
        close(12)
        close(13)
      endif

c     close file containning contact information along the walls 

      do i=1,4
	     close(17+i)
         close(17+i)
      enddo

c       close files containning iteration and overlap data
 
        close(16)

        close(30)
        close(31)

      return
      end



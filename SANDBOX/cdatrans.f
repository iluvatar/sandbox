c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR cdatrans(j,kc,nxij,nyij)

c     This routine transfers data associated to a persistent contact

c>>>>>>>>1>>>>>>>>>2>>>>>>>>>3>>>>>>>>>4>>>>>>>>>5>>>>>>>>>6>>>>>>>>>7>>

      subroutine cdatrans(j,kc,nxij,nyij)

      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer j,kc
      real*8 nxij,nyij

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c       transfer persistent contact data
        nc=nc+1
        if (nc.gt.ncmax) stop 'contact list too small'

        clist(nc)=j     
        csta(nc) = cstak(kc)
        dani(nc) = danik(kc)
        danj(nc) = danjk(kc)
        dpor(nc) = dpork(kc)
        cax(nc) = caxk(kc) 
      if (cax(nc)) then
          axn(nc) = axnk(kc)
          axt(nc) = axtk(kc)
      endif
        
        nx(nc)=nxij
        ny(nc)=nyij

c       save forces at existing contact
        fn(nc) = fnk(kc)
        ft(nc) = ftk(kc)
        fs(nc) = fsk(kc)
        if (ielas.eq.1) fel(nc)=felk(kc)       

      return
      end

      

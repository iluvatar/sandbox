c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR step()

c     Update time, positions and velocities for one step 
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine step()
      implicit none
      include '../VAR/mpglob'

c     local variables
      
      integer i
      real*8 massi

c     Update time from intermediate (half) step to the whole step

      tm=tm+dt

c     Positions and velocities of walls / disks

c     Update at every step the positions and velocities for the walls

      do i= 1,nl
c       Update positions of the walls
        if (live(i).eq.1) then
            rx(i)=rxk(i)+vxk(i)*dt
            ry(i)=ryk(i)+vyk(i)*dt
            rrot(i)=rrotk(i)+vrotk(i)*dt
c       Compute new velocities for walls that are FORCE controlled.
            massi=mass(i)
            if (idfx(i).eq.0) vx(i)=vxk(i)+dt*fx(i)/massi
            if (idfy(i).eq.0) vy(i)=vyk(i)+dt*fy(i)/massi
            if (idfrot(i).eq.0) vrot(i)=vrotk(i)+dt*frot(i)/mom(i)
        endif
      enddo
      
c-----Update positions and velocities for the disks

      do i=nl+1,np
       if (live(i).eq.1) then
c        Update positions of the disks
         rx(i)=rxk(i)+vxk(i)*dt
        if (speri) then
c           in semi-periodic boundary conditions,
c           the complementary distance is calculated
            if (rx(i).gt.rxw) rx(i)=rx(i)-rxw
            if (rx(i).lt.0.D0) rx(i)=rx(i)+rxw
        endif
         ry(i)=ryk(i)+vyk(i)*dt
         rrot(i)=rrotk(i)+vrotk(i)*dt
c        notice that all disks are force controlled 
         massi=mass(i)
         vx(i)=vxk(i)+dt*fx(i)/massi
         vy(i)=vyk(i)+dt*fy(i)/massi
         vrot(i)=vrotk(i)+dt*frot(i)/mom(i)
       endif
      enddo

c-----Calculate the total displacements of all the particles

      do i=nl+1,np
         xtot(i)=xtot(i)+vxk(i)*dt
         ytot(i)=ytot(i)+vyk(i)*dt
         rottot(i)=rottot(i)+vrotk(i)*dt
      enddo

c-----Compute new velocities for particles that are VELOCITY controlled

C     In geo.f the only particles that are velocity
c     controlled are the walls. Wall velocities are constant.
c     Note that in 'gsavepos' (in glook.f) the particles are freezed once
c     relaxation has ended, to decrease 'temperature' of the system
c       
      
      return
      end
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


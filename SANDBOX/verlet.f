c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR verlet()

c     Update the Verlet list
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine verlet()
      implicit none
      include '../VAR/mpglob'


c-----local variables

      integer i,j,ti,tj
      real*8 rxij,ryij,dij,rdij

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      nlist = 0
      
      if (np.gt.1) then
         
         do i = 1, np - 1  

            ipoint(i) = nlist + 1
          if (live(i).eq.1) then
c           particle 'i' is alive: look for neighbours
            ti=iobj(i)
            
            do j = i + 1, np

               tj=iobj(j)

             if ( (live(j).eq.1).and.(tj.eq.1) ) then
c              particle 'j' is alive and its a disk : test neighbourhood with 'i'
        
                  rxij = dabs(rx(i) - rx(j))

c                 in semi-periodic boundary conditions,
c                 consider the complementary distance
                  if (speri.and.(rxij.gt.rxw2)) rxij=rxw-rxij

                  ryij = dabs(ry(i) - ry(j))

              if (ti.eq.2) then ! l-d

c               distance between horizontal wall and particle
                if ( (i.eq.1).or.(i.eq.3) ) dij=ryij-rd(j)
c               distance between left vertical wall and particle
                if ( (i.eq.2).or.(i.eq.4) ) dij=rxij-rd(j)

               if (dij.lt.dver) then
                 nlist = nlist + 1
                 list(nlist) = j
               end if

              else ! d-d

                rdij=rd(i)+rd(j)

c-----          Comparison to reduce run time (eliminate distant particles) 
               if (rxij-rdij.le.dver) then
                if (ryij-rdij.le.dver) then

                  dij= dsqrt(rxij*rxij + ryij*ryij)-rdij

                 if (dij.lt.dver) then
                   nlist = nlist + 1
                   if (nlist.eq.nvmax) stop 'verlet list too small'
                   list(nlist) = j
                 endif

                endif
               endif
              endif


             endif
            enddo
          endif
         enddo
      endif
      
       ipoint(np) = nlist+1

      return
      end
      

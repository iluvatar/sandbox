c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR contprop()


c     program geo.f

c-----Arrays of contact properties and coefficients: reduced masses and
c     mechanical properties of contacts (restitution, friction, adhesion ...)

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine contprop()

      implicit none
      include '../VAR/mpglob'

c-----Local variables

      integer i,j,ti,tj,igd,jgd
      integer jbeg,jend,jn
      real*8 massi,massj,momi,momj,rdi,rdj,rdij

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      do i = 1, np-1 
       if (live(i).eq.1) then
c        particle 'i' is alive: look for contacts
         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1     
         
         if (jbeg.le.jend) then ! if there are contacts
            ti=iobj(i)
            massi=mass(i)
            momi=mom(i)        
            rdi=rd(i)
c           mechanical properties of particle 'i'
            igd=idgd(i)
            
            do jn = jbeg,jend
               j = clist(jn) 
               tj=iobj(j)
               massj=mass(j)
               momj=mom(j) 
               rdj=rd(j)
               jgd=idgd(j)

c-----         Reduced masses 
               
               mn(jn)=massi*massj/(massi+massj)
               mt(jn)=1.d0/(1.d0/massi+1.d0/massj+rdi**2/momi
     &              +rdj**2/momj)
               ms(jn)=momi*momj/(momi+momj)
               
c-----         Friction and restitution coefficients, cohesion
               
               if (ti.eq.1.and.tj.eq.1) then ! d-d
                   rdij=rdi+rdj
                   call mecpardd(jn,igd,jgd,rdij)

               endif
               
               if (ti.eq.2.and.tj.eq.1) then   ! l-d    

                   call mecparld(jn,i,jgd,rdj)
               
               endif

            enddo               ! for j 
         endif                  ! for neighbour-at-all condition
       endif
      enddo                     ! for loop over pairs

      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      include '../SANDBOX/mecpardd.f'
      include '../SANDBOX/mecparld.f'

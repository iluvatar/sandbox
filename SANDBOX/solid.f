c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR solid()

c     This routine creates a new contact list in which all contacts are 
c     defined by means of a cohesive behavior law. Contacts are supposed 
c     to be new, which means that cementation between grains ensures
c     the cohesion behavior.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine solid()
      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer i,j,jbeg,jend,jn,kbeg,kend,k,kc
      integer igd,jgd,ti,tj
      real*8 rxi,ryi,rroti,rxj,ryj,rrotj
      real*8 rdi,rdj,rdij,rxji,ryji,rji
      real*8 nxij,nyij,txij,tyij
      logical exc,ccr

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        sol=.FALSE.
        nc=0                      ! contact counter
     
      do i = 1, np-1
          cpoint(i)=nc+1
        if (live(i).eq.1) then
c           particle 'i' is alive: look for contacts in the Verlet list
            jbeg = ipoint(i)  
            jend = ipoint(i+1) - 1 
			
c			write(*,*) 'i',i,'jbeg',jbeg,'jend',jend
c			read(*,*)
			
c           identification of contacts in the previous contact list    
            kbeg = kpoint(i)  
            kend = kpoint(i+1) - 1 
            k = kbeg
         
          if (jbeg.le.jend) then ! if there are neighbors
              ti=iobj(i)
c             idgd(i) = identification of geological domain linked to particle 'i'
c             gdnc(idgd(i),1,ks) = index indicating the position of the new
c                 contact criterium in the corresponding arrays:
c                 newc(inc), dnewr(inc), dnewn(inc) 
c             ks = index indicating the number of phase in the numerical experiment
c                  For geo.f: 1 / 2 = relaxation / erosion;
c             Second index 1 / 2 identifies initial cohesive/residual behavior law
            if (i.gt.nl) then
c               particle 'i' is a disk
                igd = idgd(i)
            else
c               case structure: this normal vectors are constant
              if (i.eq.1) then
c                 normal to the base of the lid, oriented toward the disk
                  nxij=0.D0
                  nyij=-1.D0
              endif
              if (i.eq.2) then
c                 normal to the left vertical wall
                  nxij=-1.D0
                  nyij=0.D0
              endif
              if (i.eq.3) then
c                 normal to the horizontal top wall
                  nxij=0.D0
                  nyij=1.D0
              endif
              if (i.eq.4) then
c                 normal to the right vertical wall
                  nxij=1.D0
                  nyij=0.D0
              endif
            endif

              rxi = rx(i)
              ryi = ry(i)
              rroti=rrot(i)
              rdi = rd(i)
            
            do jn = jbeg,jend
                j = list(jn)     ! number of particle in the Verlet list
              
              if (live(j).eq.1) then
c                 particle 'j' is alive: test contact with 'i'
                  tj = iobj(j)
                  jgd = idgd(j)

c                 find out the status of the potential contact:
c                 new contact / existing contact (cohesive or non cohesive)
c                 note that the contact list is a subset of the Verlet list, and
c                 both lists are ordered increaingly for particle 'i'
                  exc =.FALSE. ! contact status is suposed new
                if (j.eq.klist(k).and.k.le.kend) then
                    exc =.TRUE.
                    kc = k
                    k = k+1
                endif

c                 Verlet list is such that at least particle 'j' is a disk       
                  rxj = rx(j)
                  ryj = ry(j)
                  rrotj=rrot(j)
                  rdj=rd(j)         
                  
                  rxji = rxj - rxi
c                 in semi-periodic boundary conditions,
c                 the sign of the complementary distance is opposite
                if (speri.and.(dabs(rxji).gt.rxw2)) then
                    if (rxji.gt.0.d0) then
                        rxji=rxji-rxw
                    else
                        rxji=rxji+rxw
                    endif
                endif
                  ryji = ryj - ryi

c>>>>>>>>1>>>>>>>>>2>>>>>>>>>3>>>>>>>>>4>>>>>>>>>5>>>>>>>>>6>>>>>>>>>7>>
                if (ti.eq.1.and.tj.eq.1) then ! d-d
                    rdij=rdi+rdj
                    rji= dsqrt(rxji**2 + ryji**2)
                    nxij =  -rxji / rji  
                    nyij =  -ryji / rji 
c                   parameters : (igd,jgd,ccr,cst,exc,kc,rdij,rji,nxij,nyij)
                    call newcondd(igd,jgd,j,ccr,.TRUE.,exc,kc,
     &                            rdij,rji,nxij,nyij,rroti,rrotj)

                  if (ccr) then
c                     relative velocities are zero (disks are freezed)
                     
                      vn(nc)=0.d0
                      vt(nc)=0.d0   
                      vs(nc)=0.d0
                  endif
                endif
               
                if (ti.eq.2.and.tj.eq.1) then   ! l-d    
c                   parameters : (igd,jgd,ccr,cst,exc,kc,rdij,rji,nxij,nyij)
                    call newconld(i,jgd,j,ccr,.TRUE.,exc,kc,
     &                            rdj,rxji,ryji,nxij,nyij,rrotj)

                  if (ccr) then
c                     calculate relative velocities at contact point
c                     (disks are freezed while walls may be moving by
c                      translation)
                      txij = nyij
                      tyij = -nxij   
                    
                      vn(nc) = vx(i)*nxij + vy(i)*nyij
                      vt(nc) = vx(i)*txij + vy(i)*tyij 
                      vs(nc) = 0.d0
                  endif

                endif

              endif
            enddo
          endif
        endif
      enddo
      
        cpoint(np)=nc+1


c       At initial step and at the beginning of erosion disks are freezed
c      do i=nl+1,np
c          vx(i)=0.d0
c          vy(i)=0.d0
c          vrot(i)=0.d0
c      enddo
      
      return
      end
      
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      include '../SANDBOX/newcondd.f'
      include '../SANDBOX/newconld.f'
      

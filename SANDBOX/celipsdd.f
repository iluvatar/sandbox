c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR celipsdd(igd,jgd,icd,jcd,rdij)

c     This routine determines whether or not the contact law between two
c     particles is associated to a rupture ellipse.
c     The lengths of the axes are calculated according to the layer's
c     parameters ant interlayer behavior.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine celipsdd(igd,jgd,icd,jcd,rdij)
      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer igd,jgd,icd,jcd
      real*8 axni,axti,axnj,axtj
      real*8 rdij

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      if (igd.eq.jgd) then

c         both particles belong to the same geological domain

        if (neli(icd).gt.0) then
c           the cohesion distance law of the geological domain is 
c           defined by an ellipse.

            cax(nc)=.TRUE.

c           ellipse's axes at the contact are defined according to
          if (neli(icd).eq.1) then
c             real (constant) distances
              axn(nc)=axnr(icd)
              axt(nc)=axtr(icd)

          else
c             normalised distances times the sum of radii
              axn(nc)=axnn(icd)*rdij
              axt(nc)=axtn(icd)*rdij
          endif

        else
c          cohesion distance law is not defined by an ellipse
c          neli(icd) = 0 (e.g. frictional non-cohesive material)
           cax(nc)=.FALSE.

        endif

      else

c         interlayer contact distance parameters are calculated

        if ( (neli(icd).eq.0).and.(neli(jcd).eq.0) ) then
c           contact is established according to the distance between 
c           particles, and not according to an ellipse.
            cax(nc) = .FALSE.

        else

c           case structure, neli(icd)
c           axni, axti = lengths of ellipse's axes for particle i
          if (neli(icd).eq.1) then
              axni=axnr(icd)
              axti=axtr(icd)
          endif
          if (neli(icd).eq.2) then
              axni=axnn(icd)*rdij
              axti=axtn(icd)*rdij
          endif
c           case structure, neli(jcd)
c           axnj, axtj = lengths of ellipse's axes for particle j
          if (neli(jcd).eq.1) then
              axnj=axnr(jcd)
              axtj=axtr(jcd)
          endif
          if (neli(jcd).eq.2) then
              axnj=axnn(jcd)*rdij
              axtj=axtn(jcd)*rdij
          endif

          if ( (neli(icd).gt.0).and.(neli(jcd).gt.0) ) then
c             both particles are associated to cohesion distance ellipses
              cax(nc) = .TRUE.

c             case structure
            if (ilaymp(ks).eq.1) then
c               minimum parameters
                axn(nc)=dmin1(axni,axnj)
                axt(nc)=dmin1(axti,axtj)
            endif
            if (ilaymp(ks).eq.2) then
c               mean parameters
                axn(nc)=(axni+axnj)/2.d0
                axt(nc)=(axti+axtj)/2.d0
            endif
            if (ilaymp(ks).eq.3) then
c               maximum parameters
                axn(nc)=dmax1(axni,axnj)
                axt(nc)=dmax1(axti,axtj)
            endif

          else
c             only one particle is linked to a cohesion distance ellipse

            if (ilaymp(ks).eq.1) then
c               minimum parameters: non-existent ellipse
                cax(nc) = .FALSE.
            else
c               ilaymp(ks) = 2 or 3: mean and maximum parameters consider
c               the existing cohesion distance ellipse (i or j). Strength
c               will be defined by choosing appropriate mechanical parameters       
              if (neli(jcd).eq.0) then  ! neli(icd)>0
c                 choose cohesion distance ellipse of particle i 
                  cax(nc) = .TRUE.
                  axn(nc)=axni
                  axt(nc)=axti
              else
c                 choose cohesion distance ellipse of particle j
                  cax(nc) = .TRUE. 
                  axn(nc)=axnj
                  axt(nc)=axtj
              endif

            endif

          endif

        endif

      endif

      return
      end

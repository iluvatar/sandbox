c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR drive()
c     Shearbox

c     Update externally imposed degrees of freedom :
c        EXTERNAL FORCES and VELOCITIES
c     Variables with e-suffix

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine drive()
      implicit none
      include '../VAR/mpglob'

      integer i

c-----Update the driven degrees of freedom of the walls to start the next step

c-----Walls are defined as follows:
c		1 : Bottom wall 
c		2 : Left wall
c		3 : Top wall
c		4 : Right wall

c     first, take care of the x direction
      do i=1,4 ! for the four walls
         if (idfx(i).eq.0) then ! the wall is force controlled in x
		    if (idfunx(i).eq.0) then ! the forces are calculated using a harmonic function
			   fxe(i)=cnst1x(i)+amp_x(i)*cos((freq_x(i)*tm)+phse_x(i))
		    elseif (idfunx(i).eq.1) then ! the forces are calculated using a ramp function
			   if (tm.lt.tmx(i)) then
                  fxe(i)=cnst2x(i)+(cnst3x(i)-cnst2x(i))*tm/tmx(i)
               elseif (tm.ge.tmx(i)) then
                  fxe(i)=cnst3x(i)
               endif
		    endif
		 elseif (idfx(i).eq.1) then ! the wall is velocity controlled in x
		    if (idfunx(i).eq.0) then ! the velocities are calculated using a harmonic function
			   vxe(i)=cnst1x(i)+amp_x(i)*cos((freq_x(i)*tm)+phse_x(i))
			elseif (idfunx(i).eq.1) then ! the velocities are calculated using a ramp function
			   if (tm.lt.tmx(i)) then
			      vxe(i)=cnst2x(i)+(cnst3x(i)-cnst2x(i))*tm/tmx(i)
			   elseif (tm.ge.tmx(i)) then
			      vxe(i)=cnst3x(i)
			   endif
			endif
			vx(i)=vxe(i)
         endif
      enddo
	  
c     then, take care of the y direction
      do i=1,4 ! for the four walls
         if (idfy(i).eq.0) then ! the wall is force controlled in y
		    if (idfuny(i).eq.0) then ! the forces are calculated using a harmonic function
			   fye(i)=cnst1y(i)+amp_y(i)*cos((freq_y(i)*tm)+phse_y(i))
            elseif (idfuny(i).eq.1) then ! the forces are calculated using a ramp function
			   if (tm.lt.tmy(i)) then
			      fye(i)=cnst2y(i)+(cnst3y(i)-cnst2y(i))*tm/tmy(i)
			   elseif (tm.ge.tmy(i)) then
                  fye(i)=cnst3y(i)
			   endif
			endif
         elseif (idfy(i).eq.1) then ! the wall is velocity controlled in y
		    if (idfuny(i).eq.0) then ! the velocities are calculated using a harmonic function
			   vye(i)=cnst1y(i)+amp_y(i)*cos((freq_y(i)*tm)+phse_y(i))
            elseif (idfuny(i).eq.1) then ! the velocities are calculated using a ramp function
			   if (tm.lt.tmy(i)) then
			      vye(i)=cnst2y(i)+(cnst3y(i)-cnst2y(i))*tm/tmy(i)
			   elseif (tm.ge.tmy(i)) then
                  vye(i)=cnst3y(i)
			   endif
			endif
			vy(i)=vye(i)
         endif
      enddo

c     note that the rotational degrees of freedom of 4 walls are velocity controlled
c     and their angular velocity is 0.d0 (see walbc.f)

      return
      end

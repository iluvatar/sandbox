c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
c     SR fres()
      
c     Compute force resultants on particles from contact forces
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      subroutine fres()
      implicit none
      include '../VAR/mpglob'
      
c     Local variables
 
      integer i,j,ti,tj
      integer jbeg,jend,jn    
      real*8 fxij,fyij,fnij,ftij,fsij,fxi,fyi,froti
      real*8 nxij,nyij,txij,tyij    
      real*8 rdi,rdj,massi

c-----External forces
      
      do i = 1,np
        if (live(i).eq.1) then
            massi = mass(i)
            fx(i)   = fxe(i)+massi*gv*dcos(gvdir)
            fy(i)   = fye(i)+massi*gv*dsin(gvdir)
            frot(i) = frote(i)
        endif
      enddo
      
c-----Contact forces
      
      do i = 1, np-1
        if (live(i).eq.1) then
c           particle 'i' is alive: look for contacts
            jbeg = cpoint(i)  
            jend = cpoint(i+1) - 1     
         
          if (jbeg.le.jend) then ! if there are contacts
              ti=iobj(i)
              rdi=rd(i)
              fxi=fx(i)
              fyi=fy(i) 
              if (ti.eq.1) froti=frot(i)
            
            do jn = jbeg,jend
c               particle 'j' is alive: test contact with 'i'               
                j=clist(jn)          

                tj=iobj(j)
                rdj=rd(j)
               
                fnij=fn(jn)
                if (ielas.eq.1) fnij=fnij+fel(jn)
                 
                ftij=ft(jn)
                fsij=fs(jn)
               
                nxij=nx(jn)
                nyij=ny(jn)
                txij =  nyij
                tyij = -nxij
               
                fxij = fnij * nxij + ftij * txij
                fyij = fnij * nyij + ftij * tyij
               
                fxi = fxi + fxij
                fyi = fyi + fyij

c               the reaction contact force on disk j has opposite direction
                fx(j) = fx(j) - fxij
                fy(j) = fy(j) - fyij
               
c               wall rotation is velocity controlled; thus only disk rotation
c               is calculated
                if (ti.eq.1) froti = froti + rdi*ftij + fsij
                if (tj.eq.1) frot(j) = frot(j) + rdj*ftij - fsij

            enddo              ! over j 
            
              fx(i) = fxi
              fy(i) = fyi
              if (ti.eq.1) frot(i)=froti
          endif                 ! for neighbour-at-all condition
        endif  
      enddo                     ! for loop over pairs
      
      
c-----Force resultants on bloqued degrees of freedom
      
      do i=1,np

c         In geo.f : only the walls are velocity controlled
c         In biax.f : walls and nailed particles can be velocity controlled
         
          if (idfx(i).eq.1) fx(i)=0.d0
          if (idfy(i).eq.1) fy(i)=0.d0
          if (idfrot(i).eq.1) frot(i)=0.d0
         
      enddo
      
      return
      end

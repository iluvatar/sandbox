c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR look()

c     Take care of contacts and forces 
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine look()
      implicit none
      include '../VAR/mpglob'

c-----Save current positions and velocities of particles in a new array

      call savepos()
            
c-----update Verlet list
      
      if (mod(ns-(nsi+1),nver).eq.0) then
         call verlet()
      endif
      
c-----New contact list and data, local frames and relative velocities

      if (sol) then
          call solid()
      else
          call contact()
      endif

c-----Compute force resultants on particles

      call fres()

c-----Arrays of contact properties and coefficients: reduced masses and
c     mechanical properties of contacts (restitution, friction, adhesion ...)

      call contprop()
      
c-----The main loop: compute forces
      
      call iter()

c-----Save contact forces of particles between stages tm and tm+dt in a new array
      
      call savefor()

      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      include '../SANDBOX/savepos.f'
      include '../SANDBOX/contact.f'
      include '../SANDBOX/contprop.f'
      include '../SANDBOX/fres.f'
      include '../SANDBOX/iter.f'
      include '../SANDBOX/solid.f'
      include '../SANDBOX/verlet.f'




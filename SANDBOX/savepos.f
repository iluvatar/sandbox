c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR savepos()

c     Copy positions and velocities in a new array  
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine savepos()
      implicit none
      include '../VAR/mpglob'

c     Local variables

      integer i

c-----External forces

      do i = 1, np

          rxk(i)=rx(i)
          ryk(i)=ry(i)
          rrotk(i)=rrot(i)
          vxk(i)=vx(i)
          vyk(i)=vy(i) 
          vrotk(i)=vrot(i)
         
      enddo                  
      
      return
      end
      

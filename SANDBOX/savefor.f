c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR savefor()

c     Copy forces in a new array  
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine savefor()
      implicit none
      include '../VAR/mpglob'


c Local variables

      integer i,jbeg,jend,jn

c-----External forces


      do i = 1, np-1
         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1    
         kpoint(i)=jbeg
         
         if (jbeg.le.jend) then    
            
            do jn = jbeg,jend
               klist(jn)=clist(jn)    
               cstak(jn) = csta(jn)
               danik(jn) = dani(jn)
               danjk(jn) = danj(jn)
               dpork(jn) = dpor(jn)
               caxk(jn) = cax(jn) 
             if (caxk(jn)) then
                 axnk(jn) = axn(jn)
                 axtK(jn) = axt(jn)
             endif      
               fnk(jn)=fn(jn)
               ftk(jn)=ft(jn)
               fsk(jn)=fs(jn)
c              save elastic forces 
               if (ielas.eq.1) felk(jn)=fel(jn)       

            end do         
            
         end if                
         
      enddo                  
      
        kpoint(np)=cpoint(np)
      
      return
      end
      

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR data()

c     Runtime data output  
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine data()
      implicit none
      include '../VAR/mpglob'

c-----Local variables

      integer i,j,ti,tj,iov,inefk,ineoa,ineta
      integer jbeg,jend,jn

      real*8 rdi,rdj
      real*8 rxi,ryi,rxj,ryj
      real*8 rxij,ryij,rij,faux
      real*8 pkf,xef,ovlaux,ovlapef

       ncov=0
	   ovmean=0.d0
       ovlapmx=0.d0

      if (nc.ge.1) then
         
c        calculate contact ovelaps
       do i = 1, np - 1  

        if (live(i).eq.1) then

         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1
         rxi = rx(i)
         ryi = ry(i)
         rdi=rd(i)
         ti=iobj(i)
            
         do jn = jbeg,jend

          if (jbeg.le.jend) then ! if there is a neighbor
            j=clist(jn)
            tj=iobj(j)

           if ((ti.eq.1).or.(tj.eq.1)) then
             rxj=rx(j)
             ryj=ry(j)
             rdj=rd(j)

             rxij = rxi - rxj
             ryij = ryi - ryj
              
            if ((ti.eq.1).and.(tj.eq.1)) then !d-d 
              rij= dsqrt(rxij**2 + ryij**2)
              if (iovlap.eq.1) ovlap(jn)=(-rij+rdi+rdj)/(rdi+rdj)
              if (iovlap.eq.2) ovlap(jn)=(-rij+rdi+rdj)/rov
    
            else   ! l-d
              if (i.eq.1) rij=dabs(ryij)
              if (i.eq.2) rij=dabs(rxij)
              if (i.eq.3) rij=dabs(ryij)
              if (i.eq.4) rij=dabs(rxij)
     
              if (iovlap.eq.1) ovlap(jn)=(rdj-rij)/rdj
              if (iovlap.eq.2) ovlap(jn)=(rdj-rij)/rov

            endif
              
            if (ovlap(jn).gt.0.d0) then

                ncov=ncov+1
                ovmean=ovmean+ovlap(jn)
              
                if (ovlap(jn).gt.ovlapmx) then
c				   write(*,*)'i=',i,' j=',j
c				   write(*,*)'rdi=',rdi,' rdj=',rdj
c				   write(*,*)'rxi=',rxi,' rxj=',rxj
c				   write(*,*)'ryi=',ryi,' ryj=',ryj
c				   write(*,*)'rxij=',rxij,' ryij=',ryij
c				   write(*,*)'rij=',rij
c				   write(*,*)'rxi=',rxi,' rxj=',rxj
c				   write(*,*)'ovlp(jn)=',ovlap(jn)
				   ovlapmx=ovlap(jn)
				endif

            endif

           endif
          endif
         enddo
        endif
       enddo
	   
        if (ncov.gt.0) ovmean=ovmean/DBLE(ncov)

c       ieoab=0 then the elastic modulus is constant (eoab is constant);

c       ieoab=1 then the elastic modulus is shifted toward the maximum value
c       between eob and ovlapmx. Note that elastic force is always <= pk*(rdi+rdj)

       if (ieoab.eq.1) then
         etb=dmax1(eob,ovlapmx)
         eab=etb-eoa
       endif

c       ieoab=2 then the elastic law is shifted horizontaly according to 'ovlapmx'
        
       if (ieoab.eq.2) then
         etb=dmax1(eob,ovlapmx)
         eta=etb-eabo
         xef=(etb-eoa)/(eob-eoa)-1.D0
         pkf=1.D0+sef*xef
       endif

c       ieoab=3 then the elastic law is shifted horizontaly according to 'ovlapmx' and
c       according to the maximum number of elastic interactions 'npef'
        
       if (ieoab.eq.3) then
         etb=dmax1(eob,ovlapmx)
c        eta = value of the 'npef' largest overlap calculated from the list
         ovlapef=ovlapmx
        do i=1,npef
          ovlaux=ovlap(1)
         do j=2,nc
           if ((ovlaux.lt.ovlap(j)).and.(ovlap(j).lt.ovlapef))
     &         ovlaux=ovlap(j)
         enddo
          ovlapef=ovlaux
        enddo

         eta=dmax1(etb-eabo,ovlapef)
         eab=etb-eta
         xef=(etb-eoa)/(eob-eoa)-1.D0
         pkf=1.D0+sef*xef
       endif

c      number of inelastic contacts such that the overlap 'ovlap(jn) > eoa'
       ineoa=0
c      number of inelastic contacts such that the overlap 'ovlap(jn) > eta'
       ineta=0

       do i=1,nc
         if (ovlap(i).gt.eoa) ineoa=ineoa+1
         if (ovlap(i).gt.eta) ineta=ineta+1
       enddo

c      number of inelastic contacts such that the elastic force 'felk > 0'
       inefk=0

c      The elastic force is calculated in each contact, according to its previous value
c      and to the new value

       if (ielas.eq.1) then

       do i = 1, np - 1  
         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1
         rdi=rd(i)
         ti=iobj(i)         
        do jn = jbeg,jend
         if (jbeg.le.jend) then ! if there is a neighbor
           j=clist(jn)
           tj=iobj(j)

          if ((ti.eq.1).or.(tj.eq.1)) then
            rdj=rd(j)

c           Perhaps the value of overlap should be evaluated at the end of the step ??
           if (ovlap(jn).gt.eta) then
c            the elastic force 'faux' is calculated from the present overlap and 
c            elastic behavior law
             faux=pk*((ovlap(jn)-eta)/eab)**plef          

            if ((ti.eq.1).and.(tj.eq.1)) then !d-d 
              faux=faux*(rdi+rdj)**pler
            else   ! l-d
              faux=faux*(rdj+rdj)**pler
            endif
C            The elastic force can increase with overlap
             if (ieoab.ge.2) faux=faux*pkf
           else
c           if overlap < maximum accepted value then elastic force is set to zero
            faux=0.D0
           endif

c          The elastic force 'felk(jn)' ought to vary continuosly (slowly) between
c          the previous value 'fel(jn)' and 'faux'. Note that 'felk(jn)' corresponds
c          to the elastic force imposed in the next iteration.

           if (dabs(faux-fel(jn)).gt.efi) then
            if (faux.gt.fel(jn)) then
              felk(jn)=fel(jn)+efi
            else
              felk(jn)=fel(jn)-efi
            endif
           else
             felk(jn)=faux
           endif
            
           if (felk(jn).gt.0.D0) inefk=inefk+1

          endif
         endif
        enddo
       enddo

       endif

        write(*,10) tm,niter,nc,ncov,ovmean,ovlapmx
c        write(*,*) tm,niter,nc,ncov,ovmean,ovlapmx
        write(16,10) tm,niter,nc,ncov,ovmean,ovlapmx

 10     format('tm=',F10.5,' niter=',I5,' nc=',I5,
     &      ' ncov=',I5,' ovmean=',F8.5,' ovlapmx =',F8.5)

      endif

      return
      end

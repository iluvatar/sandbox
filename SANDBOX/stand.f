c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR stand()

c     Initialize the program
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine stand()
      implicit none
      include '../VAR/mpglob'

c-----Local variables
      
      integer i

c-----Constants

      pi = 4.d0 * datan(1.d0)
      pi2=2.d0*datan(1.d0)
      pit2 = 8.d0 * datan(1.d0)
      de2ra=pi/180.d0      
      ra2de=180.d0/pi
      vslope=0

c-----Read flow parameters 

      open(1,file='./input/flow',status='unknown')

      read(1,*)
      read(1,*) irun
      read(1,*) iread,nread
      read(1,*) witer,nwiter
      write(*,*) 'Read the "flow" file --> OK'

      if(irun.eq.0) then
         stop 'No-execution start mode. Change mode!'      
      end if

      close(1)

c-----Read system data

      open(1,file='./input/exec_parameters',status='unknown')
      call rexec(1)
      write(*,*) 'Read the "exec_parameters" file --> OK'
      close(1)

      open(1,file='./output/exec_parameters_out',status='unknown')
      call wexec(1)
      close(1)

c-----assign the names of the input files

      f_pari='./input/par'//inftyg ! file containing the information of the particles
	  f_posi='./input/pos'//inftyg ! file containing the positions and velocities
	  f_coni='./input/con'//inftyg ! file containing the contacts info ...
      f_mat='./input/properties'
      f_dom='./input/domains'
      f_wal='./input/walls'

c-----assign the names of the files to be created during execution

      f_posh='./output/posh'
      f_conh='./output/conh'

      f_parf='./output/par'//ouftyg
      f_posf='./output/pos'//ouftyg
      f_conf='./output/con'//ouftyg

c-----Read material properties of the system

c     files gmat, gdom, gwal are prepared by the user before execution
         
       open(1,file=f_mat,status='unknown')
       call rprop(1)
       write(*,*) 'Read the "properties" file --> OK'
       close(1)
	   
      open(1,file='./output/properties_out',status='unknown')
      call wprop(1)
      close(1)
    
       open(1,file=f_dom,status='unknown')
       call rdom(1)
       write(*,*) 'Read the "domains" file --> OK'
       close(1)
	   
      open(1,file='./output/domains_out',status='unknown')
      call wdom(1)
      close(1)

       open(1,file=f_wal,status='unknown')
       call rwal(1)
	   write(*,*) 'Read the "walls" file --> OK'
       close(1)
	   
      open(1,file='./output/walls_out',status='unknown')
      call wwal(1)
      close(1)
                   
c     Set initial kinematic state of the system from final state in previous stages. 
c     Initialize history and stress/strain files. Time and step number initialisation.
c     Definning initial and final number of step and time intervals in numerical 
c     biaxial experiment; initialize elastic parameters

      call sikifi()

c-----Initialisation of variables
   
      do i=nl+1,np
c        All the disks are force controlled and external forces are set to zero
         idfx(i)=0
         idfy(i)=0
         idfrot(i)=0
         fxe(i)=0.d0
         fye(i)=0.d0
         frote(i)=0.d0
      enddo

c-----Fill in empty arrays
         
      call fill()
   
c-----Take into account system data  

      call walbc()

c-----Apply external velocities to walls that are velocity controlled

      do i=1,nl
        if(idfx(i).eq.1) vx(i)=vxe(i)
        if(idfy(i).eq.1) vy(i)=vye(i)
        if(idfrot(i).eq.1) vrot(i)=vrote(i)
      enddo
	        
      return
      end
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      include '../SANDBOX/fill.f'
      include '../SANDBOX/sikifi.f'
      include '../SANDBOX/walbc.f'



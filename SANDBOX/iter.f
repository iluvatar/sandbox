c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR iter()

c     Gauss-Sidel iteration

c     This routine has been modified in relation to initial version.
c     It includes the calculation of contact moments using a formal
c     angular relative velocity law (positive moments and angular rotations
c     are conterclockwise.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine iter()
      implicit none
      include '../VAR/mpglob'


c     Local variables

      real*8 fnij,ftij,fsij,fxi,fyi,fxj,fyj,froti,frotj
      real*8 nxij,nyij,txij,tyij
      real*8 rdi,rdj,massi,massj,momi,momj
      real*8 muij,mnij,mtij,msij

c     variables used to optimise calculations within the main loop
      real*8 vnbc(ncmax),vtbc(ncmax),vsbc(ncmax)
      real*8 mnminx(ncmax),mnminy(ncmax),mnmjnx(ncmax),mnmjny(ncmax)
      real*8 mtmitx(ncmax),mtmity(ncmax),mtmjtx(ncmax),mtmjty(ncmax)
      real*8 mtrmi(ncmax),mtrmj(ncmax),msmi(ncmax),msmj(ncmax)
      
      integer i,j,k,ti,tj
      integer jbeg,jend,jn
      integer idfxi,idfyi,idfroti,idfxj,idfyj,idfrotj 
      integer nstop
      
      real*8 dfxij,dfyij,dfnij,dftij,dfsij,fnij0,ftij0,fsij0
c      real*8 fn1,fn2,dfn,ft1,ft2,dft,fs1,fs2,dfs
      real*8 fn1,fn2,dfn
      real*8 fnexij,exij,slij
      
      
c-----No-contact case
      
      if(nc.eq.0) then
         niter=0
         return
      endif
      
C-----Calculate coefficients to be used in the Gauss-Siedel iteration process.
c     these values are calculated only once.


      do i = 1, np-1
         jbeg = cpoint(i)  
         jend = cpoint(i+1) - 1
c        note that particles that are not alive do not have neighbors
            
        if (jbeg.le.jend) then ! if there is a neighbor
           massi=mass(i)
           momi=mom(i)
               
          do jn=jbeg,jend

             j=clist(jn)
             massj=mass(j)
             momj=mom(j)
                  
             mnij=mn(jn)
             mtij=mt(jn)
             msij=ms(jn)

             nxij=nx(jn)
             nyij=ny(jn)
c            unitary vector (txij, tyij) is tangent to contact, 
c            and positive in the counterclockwise direction
             txij=nyij
             tyij=-nxij
         
c       ---- Coefficients for calcularing the normal contact force:

c            vnbc(jn) = constant term in normal force equation, which depends
c                         on the relative normal velocity before the collision 
             vnbc(jn) =-(1.d0+en(jn))*mnij*vn(jn)/dt

             mnminx(jn) = nxij*mnij/massi
             mnminy(jn) = nyij*mnij/massi

             mnmjnx(jn) = nxij*mnij/massj
             mnmjny(jn) = nyij*mnij/massj

c       ---- Coefficients for calcularing the shear contact force:

c            vtbc(jn) = constant term in shear force equation, which depends
c                         on the relative tangential velocity before the collision 
             vtbc(jn)=-(1.d0+et(jn))*mtij*vt(jn)/dt

             mtmitx(jn) = txij*mtij/massi
             mtmity(jn) = tyij*mtij/massi
             mtmjtx(jn) = txij*mtij/massj
             mtmjty(jn) = tyij*mtij/massj

             mtrmi(jn) = mtij*rd(i)/momi
             mtrmj(jn) = mtij*rd(j)/momj

c       ---- Coefficients for calcularing the contact moment :

c            vsbc(jn) = constant term in moment equation, which depends
c                         on the relative angular velocity before the collision 
             vsbc(jn)=-(1.d0+es(jn))*msij*vs(jn)/dt

             muij=mu(jn)

             msmi(jn) = msij/momi
             msmj(jn) = msij/momj

          enddo            ! over j
               
        endif               ! if jn
            
      enddo                  ! over i

c-----Main CD loop
      
       fn2=0.d0
       fn1=0.d0
c       ft2=0.d0
c       ft1=0.d0
c       fs2=0.d0
c       fs1=0.d0
       nstop=0
       k=1
    
      do while ((nstop.lt.nitermn).and.(k.le.nitermx))
         
         do i = 1, np-1
            jbeg = cpoint(i)  
            jend = cpoint(i+1) - 1
c           note that particles that are not alive do not have neighbors
            
            if (jbeg.le.jend) then ! if there is a neighbour
c              fx(i), fy(i), frot(i) = total force/moment acting on particle 'i'
               fxi=fx(i)
               fyi=fy(i)
               froti=frot(i)
               rdi=rd(i)
               ti=iobj(i)
               idfxi=idfx(i)
               idfyi=idfy(i)
               idfroti=idfrot(i)
               
               do jn=jbeg,jend
                  j=clist(jn)

                  nxij=nx(jn)
                  nyij=ny(jn)
c                 unitary vector (txij, tyij) is tangent to contact, 
c                 and positive in the counterclockwise direction
                  txij=nyij
                  tyij=-nxij
c                 fx(j), fy(j), frot(j) = total force/moment acting on particle 'j'
                  fxj=fx(j)
                  fyj=fy(j)
                  frotj=frot(j)
                  rdj=rd(j)
                  tj=iobj(j)
                  idfxj=idfx(j)
                  idfyj=idfy(j)
                  idfrotj=idfrot(j)
                  
                  fnij0=fn(jn)
                  ftij0=ft(jn)
                  fsij0=fs(jn)
                  
                  exij=ex(jn)
                  slij=sl(jn)
                  
                  muij=mu(jn)	  
                  
c-----Normal forces
                  
                  fnij = vnbc(jn) + fnij0
     &                 - mnminx(jn)*fxi-mnminy(jn)*fyi
     &                 + mnmjnx(jn)*fxj+mnmjny(jn)*fyj
                                    
                  if (fnij.lt.-exij) fnij=-exij		    
                  
                  dfnij= fnij-fnij0
                  dfxij = dfnij * nxij 
                  dfyij = dfnij * nyij 
                  
c                 if degrees of freedom are force controlled
                  if (idfxi.eq.0) fxi= fxi + dfxij
                  if (idfyi.eq.0) fyi= fyi + dfyij
                  if (idfxj.eq.0) fxj= fxj - dfxij
                  if (idfyj.eq.0) fyj= fyj - dfyij
                  
c-----Friction forces
                  
                  fnexij=fnij+exij

                  ftij= vtbc(jn) + ftij0
     &                 - mtmitx(jn)*fxi-mtmity(jn)*fyi
     &                 + mtmjtx(jn)*fxj+mtmjty(jn)*fyj
     &                 - mtrmi(jn)*froti
     &                 - mtrmj(jn)*frotj
                  
                  if (ftij.gt.muij*fnexij) ftij=muij*fnexij
                  if (ftij.lt.-muij*fnexij) ftij=-muij*fnexij
                  
                  dftij = ftij - ftij0
                  dfxij = dftij * txij 
                  dfyij = dftij * tyij 
                  
                  if (idfxi.eq.0) fxi= fxi + dfxij
                  if (idfyi.eq.0) fyi= fyi + dfyij
                  if (idfxj.eq.0) fxj= fxj - dfxij
                  if (idfyj.eq.0) fyj= fyj - dfyij
c********1*********2*********3*********4*********5*********6*********7*********8
                  if (ti.eq.1.and.idfroti.eq.0) froti=froti+rdi*dftij
                  if (tj.eq.1.and.idfrotj.eq.0) frotj=frotj+rdj*dftij
c                 walls are rotation controlled for biax.f and geo.f (idfrot=1)
                 if (ti.eq.2.and.idfroti.eq.0) froti = froti + dftij !a modifier
                 if (tj.eq.2.and.idfrotj.eq.0) frotj = frotj + dftij !a modifier 
                  
c-----End friction forces

c-----Local moments
                  
                   fsij= vsbc(jn) + fsij0
     &                 - msmi(jn)*froti
     &                 + msmj(jn)*frotj
                  
                  if (fsij.gt.slij*fnexij) fsij=slij*fnexij                  
                  if (fsij.lt.-slij*fnexij) fsij=-slij*fnexij
                  
                  dfsij = fsij - fsij0

                  if (ti.eq.1.and.idfroti.eq.0) froti = froti + dfsij 
                  if (tj.eq.1.and.idfrotj.eq.0) frotj = frotj - dfsij
c                 walls are rotation controlled for biax.f and geo.f (idfrot=1)
                 if (ti.eq.2.and.idfroti.eq.0) froti = froti + dfsij !a modifier
                 if (tj.eq.2.and.idfrotj.eq.0) frotj = frotj - dfsij !a modifier 
                  
c-----End local moments
                  
c-----Write new values of contact forces
                  
                  fn(jn)=fnij
                  ft(jn)=ftij
                  fs(jn)=fsij
                  
c-----Normal force mean
                  
                  fn2=fn2+dabs(fnij)	   
c                  ft2=ft2+dabs(ftij)	   
c                  fs2=fs2+dabs(fsij)	   
                  
c-----Write new values of resultants
                  
                  fx(j)=fxj
                  fy(j)=fyj
                  frot(j)=frotj
                  
               enddo            ! over j
               
c-----Write new values of resultants	   
               
               fx(i)=fxi
               fy(i)=fyi
               frot(i)=froti
            endif               ! if jn
            
         enddo                  ! over i
         
c-----Check convergence taking into account normal and shear forces and moments
         
         if (fn2.ne.0.d0) then
            dfn=dabs(fn2-fn1)/fn2
         else
            dfn=0.d0
         endif

c         if (ft2.ne.0.d0) then
c            dft=dabs(ft2-ft1)/ft2
c         else
c            dft=0.d0
c         endif

c         if (fs2.ne.0.d0) then
c            dfs=dabs(fs2-fs1)/fs2
c         else
c            dfs=0.d0
c         endif
         
c         if (dfn+dft+dfs.lt.epsf) then
         if (dfn.lt.epsf) then
            nstop=nstop+1
         else
            nstop=0
         endif
         
         fn1=fn2
         fn2=0.d0
c         ft1=ft2
c         ft2=0.d0
c         fs1=fs2
c         fs2=0.d0
         k=k+1
         
c-----End check convergence
         
      enddo                     !  while
      
       niter=k
      
      return 
      end


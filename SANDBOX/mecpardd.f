c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR mecpardd(jn,igd,jgd,rdij)

c     This routine determines the mechanical parameters at a disk-disk contact:
c     restitution coeficients, extension and moment strength.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine mecpardd(jn,igd,jgd,rdij)
      implicit none
      include '../VAR/mpglob'
      
c     Local variables
      
      integer jn,igd,jgd,js,irc,impr,jrc,jmp
      real*8 rdij,exi,sli,exj,slj

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        
      if (csta(jn)) then
c       initial behavior law
        js = 1
      else 
c       residual behavior law
        js = 2
      endif
        irc = gdrc(igd,js,ks)
        impr = gdmpr(igd,js,ks)
        jrc = gdrc(jgd,js,ks)
        jmp = gdmpr(jgd,js,ks)

      if (igd.eq.jgd) then
c         both disks belong to the same geological domain
          en(jn)=enl(irc)
          et(jn)=etl(irc)
          es(jn)=esl(irc)
          mu(jn)=mul(impr)
c         case structure:
        if (cstr(impr).eq.1) then 
c           cstr( ) = cohesive strength criteriun
c           real (constant) values
            ex(jn)=exlr(impr)
            sl(jn)=sllr(impr)
        endif
        if (cstr(impr).eq.2) then
c           normalised values times the sum of radii
            ex(jn)=exln(impr)*rdij
            sl(jn)=slln(impr)*rdij
        endif
     
      else
c         disks belong to different geological domains
c         case structure
        if (ilaymp(ks).eq.1) then
c           minimum parameters
            en(jn)=dmin1(enl(irc),enl(jrc))
            et(jn)=dmin1(etl(irc),etl(jrc))
            es(jn)=dmin1(esl(irc),esl(jrc))
        endif
        if (ilaymp(ks).eq.2) then
c           mean parameters
            en(jn)=(enl(irc)+enl(jrc))/2.d0
            et(jn)=(etl(irc)+etl(jrc))/2.d0
            es(jn)=(esl(irc)+esl(jrc))/2.d0
        endif
        if (ilaymp(ks).eq.3) then
c           maximum parameters
            en(jn)=dmax1(enl(irc),enl(jrc))
            et(jn)=dmax1(etl(irc),etl(jrc))
            es(jn)=dmax1(esl(irc),esl(jrc))
        endif

        if (ilayf(ks)) then
c           inter-layer friction is present
          if (ilaymp(ks).eq.1) mu(jn)=dmin1(mul(impr),mul(jmp))
          if (ilaymp(ks).eq.2) mu(jn)=(mul(impr)+mul(jmp))/2.d0
          if (ilaymp(ks).eq.3) mu(jn)=dmax1(mul(impr),mul(jmp))

        else
            mu(jn)=0.d0
        endif

        if (ilayc(ks)) then
c           inter-layer cohesion is present
c           case structure, cstr(impr)
          if (cstr(impr).eq.1) then
c             real (constant) values
              exi=exlr(impr)
              sli=sllr(impr)
          endif
          if (cstr(impr).eq.2) then
c             normalised values times the sum of radii
              exi=exln(impr)*rdij
              sli=slln(impr)*rdij
          endif
c           case structure, cstr(jcd)
          if (cstr(jmp).eq.1) then
c             real (constant) values
              exj=exlr(jmp)
              slj=sllr(jmp)
          endif
          if (cstr(jmp).eq.2) then
c             normalised values times the sum of radii
              exj=exln(jmp)*rdij
              slj=slln(jmp)*rdij
          endif
c         case structure
          if (ilaymp(ks).eq.1) then
c             minimum parameters
              ex(jn)=dmin1(exi,exj)
              sl(jn)=dmin1(sli,slj)
          endif
          if (ilaymp(ks).eq.2) then
c             mean parameters
              ex(jn)=(exi+exj)/2.d0
              sl(jn)=(sli+slj)/2.d0 
          endif
          if (ilaymp(ks).eq.3) then
c             maximum parameters
              ex(jn)=dmax1(exi,exj)
              sl(jn)=dmax1(sli,slj)
          endif
        else 
            ex(jn)=0.d0
            sl(jn)=0.d0
        endif

      endif

      return
      end

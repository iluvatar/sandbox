c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR hand()

c     Data transfer and updates at run time
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine hand()
      implicit none
      include '../VAR/mpglob'

c-----Local variables

      integer i
c      real*8 vxi,vyi

c-----Calculate overlap and write parameters (overlap, iterations, tm,...)
c     and/or elastic forces between overlapping particles (ielas=1)
      
      if ( ( (witer.eq.1).and.(mod(ns-(nsi+1),nwiter).eq.0) )
     &      .or.(ielas.eq.1)) then
        call data()
      endif

c-----calculate contact forces acting on the walls

       call fconwd()

c-----write files concerning stress/strain and contact data for the walls 
c     (behaviour law) at every step

      do i=1,4
         call wwallx(17+i,i)
         call wwally(21+i,i)
      enddo
	  
c-----write history files

      if ( (ihis.ne.0.and.mod(ns-(nsi+1),nhis).eq.0) ) then
         call wpos(12)
		 call wcon(13)
		 
		 open(1,file=f_posf,status='unknown')
         call wpos(1)
         close(1)
         
         open(1,file=f_conf,status='unknown')
         call wcon(1)
         close(1)

         open(1,file=f_parf,status='unknown')
         call wpar(1)
         close(1)
		 
		 call wcpu
		 call wdistot(33)
		 
	  endif 
      
c-----save the final configuration

      if ( (ns.eq.nsf).or.(irun.eq.0) ) then

        open(1,file=f_posf,status='unknown')
        call wpos(1)
        close(1)
         
        open(1,file=f_conf,status='unknown')
        call wcon(1)
        close(1)

        open(1,file=f_parf,status='unknown')
        call wpar(1)
        close(1)

		call wcpu
		call wdistot(33)

       if (irun.eq.0) then 
         call sit()
         write(*,*) 'Stopped by external request.',ns,'  steps done.'
         write(*,*) ' time of execution tm =',tm
         stop 
       endif

      endif
      
      return
      end
      
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      include '../SANDBOX/fconwd.f'
      include '../SANDBOX/data.f'      

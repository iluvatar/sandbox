c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     SR fill()

c     Fill mass and radius arrays, parameters
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine fill()
      implicit none
      include '../VAR/mpglob'

c-----Local variables

      integer i,igd,irho
      real*8 massi,rdi
      
c-----
            
       mmean=0.d0
       momean=0.d0
      do i=nl+1,np

         igd=idgd(i)
         irho=gdrho(igd)
         rdi=rd(i)
         massi = rhol(irho)*pi*rdi*rdi
         mass(i)=massi
         mmean=mmean+massi
         mom(i) = 0.5d0 * massi * rdi*rdi
         momean=momean+mom(i)

      enddo
c      mmean,  momean = average mass and average moment of inertia of the disks
       mmean=mmean/float(nd)
       momean=momean/float(nd)
c      mass and moment of inertia of each of the 4 walls is calculated from the
c      scale factor 'sfmaw' and 'sfmiw' multiplied by the total mass and 
c      moment of inertia of the disks.
      do i=1,nl
        mass(i)=sfmaw*mmean*float(nd)
        mom(i)=sfmiw*momean*float(nd)
      enddo
c      rdmean, rmax = average and maximum radius of the disks      
       rdmean=0.d0
       rmax= 0.d0
      do i=nl+1,np
        rdi=rd(i)
        rmax=dmax1(rmax,rdi)
        rdmean=rdmean+rdi
      enddo
       rdmean=rdmean/dfloat(nd)
            
      return
      end
      



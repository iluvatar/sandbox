c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  SandBox.f
  
c  Method  : Non Smooth Distinct Element Method 
c            or Contact Dynamics 
c  Date    : October 2008

c  System = MAINbox  
c  A MAIN box with semi-periodic conditions
c  Author  : A. Taboada, N. Estrada, J. Chang, A. P. Aguilar

c  SandBox.f is based on program MAIN.f written at Geosciences
c  Montpellier laboratory by A. Taboada.

c >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      program SandBox
      implicit none 
      include '../VAR/mpglob'

c-----Main program

      call stand()

      do ns=nsi+1,nsf+1
c       calculate initial contacts and forces acting between initial/final stage 
        call look()
c       update positions and velocities
        call step()
c       display and save positions, velocities, and forces
        call hand()
c       update: time, external forces, flow variables, and slow down velocities
        call jump()
      enddo

c-----Close history files

      call sit()

c-----Stop execution

c      pause

      stop 'All steps done.'

      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c-----Computation routines

      include '../LIB/io.f'
      include '../SANDBOX/stand.f'
      include '../SANDBOX/look.f'
      include '../SANDBOX/step.f'
      include '../SANDBOX/hand.f'
      include '../SANDBOX/jump.f'
      include '../SANDBOX/sit.f'

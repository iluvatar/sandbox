!23456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%

program build_SandBox
   implicit none 

! Este programa construye una muestra (es decir, escribe los archivos pari y posi) con las
! siguientes características:
! - radios entre rmin y rmax
! - distribución uniforme de radios
! - partículas organizadas en una grilla rectangular de nrows x ncolumns
! - velocidad uniformemente distribuida en magnitud y dirección entre -vmax y vmax

!23456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%
! local variables

   integer :: i, par, column, ncolumns, row, nrows, nparticles
   double precision :: rmin, rmax, grsize, vmax, x
   integer, dimension (50000) :: iobj, idgd, live
   double precision, dimension (50000) :: rd, rx, ry, rrot, vx, vy, vrot

!23456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%123456789%
! main program

! leer datos de entrada

   open(1,file='build_parameters',status='unknown')

   read(1,*)
   read(1,*) rmin, rmax
   read(1,*) nrows
   read(1,*) ncolumns
   read(1,*) grsize
   read(1,*) vmax

   close(1)

! escoger a qué clase pertenece cada partícula

   nparticles = ncolumns*nrows + 4

   idgd(1) = 0
   idgd(2) = 0
   idgd(3) = 0
   idgd(4) = 0
   iobj(1) = 2
   iobj(2) = 2
   iobj(3) = 2
   iobj(4) = 2
   do par=5, nparticles
      idgd(par)=1
      iobj(par)=1
   enddo

! asignar un radio a cada partícula

   call random_seed()
   do par=5, nparticles
      call random_number(x)
      rd(par) = rmin + x * (rmax - rmin)
   enddo

! asignar la posición de las partículas

   do row=1, nrows
      do column=1, ncolumns
         par = 4 + (row - 1) * ncolumns + column
         rx(par) = DBLE(column) * grsize - grsize/2.
         ry(par) = DBLE(row) * grsize - grsize/2.
         rrot(par) = 0.
      enddo
   enddo

! asignar la velocidad de las partículas

   call random_seed()
   do par=5, nparticles
      call random_number(x)
      vx(par) = -vmax + x * (2.*vmax)
      call random_number(x)
      vy(par) = -vmax + x * (2.*vmax)
      vrot(par) = 0.
   enddo

! asignar las posiciones y velocidades de los muros

   rx(1) = 0.
   rx(2) = 0.
   rx(3) = 0.
   rx(4) = DBLE(ncolumns) * grsize
   ry(1) = 0.
   ry(2) = 0.
   ry(3) = DBLE(nrows) * grsize
   ry(4) = 0.
   rrot(1) = 0.
   rrot(2) = 1.5708
   rrot(3) = 0.
   rrot(4) = 1.5708

   vx(1) = 0.
   vx(2) = 0.
   vx(3) = 0.
   vx(4) = 0.
   vy(1) = 0.
   vy(2) = 0.
   vy(3) = 0.
   vy(4) = 0.
   vrot(1) = 0.
   vrot(2) = 0.
   vrot(3) = 0.
   vrot(4) = 0.

! escribir pari

   open(1,file='pari',status='unknown')

   write(1,*) 4, nparticles-4, nparticles
   write(1,*) 0, 0.
   write(1,*) (iobj(i),i=1,nparticles)
   write(1,*) (rd(i),i=1,nparticles)
   write(1,*) (idgd(i),i=1,nparticles)

   close(1)

! escribir posf

   open(1,file='posi',status='unknown')

   call random_seed()

   write(1,*) 4, nparticles-4, nparticles
   write(1,*) 0, 0.
   write(1,*) (rx(i),i=1,nparticles)
   write(1,*) (ry(i),i=1,nparticles)
   write(1,*) (rrot(i),i=1,nparticles)
   write(1,*) (vx(i),i=1,nparticles)
   write(1,*) (vy(i),i=1,nparticles)
   write(1,*) (vrot(i),i=1,nparticles)
   write(1,*) (1,i=1,nparticles)
   write(1,*)

   close(1)

return
end

c     SR ranf
c     Random number function 

      Double Precision function ranf()
      implicit double precision(a-h,o-z)
      logical logic
      common/gen1/fnorm
      common/gen2/n1,n2

      nn2=n1
      np1=n1*2
      np2=n2*2
      ntemp=np1*8
      if(ntemp.lt.0) then
      logic=.true.
      else
      logic=.false.
      end if
      if(logic) then
      np2=np2+1
      ntemp=np1*8
            if(ntemp.lt.0) then
            np1=np1-268435456
            end if
      end if
      ntemp=np2*8
      if(ntemp.lt.0) then
      np2=np2-268435456
      end if
      ns2=nn2+np2
      ntemp=ns2*8
      if(ntemp.lt.0) then
      ns2=ns2-268435456
      end if
      nt1=np1+n1
      nt2=ns2+n2
      ntemp=nt1*8
      if(ntemp.lt.0) then
      logic=.true.
      else
      logic=.false.
      end if
      if(logic) then
      nt2=nt2+1
      ntemp=nt1*8
            if(ntemp.lt.0) then
            nt1=nt1-268435456
            end if
      end if
      ntemp=nt2*8
      if(ntemp.lt.0) then
      nt2=nt2-268435456
      end if
      n1=nt1
      n2=nt2
      xal1=dfloat(n1)
      xal2=dfloat(n2)*268435456.d0
      xal=xal1+xal2
      ranf=xal*fnorm

      return
      end

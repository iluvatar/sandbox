c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR cordinum()

c     Calculate the average coordination number of a particle set 
c     located inside a rectangular window. Particles in contact
c     with the walls and the heterogeneous particles are kept.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine cordinum()
      implicit none
      include '../VAR/preglob'


c-----local variables
 
      integer i,j,nsp,nsd
      real*8 rxi,ryi,rdi,rxj,ryj,rdj,rdij
      real*8 rxij,ryij,dij,eps

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c      nsp = number of walls + small disks (excluding het. particles)
       nsp=np-ndhp

c      nsd = number of small disks (excluding het. particles)
       nsd=nd-ndhp

c      eps = maximum distance between particles considered to be in
c            geometrical contact
       eps=0.00001

c      ncor = total number of contacts between the particles 
       ncor = 0
c      ndcor = number of disks considered in the particle set 

       ndcor=0

      do i=nl+1,nsp
          dcor(i)=0
          rxi=rx(i)
          ryi=ry(i)
          rdi=rd(i)
        if ((rxi-rdi.ge.zxmin).and.(rxi+rdi.le.zxmax)) then
          if ((ryi-rdi.ge.zymin).and.(ryi+rdi.le.zymax)) then
c             the particle is inside the window where the coordination
c             number is calculated
              ndcor=ndcor+1
c             dcor(i) = indicator for particles belonging to particle set
c                       for the evaluation of the ccordination number
              dcor(i)=1
          endif
        endif
      enddo

      do i = nl+1, nsp  

c         note that the pointer begins with 'i = nl+1'
          icor(i) = ncor + 1
            
        if (dcor(i).eq.1) then
c           particle 'i' is inside the window

          do j = 1,np
c             find contacts between particle 'i' and walls/disks/
c             heterogeneous particles

            if (i.ne.j) then

                rxi=rx(i)
                ryi=ry(i)
                rdi=rd(i)
                rxj=rx(j)
                ryj=ry(j)
                rdj=rd(j)
                 
                rxij = dabs(rxi - rxj)
                ryij = dabs(ryi - ryj)

              if (j.le.nl) then ! d-l
c                 particle 'j' is a wall

c                 distance between horizontal wall and particle
                  if ( (j.eq.1).or.(j.eq.3) ) dij=ryij-rdi
c                 distance between vertical wall and particle
                  if ( (j.eq.2).or.(j.eq.4) ) dij=rxij-rdi

                if (dij.lt.eps) then
                    ncor = ncor + 1
                    lcor(ncor) = j
                    if (ncor.eq.ncmax) stop 'list too small'
                end if

              else ! d-d

                  rdij=rdi+rdj

c-----            Comparison to reduce run time (eliminate distant particles) 
                if (rxij-rdij.lt.eps) then
                  if (ryij-rdij.lt.eps) then

                      dij= dsqrt(rxij**2 + ryij**2)-rdij

                    if (dij.lt.eps) then
                        ncor = ncor + 1
                        lcor(ncor) = j
                        if (ncor.eq.ncmax) stop 'list too small'
                    endif

                  endif
                endif
              endif


            endif
          enddo
        endif
      enddo
      
       icor(nsp+1) = ncor+1
       
c      zcor = mean coordination number
       zcor = dble(ncor)/dble(ndcor)

       write(*,*) ' coordination number zcor = ',zcor

c      it is possible to calculate mean coordination numbers as a
c      function of the radius of particle 'i'

      return
      end
      

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  depot.f
  
c  The particles are deposited by means of geometric criteria as to prepare
c  an initial configuration (dense or loose)

c
c  Option 1: Criteria for location (sedimentation) of particles
c
c     npg,npw,nphp=1  : 

c                particle is located in the position where potential energy 
c                is minimun (minimize y coord. in the shadow).
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine depot()
      implicit none
      include '../VAR/preglob'

c-----local variables
  
      integer i
      real*8 rcirc

c-------------------------------------------------------------
c The box is already defined through the system data (rxw,ryw,rrotw)
      
c       nd=ndi                  ! number of disks

       nl=4                    ! number of lines in the box
       np=nl+nd                ! number of particles

c      initial conditions for the chain array are specified
       nch=1                   ! initial number of elements in the chain
       lch(0)=2                ! element 2 = left vertical wall
       lch(1)=1                ! element 1 = horizontal base
       lch(2)=4                ! element 4 = right vertical wall

      if (nhp.gt.0) then
        ndhp=0                  ! number of deposited heterogeneous particles
       do i=1,nhp
         inhp(i)=0              !flag indicating if hp is deposited or not
       enddo
      endif


c      the constants used for potential function are defined
c       call potval()

        epsa=0.00000000001d0    ! delta for comparison of angle intervals

      do ip=5,np
         
        call radius(rcirc)     ! this routine is in prepa.f  
        rd(ip)=rcirc

        vx(ip)=0.d0
        vy(ip)=0.d0
        vrot(ip)=0.0d0
        rrot(ip)=0.d0
        iobj(ip)=1

c       coordinates of disk 'ip' are calculated

        call shadow()
        call shadhet()
        call locate()

      enddo  

      if (nhp.gt.0) then

c       the final number of particles must be increased according to the
c       'ndhp'
         nd=nd+ndhp
         np=np+ndhp

       if (ndhp.lt.nhp) then
         write(*,*) ' Number of deposited heterogeneous particles = ',ndhp
         write(*,*) ' is less than ',nhp
       endif

      endif  

      return
      end  
c==============================================================
c     SR potential(x,y,p)
c-----Calculate the value of the potential scalar function p in terms of 
c     the position x,y and the walls and heterogeneous particles.  

      subroutine potential(x,y,p)
      implicit none

      include '../VAR/preglob'

      real*8 x,y,p,dx,dy,d
      integer i

       p=0.D0

      if (npg.eq.1) then
        p=c1*y    ! the value of the potential function depends on the altitude
      endif

      if (npw.eq.1) then
c       the potential function depends on the distance to the vertical walls 
        p=p-c2/(x+c3)-c2/(rxw+c3-x)

      endif

      if (nphp.eq.1) then
c       the potential function depends on the distance to the hps
       do i=1,nhp
         dx=x-xhp(i)
         dy=y-yhp(i)
         d=dsqrt(dx*dx+dy*dy)/rhp(i)
         p=p-chp1/d+chp2*d

       enddo

      endif

      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  depotd.f
  
c  The particles are deposited by means of geometric criteria as to prepare
c  an initial configuration (dense or loose)

c
c  Option 1: Criteria for location (sedimentation) of particles
c

c                particle is located in the position where potential energy 
c                is minimun (minimize y coord. in the shadow).
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine depotd()
      implicit none
      include '../VAR/preglob'

c-----local variables
  
      integer i
      real*8 rcirc

c      nl = number of line segments in the box, including the segments in the base
c      nbld = number of base line disks
       ip=nl+ndel

c      All the line segments are numbered counterclockwise beginning with at the left-down
c      corner of the window
c      initial conditions for the chain array are specified
       nch=ndel                    ! initial number of elements in the chain
       lch(0)=1                ! left vertical wall
       lch(nch+1)=nls+1            !  right vertical wall

       epsa=0.00000000001d0    ! delta for comparison of angle intervals

      do jgu=1,ngu
        xsmin=xgu(1,jgu)
        xsmax=xgu(1,jgu)
        ysmin=ygu(1,jgu)
        ysmax=ygu(1,jgu)
         
c       find the rectangular window that contains the geological unit
       do i=1,npgu(jgu)
         if (xgu(i,jgu).lt.xsmin) xsmin=xgu(i,jgu)
         if (xgu(i,jgu).gt.xsmax) xsmax=xgu(i,jgu)
         if (ygu(i,jgu).lt.ysmin) ysmin=ygu(i,jgu)
         if (ygu(i,jgu).gt.ysmax) ysmax=ygu(i,jgu)
       enddo  

c       Each geological unit is analized
        rdmin=rdming(jgu)
        rdmax=rdmaxg(jgu)
        ngran=ngrang(jgu)
        psp=pspg(jgu)
        pvsp=pvspg(jgu)

       if (ngran.eq.4) then
         psp=1.d0/(1.d0+rdmin*rdmin*(1.d0-pvsp)/(rdmax*rdmax*pvsp))
       endif

        inside=1

       do while (inside.eq.1)
c        deposit a new particle in geological unit 'jgu', while the disk is 
c        beneath the profile
         ip=ip+1
     
         call radius(rcirc)     ! this routine is in prepa.f  
         rd(ip)=rcirc

         vx(ip)=0.d0
         vy(ip)=0.d0
         vrot(ip)=0.0d0
         rrot(ip)=0.d0
         iobj(ip)=1
c        idgd(ip) = number of geological domain associated to particle 'i'
c        this value is calcualted in 'geostruc'
         idgd(ip)=jgu
         call shadowd()
         call geochand()
       enddo

      enddo

        nd=ip-(nl+ndel)

       do i=1,nd
        rd(nl+i)=rd(nl+ndel+i)  
        rx(nl+i)=rx(nl+ndel+i) 
        ry(nl+i)=ry(nl+ndel+i) 
        iobj(nl+i)=iobj(nl+ndel+i)
        idgd(nl+i)=idgd(nl+ndel+i)
       enddo

       np=nl+nd

      return
      end  

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   
      include '../PRE/geochand.f'


c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  prepa.f
c  Shearbox
  
c  Put particles on a regular triangular array in a rectangular box (irun=15).
c  The size of triangles corresponds to twice the radious of the largest 
c  particle. Particles will deposit under gravity forces to create a dense 
c  pile (using contact dynamics)
c
c  Two options: 
c
c      icol=0 : fill a box with a regular triangular grid,
c
c
c      icol=1 : fill a column with nwidth particles at the base (completer!)
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine prepa()
      implicit none
      include '../VAR/preglob'

c-----local variables

      integer n,npr,i,j,k,np1,np2,ndc
      real*8 rl,rtn,rtn2,r3,ryl1,ryl2,rcirc,xo

c-------------------------------------------------------------
c The box is already defined through the system data (rxw,ryw,rrotw)
    

c       the box is filled with a regular triangular mesh of particles

       if (icol.eq.0) then
         xo=0.D0
         rl=rxw/(2.D0*rdmax) 
c        n = maximum number of disks of size rdmax in a row  
c        notice that in 2 shifted rows you must include n+1/2 disks 
         n=idnint(rl)-1          ! idnint(rl) is the nearest integer
c        2 rows of circles fit exactly the horizontal box length  
         rtn=rxw/(dfloat(2*n+1)) ! radius of circles in triangular grid
       else
         xo=xline
         n=nwidth                ! Number of particles in a row (sysdat)
         rtn=rdmax
       endif

        rtn2=2.D0*rtn           ! length of triangle sides
        r3=dsqrt(3.D0)

c       npr = number of pair of rows closest to fixed number of disks 
        npr=nint(float(nd)/float(2*n))
c       notice that the initial number of disks will be changed
        ndc=2*n*npr              ! number of disks in the column

        write(*,*) ' The final number of disks in the column = ',ndc
        pause
        
        nd=ndc
        np=nl+nd               ! number of particles

       do i=1,npr
c        npr = number of pair of rows
         ryl1=rtn+rtn2*r3*dfloat(i-1)        ! y coord. of first row
         ryl2=rtn+rtn2*r3*(dfloat(i)-0.5D0)  ! y coord. of second shifted row
         k=4+(i-1)*2*n

        do j=1,n
c         coordinates and parameters of disks in lower row are calculated 
          np1=k+j
          rx(np1)=xo+rtn+rtn2*dfloat(j-1)
          ry(np1)=ryl1

          call radius(rcirc)
          rd(np1)=rcirc

c         if (ry(np1).gt.rymax) rymax=ry(np1)

          rrot(np1)=0.d0
          vx(np1)=0.d0
          vy(np1)=0.d0
          vrot(np1)=0.0d0
            
          iobj(np1)=1

c         coordinates and parameters of disks in upper row are calculated
          np2=k+n+j
          rx(np2)=xo+rtn2*dfloat(j)
          ry(np2)=ryl2

          call radius(rcirc)
          rd(np2)=rcirc

          rrot(np2)=0.d0
          vx(np2)=0.d0
          vy(np2)=0.d0
          vrot(np2)=0.0d0
            
          iobj(np2)=1

        enddo

       enddo
      
      return
      end  
c==============================================================
c     SR radius(x,y,teta,xp,yp)
c-----Calculate coordinates of a point, after counter-clockwise teta rotation 

      subroutine radius(rcirc)
      implicit none
      include '../VAR/preglob'

      real*8 rcirc,ranf,ran

c      ran = normalised random number value (between (0, 1) calculated from
c      a uniform probability density function generator (ranf())
       ran=ranf()

c      uniform distribution of radius between (rdmin,rdmax)
       rcirc=rdmin+ran*(rdmax-rdmin)
 
      if (ngran.eq.2) then
c       uniform volume distribution for radius between (rdmin,rdmax)
        rcirc=rdmin*rdmax/((rdmin+rdmax)-rcirc)     
      endif

      if (ngran.eq.3) then
c       two particle size distribution function: rdmin and rdmax
c       porcentual number of particles of each size is fixed (ppp = rdmin)
       if (ran.le.psp) then
         rcirc=rdmin
       else
         rcirc=rdmax
       endif
      endif

      if (ngran.eq.4) then
c       two particle size distribution function: rdmin and rdmax
c       porcentual volume of particles of each size is fixed (pvpp = rdmin).
c       The value of psp is calculated from pvpp, rdmin and rdmax.
       if (ran.le.psp) then
         rcirc=rdmin
       else
         rcirc=rdmax
       endif
      endif

      return
      end

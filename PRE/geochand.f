c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR geochand()

c     associates disks to different geologic units with different
c     mechanical parameters.

c     Geological units should be defined by a profile function between (0,rxw),
c     from left to right [jtgu(jgu)=1], or by a closed polyline [jtgu(jgu)=2],
c     defined clockwise.
c     Partilces belong to a given layer as long as its centre is located inside
c     the geological unit. When the position of a new particle is outside the
c     domain then it will be the last particle inside the domain and the layer
c     number is increased.
c

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine geochand()
      implicit none
      include '../VAR/preglob'

c-----Local variables

      integer i,k,nps1,indi,nints,ingu
      integer imin,imout,nmin,norchi,norchf,ntrans

      real*8 x1,x2,y1,y2,x0,x3
      real*8 sm,sb,xpar,ypar,rdi,ygus,dpsmin,dpseg,dpotin,dpout
      real*8 svx,svy,sv,vnx,vny,vpx,vpy,vp,vp2,dp,ds


c -   calculate the coordinates of points in the compressed list, where
c     the new particle may be deposited

      do i=1,nchf
         
        nps1=lch(ncf(i))

c       ! arc segment

         rdi = rd(nps1)+rd(ip)
         xch(i) = rx(nps1)+rdi*cos(anca(i))
         ych(i) = ry(nps1)+rdi*sin(anca(i))

        if (i.eq.nchf) then
          xch(nchf+1) = rx(nps1)+rdi*cos(ancs(i))
          ych(nchf+1) = ry(nps1)+rdi*sin(ancs(i))
        endif

      enddo
        
c      Calculate

c      inside = 0  : all the points are outside the geological unit 
c               1  : at least one point is inside the geological unit
       inside=0
       dpotin=0.d0

      do i=1,nchf+1
c       analize the potential energy in all the points of the comp. list
        xpar=xch(i)
        ypar=ych(i)

        indi=0
            
c       Find out if point 'i' belongs to the geological unit 'jgu':
c       indi = 0/1 : the disk is outside / inside the domain
       if (ypar.lt.ysmin) then
         indi=1    ! the point is "inside" the slope (beneath ysmin)
       else
         nints=0 ! check if point is "inside" the slope.
c        (i.e. neither outside nor in the boundary)
         ingu=0
         if (ypar.lt.ysmax) ingu=1
        if (ingu.eq.1) then
c         the disk center may be inside the geological unit
c         The number of intersections between a vertical line with lower point
c         (xpar,ypar) and the geological unit profile, is calculated.
         do k=2,npgu(jgu)     ! line segments in geol. unit are analized
           x1=xgu(k-1,jgu)
           x2=xgu(k,jgu)
           y1=ygu(k-1,jgu)
           y2=ygu(k,jgu)
          if (x1.ne.x2) then
           if ((x1.lt.xpar).and.(xpar.le.x2)) then
c            xpar belongs to (x1,x2], an 'upper' profile line segment
             sm=(y2-y1)/(x2-x1)
             sb=y1-sm*x1
             ygus=sm*xpar+sb
c            centre of disk is below the line segment
             if (ypar.lt.ygus) nints=nints+1                
           else
            if ((x2.le.xpar).and.(xpar.lt.x1)) then
c             xpar belongs to (x2,x1], a 'lower' profile line segment
              sm=(y2-y1)/(x2-x1)
              sb=y1-sm*x1
              ygus=sm*xpar+sb
c             centre of disk is below or along the line segment
              if (ypar.le.ygus) nints=nints+1   
            endif
           endif
          else
           if ( (xpar.eq.x1).and.(ypar.lt.dmax1(y1,y2)) ) then
c            the center of disk is along a vertical axis passing by 
c            a boundary line segment, below the upper point
c            x0 / x3 = x coord. of points located previous / after line segment
c            in geological unit 
c            in this case k.ge.3 since vertical lines are inside the profile
             x0=xgu(k-2,jgu)
             x3=xgu(k+1,jgu)
            if ( ((x0-xpar)*(x3-xpar)).gt.0.d0 ) then
c             the vertical segment is the boundary of an 'overhang'
             if (ypar.lt.dmin1(y1,y2)) then
               nints=nints+1
             else
c              ypar is in the interval [ymin,ymax)
               if (y1.gt.y2) nints=nints+1
             endif
            else
c             the vertical segment is a 'wall' along the profile
c             ypar is in interval [ymin,ymax)
              if (y1.gt.y2) nints=nints+1
            endif
           endif
          endif
         enddo
        endif
       endif

c-------Find out the point where the particle ought to be deposited
c       according to potential function criteria (distance to geol. unit)
       if ( (indi.eq.1).or.(mod(nints,2).eq.1) ) then
c ----   if the number of intersections 'nints' is odd then
c        the centre of the particle (disk) is 'inside' the slope.
         if (inside.eq.0) inside=1
        do k=2,npgu(jgu)
          x1=xgu(k-1,jgu)
          x2=xgu(k,jgu)
          y1=ygu(k-1,jgu)
          y2=ygu(k,jgu)
c         sv = vector parallel to line segment in slope
          svx=x2-x1
          svy=y2-y1
          sv=dsqrt(svx**2+svy**2)
c         vn = normalised vector parallel to sv
          vnx=svx/sv
          vny=svy/sv
c         vp = vector from initial segment point to centre of disk
          vpx=xpar-x1
          vpy=ypar-y1
          vp=dsqrt(vpx**2+vpy**2)
          if (k.eq.2) dpsmin=vp
c         dp = perpend. distance from slope segment to center of disk 
          dp=abs((vnx*vpy)-(vny*vpx)) 
c         ds = parallel distance from initial point in slope
c         segment to center of disk
          ds=((vnx*vpx)+(vny*vpy))
         if ( (ds.ge.0.d0).and.(ds.le.sv) ) then
c          the perpendicular vector between the point and the line segment
c          is in interval (0, sv)
           dpseg=dp
         else
           dpseg=vp
         endif
          dpsmin=dmin1(dpsmin,dpseg)
        enddo 
c        vp2 = distance to the last point in the geological profile
         vp2=dsqrt( (xpar-x2)**2+(ypar-y2)**2 )
c        shortest distance between the point and line segments is dpsmin
         dpsmin=dmin1(dpsmin,vp2)
      
        if (dpsmin.gt.dpotin) then
c         dpotin = greatest distance between the points in the chain and their 
c         corresponding closest distance to line seg. (negative of potential energy)
          dpotin=dpsmin
c         imin = position of the point located at distance dpotin
          imin=i
        endif
       else 
        if (inside.eq.0) then
c         if all the analized points are, up to now, outside the geological unit,
c         then look for the closest point to the geological unit :
         do k=2,npgu(jgu)
           x1=xgu(k-1,jgu)
           x2=xgu(k,jgu)
           y1=ygu(k-1,jgu)
           y2=ygu(k,jgu)
c          sv = vector parallel to line segment in slope
           svx=x2-x1
           svy=y2-y1
           sv=dsqrt(svx**2+svy**2)
c          vn = normalised vector parallel to sv
           vnx=svx/sv
           vny=svy/sv
c          vp = vector from initial segment point to centre of disk
           vpx=xpar-x1
           vpy=ypar-y1
           vp=dsqrt(vpx**2+vpy**2)
           if (k.eq.2) dpsmin=vp
c          dpout is inicialized for the first particle and first segment
           if ( (i.eq.1).and.(k.eq.2) ) dpout=vp
c          dp = perpend. distance from slope segment to center of disk 
           dp=abs((vnx*vpy)-(vny*vpx)) 
c          ds = parallel distance from initial point in slope
c          segment to center of disk
           ds=((vnx*vpx)+(vny*vpy))
          if ( (ds.ge.0.d0).and.(ds.le.sv) ) then
c           the perpendicular vector between the point and the line segment
c           is in interval (0, sv)
            dpseg=dp
          else
            dpseg=vp
          endif
           dpsmin=dmin1(dpsmin,dpseg)
         enddo 
c         vp2 = distance to the last point in the geological profile
          vp2=dsqrt( (xpar-x2)**2+(ypar-y2)**2 )
c         shortest distance between the point and line segments is dpsmin
          dpsmin=dmin1(dpsmin,vp2)
      
         if (dpsmin.lt.dpout) then
c          dpout = shortest distance between the points in the chain and their 
c          corresponding closest distance to line seg. (negative of potential energy)
           dpout=dpsmin
c          imout = position of the point located at distance dpotin
           imout=i
         endif

        endif
       endif 
      enddo ! loop in points in the chain : i=1,nchf+1

c      The location of the new particle corresponds to the point where the potential 
c      energy is the lowest (i.e. the most distant point to top of the geol. unit) 

c      The potential energy only takes into consideration the potential function
c      that depends on the distance to the geological unit. To have a discontinuous
c      set of particles along the boundary of the model (walls 1, 3, 4) you may impose
c      an initial chain, and then deposit the others. This initial chain should allow 
c      to impose varying velocities to rigid particles that are not in contact. 

       ncf(nchf+1)=nch+1     ! last element in the final chain = right wall

c      the chain is modified according to the new element

      if (inside.eq.0) then
c       all the points are outside thus the geological unit must be changed
        k=imout
      else
c       at least one point is inside the geol. unit
        k=imin
      endif
c      k indicates the point where the particle should be deposited
       nmin=ncf(k)
       rx(ip)=xch(k)
       ry(ip)=ych(k)

      if (k.eq.nchf+1) then
c       add a new particle at the end of the compressed chain and trim the end
        lch(ncf(nchf)+1)=ip
        nch=ncf(nchf)+1
c       vertical wall is the line segment 'npbs'
        lch(nch+1)=npbs

      else
       if (k.eq.1) then
c        insert a new particle at the beginning of the compressed chain and shift
c        the remainning chain by one position
         norchi=nmin

c        first line segment in the base
         norchf=2
       else
c        insert a new particle inside the compressed chain and shift
c        the remainning chain by one position
         norchi=nmin
         norchf=ncf(k-1)+2
       endif
        ntrans=norchf-norchi
 
       if (ntrans.eq.1) then
 
        do i=nch+1,norchi,-1    ! step is negative
          lch(i+ntrans)=lch(i)
        enddo
       endif
c       if translation = 0 then it is not necessary to shift the chain
       if (ntrans.lt.0) then 
 
        do i=norchi,nch+1   
          lch(i+ntrans)=lch(i)
        enddo
       endif
 
        lch(norchf-1)=ip
        nch=nch+ntrans
      endif
      
       nosha=5
cc       call dispcirc()

      return
      end

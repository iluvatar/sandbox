c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  packfrac.f
  
c  Calculate the mean packing fraction of particles deposited. 

c  Packing fraction is defined as the ratio between the area of the disks and
c  the total area of the system.

c  this routine is called in create, after sedimentation of particles and big
c  heterogeneous particles.

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine packfrac()
      implicit none
      include '../VAR/preglob'

c-----local variables
  
      integer i,i0,i1,i2,nsp
      real*8 x0,y0,x1,y1,rad,np1,np2,v1,v2,v,nn1,nn2
      real*8 alfa,npvnn,gama,sout

c-------------------------------------------------------------
c The box is already defined through the system data (rxw,ryw,rrotw)

c     The surface is subdivided in trapesoidal areas with vertical sides,
c     limited by the center of circles in the final chain.

c      ssys = total surface of the system of particles

c      area of rectangles adjacent to the vertical walls of the box

       ssys=rx(lch(1))*ry(lch(1))+(rxw-rx(lch(nch)))*ry(lch(nch))

c      nsp = number of walls + small disks (excluding het. particles)
       nsp=np-ndhp

      do i=2,nch
c       the area of the 'i' trapesoide is calculated
        i0=lch(i-1)
        x0=rx(i0)
        y0=ry(i0)
        i1=lch(i)
        x1=rx(i1)
        y1=ry(i1)
        ssys=ssys+(x1-x0)*(y0+y1)/2.d0
      enddo

      if (ndhp.gt.0) then
c       np = nsp+ndhp (ndhp is number of deposited heterogeneous particles)
       do i=nsp+1,np
c        the surface of deposited big particles is eliminated in order to
c        calculate paking fraction in the remaining zone.
         rad=rd(i)
         ssys=ssys-pi*rad*rad
       enddo
      endif

c      spar = surface ocupied by the particles in the system
       spar=0.d0

      do i=nl+1,nsp

        rad=rd(i)
        spar=spar+pi*rad*rad

      enddo

c      npr = (np1,np2) = normalised vector between centers of 
c                  particles 'i-1' (previous) and 'i'
       np1=1.d0
       np2=0.d0

      do i=1,nch
c       calculate areas of particles zones that are outside the system surface
        i1=lch(i)
       if (i.eq.nch) then
c        nne =(nn1,nn2) = normalised vector between centers of particles 'i' 
c        and 'i+1' (next)
         nn1=1.d0
         nn2=0.d0
       else
         i2=lch(i+1)
         v1=rx(i2)-rx(i1)
         v2=ry(i2)-ry(i1)
         v=dsqrt(v1*v1+v2*v2)
         nn1=v1/v
         nn2=v2/v
       endif

        alfa=dacos(np1*nn1+np2*nn2)  ! alfa is grater or equal than zero
        npvnn=np1*nn2-np2*nn1
        if (npvnn.lt.0.d0) alfa=-alfa
c       gama = external angle between vectors 'npr' and 'nne'
        gama=pi-alfa
c       sout = surface or the particle located outside of the system
        sout=rd(i1)*rd(i1)*gama/2.d0
       if (i1.le.nsp) then
c        'sout' is substracted from the surface of particles
         spar=spar-sout
       else
c        note that the whole area of het part. had been substracted from ssys,
c        thus we have to addition 'sout' to ssys
         ssys=ssys+sout
       endif
c       normalised previous vector in next step is defined according to 'nne'
        np1=nn1
        np2=nn2
      enddo

       pkf=spar/ssys
       write(*,*) ' packing fraction =',pkf

      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  pre.f : preprocesor for program main.f / (biax.f, geo.f)
  
c  Version : 1.
c  Date    : january 2002

c  System = shearbox  
c  A box with four walls and three degrees of freedom
c  Authors  : A. Taboada, J. Chang, F. Radjai

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      program pre_SandBox
      implicit none 
      include '../VAR/preglob'

c-----Constants

      fnorm=1.d0/(268435456.d0*268435456.d0)
      pi = 4.d0 * datan(1.d0)
      pi2=2.d0*datan(1.d0)
      pit2 = 8.d0 * datan(1.d0)
      de2ra=pi/180.d0      
      ra2de=180.d0/pi

c     time clock is set to zero

      tm=0.D0

c     seeds for random function in generation of disk radii

      seed1=238463
      seed2=8790123  

c-----Read flow parameters 

      open(1,file='./sample_creation/pre_parameters',status='unknown')

      read(1,*) irun

      if(irun.eq.0) then
      stop 'No-execution start mode. Change mode!'      
      end if

      read(1,*) ixsize,iysize
      read(1,*) izoom,xmin,ymin,xmax,ymax
      read(1,*) imarkgd,imark,lah,lat
      read(1,*) igeou
      read(1,*) vgeou,vfrac,vslope

      close(1)

      call create()

C-----reset flow variables 

cc 10   pause

cc      open(1,file='fpre',status='unknown')

cc      read(1,*) irun

cc      if(irun.eq.0) then
cc      stop 'No-execution start mode. Change mode!'      
cc      end if

cc      read(1,*) ixsize,iysize
cc      read(1,*) izoom,xmin,ymin,xmax,ymax

cc      close(1)

cc      call predisp()

cc      goto 10

      end
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c-----Computation routines

      include '../PRE/create.f'
      include '../LIB/io.f'


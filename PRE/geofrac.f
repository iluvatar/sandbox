c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR geofrac()

c     associates disks in different geologic units to fractures and joints
c     that have modified mechanical parameters.

c     A fracture is defined by two points (0,rxw)

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine geofrac()
      implicit none
      include '../VAR/preglob'

c-----Local variables

      integer i,j,k,igufr,indfr

      real*8 x1,x2,y1,y2,xfmin,xfmax,yfmin,yfmax,xpar,ypar,rpar
      real*8 svx,svy,sv,vnx,vny,vpx,vpy,dp,ds,iwhfr

c ------ Removing particles above lines

c      indfr = number of geological unit associated to a fracture segment
c      Fracture segment mechanical properties will be stocked after
c      geological units in the same data structure.

      indfr=0

      if (irun.eq.20) then
       do i=1,np
        if (indfr.lt.(idgd(i))) indfr=idgd(i)
       enddo
      endif
 
      do j=1,nfrac
c       nfrac = number of fractures
        x1=xfrac(1,j)
        y1=yfrac(1,j)
        x2=xfrac(2,j)
        y2=yfrac(2,j)

        xfmin=dmin1(x1,x2)
        xfmax=dmax1(x1,x2)
        yfmin=dmin1(y1,y2)
        yfmax=dmax1(y1,y2)

       do i=1,gufrac(j)
         indfr=indfr+1

c        igufr = number of the geological unit affected by fracture 'j' 
         igufr=ngufr(i,j)
c        iwhfr = half width of fracture 'j' across geol. unit 'i' 
         iwhfr=whfrac(i,j)

c        find the rectangular window that contains the fracture 'i' with
c        thickness 'iwhfr'

         xsmin=xfmin-iwhfr
         xsmax=xfmax+iwhfr
         ysmin=yfmin-iwhfr
         ysmax=yfmax+iwhfr
         
        do k=5,np

         if (idgd(k).eq.igufr) then
c          particle 'k' belongs to geol. domain (unit) 'j'

           xpar=rx(k)
           ypar=ry(k)
           rpar=rd(k)

          if ( (xpar.gt.xsmin).and.(xpar.lt.xsmax) ) then
           if ( (ypar.gt.ysmin).and.(ypar.lt.ysmax) ) then
c            the central point of particle 'k' is located inside rect. window

             svx=x2-x1
             svy=y2-y1
             sv=dsqrt(svx**2+svy**2)
c            vn = normalised vector parallel to sv
             vnx=svx/sv
             vny=svy/sv
c            vp = vector from initial segment point to centre of disk
             vpx=xpar-x1
             vpy=ypar-y1
c            dp = perpend. distance from slope segment to center of disk 
             dp=abs((vnx*vpy)-(vny*vpx))

c            ncfr = number of criterium for association of geological units:
c            ncfr = 1 : particles that have their centres inside the window
c            ncfr = 2 : particles that are entirely located inside the window 

             if (ncfr.eq.2) dp=dp+rpar

            if (dp.lt.iwhfr) then
c             perpendicular dist. to the fracture is less than half thickness

c             ds = parallel distance from initial point in slope
c             segment to center of disk
              ds=((vnx*vpx)+(vny*vpy))

             if (ncfr.eq.1) then
              if ( (ds.gt.0.d0).and.(ds.lt.sv) ) then
c               the center of the particle is inside the fracture
                idgd(k)=indfr
              endif
             else
              if ( (ds-rpar.gt.0.d0).and.(ds+rpar.lt.sv) ) then
c               the whole particle is inside the fracture
                idgd(k)=indfr
              endif
             endif
            
            endif
           endif
          endif  
         endif
        enddo ! k loop around particle list

       enddo ! i loop aroundgeological unit list

      enddo ! j loop around fracture list

      return
      end

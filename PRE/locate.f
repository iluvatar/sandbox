c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR locate()  

c     calculate the coordinates of a particle according to a specified
c     criteria. This routine is called in 'depot.f'.

c  Option 1: Criteria for location (sedimentation) of particles
c
c     npg,npw,nphp=1  : 
c     particle is located in the position where potential energy 
c     is minimun (minimize y coord. in the shadow). It is centred
c     along horizontal line segments.
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine locate()
      implicit none
      include '../VAR/preglob'

c local variables
      
      integer i,nps1,nmin,inds,imin,norchi,norchf,ntrans
      real*8 xminc,yminc,r1,x1,y1,pmin,pot,alfa

       if ((nhp.gt.0).and.(nihp.gt.0)) then      
c        the shadows between the final chain and hp do intersect
        do i=1,nihp
          alfa=ahid(i)
          nps1=nhid(i)
          r1=rhp(nps1)+rd(ip)
          x1=xhp(nps1)+r1*dcos(alfa)
          y1=yhp(nps1)+r1*dsin(alfa)

          call potential(x1,y1,pot)
         if (i.eq.1) then
           imin=1
           pmin=pot
         else
          if (pot.lt.pmin) then
            pmin=pot
            imin=i
          endif
         endif
        enddo

         alfa=ahid(imin)
         nps1=nhid(imin)
         r1=rhp(nps1)+rd(ip)
         rx(ip)=xhp(nps1)+r1*dcos(alfa)
         ry(ip)=yhp(nps1)+r1*dsin(alfa)

         ndhp=ndhp+1
         inhp(nhid(imin))=1
         norchi=ncf(ndih(imin))
         norchf=norchi+4
         ntrans=4

        do i=nch+1,norchi,-1
          lch(i+ntrans)=lch(i)
        enddo

         lch(norchi+1)=ip
         lch(norchi+2)=np+ndhp
         lch(norchi+3)=ip
         nch=nch+4
         i=nhid(imin)   ! i = number of the hp corresponding to minimum

         rx(np+ndhp)=xhp(i)
         ry(np+ndhp)=yhp(i)
         rd(np+ndhp)=rhp(i)

         vx(np+ndhp)=0.d0
         vy(np+ndhp)=0.d0
         rrot(np+ndhp)=0.d0
         vrot(np+ndhp)=0.0d0
            
         iobj(np+ndhp)=1

       else   ! the disk is located in the minima of the final chain shadow
c        the shadows between the final chain and hp do not intersect

         ncf(nchf+1)=nch+1     ! last element in the final chain = right wall
         nps1=lch(ncf(1))
         nmin=ncf(1)
         inds=0

        if ( (npg.eq.1).and.((npw.eq.0).and.(nphp.eq.0)) ) then   
c         potential energy function depends only on 'y' position

         if (nps1.eq.1) then
c          new particle is centred along horizontal line segment
           xminc=(anca(1)+ancs(1))/2.D0
           yminc=rd(ip)
         else
           r1=rd(nps1)+rd(ip)
           xminc=rx(nps1)+r1*dcos(anca(1))
           yminc=ry(nps1)+r1*dsin(anca(1))
           i=1

 10       if (i.gt.nchf) goto 20   ! while structure between 10,20

            nps1=lch(ncf(i))
           if (nps1.eq.1) then
c            new particle is centred along horizontal line segment
             xminc=((anca(i)+ancs(i))/2.D0)   
             yminc=rd(ip)
             nmin=ncf(i)
             i=nchf+1
           else
             r1=rd(nps1)+rd(ip)
             y1=ry(nps1)+r1*dsin(ancs(i))
            if (y1.lt.yminc) then
              xminc=rx(nps1)+r1*dcos(ancs(i))
              yminc=y1
              imin=i
              nmin=ncf(i)
              inds=1
            endif
             i=i+1
           endif 
 
          goto 10                    ! end of while structure
 20       continue
 
         endif

        else
c         potential energy function depends on vertical walls (npw=1),
c         and on 'hp' and vertical walls (nphp=1)

         if (nps1.eq.1) then ! first particle is a horizontal line segment
c          minima is located at left corner of horizontal line shadow
           xminc=anca(1)
           yminc=rd(ip)
         else                ! first particle is a disk
c          minima is located at limit of angular interval with prev. particles
           r1=rd(nps1)+rd(ip)
           xminc=rx(nps1)+r1*dcos(anca(1))
           yminc=ry(nps1)+r1*dsin(anca(1))
         endif

          call potential(xminc,yminc,pmin)

         do i=1,nchf

           nps1=lch(ncf(i))
          if (nps1.eq.1) then
c           right corner of horizontal line shadow is tested
            x1=ancs(i)   
            y1=rd(ip)
          else
c           right boundary of angular interval is tested
            r1=rd(nps1)+rd(ip)
            x1=rx(nps1)+r1*dcos(ancs(i))
            y1=ry(nps1)+r1*dsin(ancs(i))
          endif

           call potential(x1,y1,pot)

          if (pot.lt.pmin) then
            xminc=x1
            yminc=y1
            imin=i
            nmin=ncf(i)
            inds=1
            pmin=pot
          endif
 
         enddo

        endif

c        the chain is modified according to the new element
         rx(ip)=xminc
         ry(ip)=yminc

        if ( ( (npg.eq.1).and.((npw.eq.0).and.(nphp.eq.0)) ).and.
     &     (lch(nmin).eq.1) ) then

         do i=nch+1,nmin,-1         ! step is negative
           lch(i+2)=lch(i)
         enddo
          lch(nmin+1)=ip
          nch=nch+2
        else   ! the minimum is not centred along a horizontal line segment
           
         if (inds.eq.0) then
           norchi=nmin
           norchf=2
         else
           norchi=ncf(imin+1)
           norchf=nmin+2
         endif
 
          ntrans=norchf-norchi
 
         if (ntrans.eq.1) then
 
          do i=nch+1,norchi,-1    ! step is negative
            lch(i+ntrans)=lch(i)
          enddo
         endif
 
         if (ntrans.lt.0) then 
 
          do i=norchi,nch+1   
            lch(i+ntrans)=lch(i)
          enddo
         endif
 
          lch(norchf-1)=ip
          nch=nch+ntrans
        endif

       endif
 
c         nosha=5
c         call dispcirc()

      return
      end
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  pretrans.f
  
c  Transduce system variables into internal variables :
c  give initial values to position and velocity of the walls
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine pretrans()
      implicit none
      include '../VAR/preglob'

c-----Local variables

      integer i
      
c-----Fixed positions and driving speeds and forces along the walls---------
      
c        initial particle system is calculated by pre.f considering a
c        rectangular box

       do i=1,nl

         iobj(i)=2

         rd(i)=0.d0
         rx(i)=0.d0
         ry(i)=0.d0
         rrot(i)=0.d0
      
         vx(i)=0.d0
         vy(i)=0.d0
         vrot(i)=0.d0

       enddo

        rrot(2)=rrotw
        rrot(4)=rrotw

        rx(4)=rxw

       if (irun.eq.15) then
         ry(3)=ry(np)+rdmax        
       else
         ry(3)=ryw      
       endif
       
      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR shadhet()  

c     calculate the intersection between the shadow of the chain 
c     of particles and the heterogeneous "big" particles that are
c     located at known positions
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine shadhet()
      implicit none
      include '../VAR/preglob'

c     local variables
      
      integer i,j,k,nps1,indi
      real*8 x1,y1,r1,xh,yh,rh,v1hx,v1hy,v1h,xint,ximin,ximax
      real*8 alfa,alfa1,alfa2,gama,beta,teta,alfaa,alfas

c      'het', 'hp' = heterogeneous "big" particles
       nihp=0   !number of intersections between the shadow and an hps
      if ((nhp.gt.0).and.(ndhp.lt.nhp)) then 

       do i=1,nhp   !number of heterogeneous "big" particles
          
        if (inhp(i).eq.0) then    ! flag for hp i
          xh=xhp(i)
          yh=yhp(i)
          rh=rhp(i)+rd(ip)

         do j=1,nchf   !number of particles in the final clipped shadow
           
           nps1=lch(ncf(j))

          if (nps1.gt.4) then     !intersection between disk shadow and hp

            r1=rd(nps1)+rd(ip)
            y1=ry(nps1)
            v1hy=yh-y1

           if (dabs(v1hy).lt.r1+rh) then

             x1=rx(nps1)
             v1hx=xh-x1

            if (dabs(v1hx).lt.r1+rh) then

              v1h=dsqrt(v1hx*v1hx+v1hy*v1hy)

             if (v1h.le.r1+rh) then

               alfaa=anca(j)
               alfas=ancs(j)
               teta=dacos(v1hx/v1h)
               if (v1hy.lt.0.d0) teta=-teta
               gama=dacos((v1h*v1h+r1*r1-rh*rh)/(2.d0*v1h*r1))
               alfa1=teta+gama
               beta=dacos((v1h*v1h+rh*rh-r1*r1)/(2.D0*v1h*rh))
               alfa2=teta+pi-beta

              do k=1,2 !two posible intersections between disk shadow and hp
                
                indi=0
                if (k.eq.2) alfa1=teta-gama
                if (alfa1.lt.0.d0) alfa1=alfa1+pit2

               if (alfa1.ge.alfas) then

                 if (alfa1.le.alfaa) indi=1

               else
                if (alfa1+pit2.le.alfaa) then
                  alfa1=alfa1+pit2
                  indi=1
                endif
               endif
                
               if (indi.eq.1) then

                 if (k.eq.2) alfa2=teta+pi+beta
                 if (alfa2.lt.0.D0) alfa2=alfa2+pit2
                 if (alfa2.ge.pit2) alfa2=alfa2-pit2

                 nihp=nihp+1        !# of intersections between shadow and hp
                 ndih(nihp)=j       !position of the disk in the chain 
                 nhid(nihp)=i       !# of hp intersecting with disk j
                 adih(nihp)=alfa1   !angle of intersection related to disk
                 ahid(nihp)=alfa2   !angle of intersection related to hp
               endif
              enddo
             endif
            endif
           endif

          else    !intersection between the base line shadow and hp

           if (nps1.eq.1) then
             ximin=anca(j)
             ximax=ancs(j)
            if (yh-rh.le.rd(ip)) then
             if ((xh+rh.ge.ximin).and.(xh-rh.le.ximax)) then
               gama=dacos((rd(ip)-yh)/rh)
               alfa=pi2+gama
              do k=1,2   !two posible intersections between disk shadow and hp
                indi=0
                if (k.eq.2) alfa=pi2-gama
                xint=xh+rh*dcos(alfa)
               if ((xint.ge.ximin).and.(xint.le.ximax)) then
                 nihp=nihp+1
                 ndih(nihp)=j
                 nhid(nihp)=i
                 adih(nihp)=xint
                 ahid(nihp)=alfa
               endif
              enddo
             endif
            endif
           endif
          endif
         enddo
        endif
       enddo
      endif

      if ((nhp.gt.0).and.(nihp.gt.0)) then      
c        the shadows between the final chain and hp do intersect
         nosha=4
cc         call dispcirc()
      endif

      return
      end

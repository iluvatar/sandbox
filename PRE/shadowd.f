c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR shadowd()  

c     calculate the chain of particles and line segments above which the 
c     new particle ought to be located (deposited)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine shadowd()
      implicit none
      include '../VAR/preglob'

c local variables
      
      integer i,j,k,nps0,nps1,nps2,nchmin,nchmax,ind1,ind2
      real*8 x1,y1,r1,x2,y2,r2,v1x,v1y,v1,v2x,v2y,v2,v1sv2,v1vv2
      real*8 xvsh,xint,ximin,ximax,ymaxw,rsh,dx,dy,a,a12,a2,ab,ac
      real*8 sa,sb,sc,alfa,beta,gama,teta,alfaa,alfas,alfa1,alfa2
      real*8 va,vax,vay,v1v2

c     nls+3, nls+1 = vertical left and right walls bounding the box            
      lch(nch+1)=nls+1

       nps1=lch(1)

c       first element in the chain is a circle
        anca(1)=dacos((rd(ip)-rd(nps1))/(rd(ip)+rd(nps1)))

       nps1=lch(nch)

c       last element in the chain is a circle
        ancs(nch)=dacos((rd(nps1)-rd(ip))/(rd(nps1)+rd(ip)))

       a=0.D0
       v2x=1.D0
       v2y=0.D0
       v2=1.D0
      do i=1,nch-1
c       nps0,nps1,nps2= number of particles in the chain to be analized
        nps1=lch(i)
        nps2=lch(i+1)
        nps0=lch(i-1)

c        the elements 'i' and 'i+1' are both circles (general case) 
         x1=rx(nps1)
         y1=ry(nps1)
         r1=rd(nps1)
         x2=rx(nps2)
         y2=ry(nps2)
         r2=rd(nps2)
         v1x=v2x
         v1y=v2y
         v1=v2
         v2x=x2-x1
         v2y=y2-y1
         v2=r1+r2
         sa=v2
         sb=r1+rd(ip)
         sc=r2+rd(ip)
         ab=dacos((sa*sa+sc*sc-sb*sb)/(2.D0*sa*sc))
         ac=dacos((sa*sa+sb*sb-sc*sc)/(2.D0*sa*sb))
 
        if (nps0.eq.nps2) then
          a12=-pi
        else
          v1sv2=v1x*v2x+v1y*v2y
          v1vv2=v1x*v2y-v2x*v1y
          v1v2=v1*v2
          call arcos(v1sv2,v1vv2,v1v2,a12)
        endif
 
         a2=a+a12
         ancs(i)=a2+ac
 
        if (lch(i+2).eq.nls+1) then
          v2x=-v2x
          v2y=-v2y
          call arcos(v2x,v2y,v2,a)
          if (a.lt.0.D0) a=a+pit2
          anca(i+1)=a-ab
        endif
 
        if (lch(i+2).ge.nl+1) then
          call arcos(v2x,v2y,v2,a)
          if (a.lt.-pi2) a=a+pit2
          anca(i+1)=a+pi-ab
        endif

      enddo  

      do i=1,nch
       if (lch(i).ge.nl+1) then
        if (ancs(i).lt.0.D0) then
          ancs(i)=ancs(i)+pit2
          anca(i)=anca(i)+pit2
        endif
        if (ancs(i).ge.pit2) then
          ancs(i)=ancs(i)-pit2
          anca(i)=anca(i)-pit2
        endif  
       endif
      enddo

        nosha=1
cc        call dispcirc()

c     Find out which element defines the uppermost intersection with left wall

       ymaxw=0.D0
      do i=1,nch

        nps1=lch(i)

        if (ancs(i).le.anca(i)) then   ! the angular interval is positive
          x1=rx(nps1)
          y1=ry(nps1)
          r1=rd(nps1)
          rsh=r1+rd(ip)
         if (x1-rsh.lt.rd(ip)) then
           dx=rd(ip)-x1
           dy=dsqrt(rsh*rsh-dx*dx)
           alfa=dacos(dx/rsh)
          if (alfa.ge.ancs(i)) then
           if ((i.eq.1).or.(alfa.le.anca(i))) then
            if (y1+dy.ge.ymaxw) then
              ymaxw=y1+dy
              nchmin=i
              anca(i)=alfa
            endif
           endif
          else
           if ((i.eq.1).or.(alfa+pit2.le.anca(i))) then
            if (y1+dy.ge.ymaxw) then
              ymaxw=y1+dy
              nchmin=i
              anca(i)=alfa+pit2
            endif
           endif
          endif
         endif
        endif

      enddo 

c     Find out which element defines the uppermost intersection with right wall

       ymaxw=0.D0
       xvsh=rxw-rd(ip)                  ! rxw = variable global
      do i=nch,1,-1

        nps1=lch(i)

        if (ancs(i).le.anca(i)) then   ! the angular interval is positive
          x1=rx(nps1)
          y1=ry(nps1)
          r1=rd(nps1)
          rsh=r1+rd(ip)
         if (x1+rsh.gt.xvsh) then
           dx=xvsh-x1
           dy=dsqrt(rsh*rsh-dx*dx)
           alfa=dacos(dx/rsh)
          if ((i.eq.nch).or.(alfa.ge.ancs(i))) then
           if (alfa.le.anca(i)) then
            if (y1+dy.gt.ymaxw) then
              ymaxw=y1+dy
              nchmax=i
              ancs(i)=alfa
            endif
           endif
          else
           if ((i.eq.nch).or.(alfa+pit2.le.anca(i))) then
            if (y1+dy.ge.ymaxw) then
              ymaxw=y1+dy
              nchmax=i
              ancs(i)=alfa+pit2
            endif
           endif
          endif
         endif
        endif

      enddo 

c     compress the list of elements in the chain, selecting those with 
c     positive intervals

       nchc=0
      do i=nchmin,nchmax
        nps1=lch(i)

        if (anca(i).gt.ancs(i)) then
          nchc=nchc+1
          anca(nchc)=anca(i)
          ancs(nchc)=ancs(i)
          ncf(nchc)=i
        endif
      enddo

        nosha=2
cc        call dispcirc()

c     calculate the final list of elements which defines the shadow
c     where the new particle ought to be located.

       i=1
       nchf=1

 10   if (i.ge.nchc) goto 20     ! while structure

        nps1=lch(ncf(i))

       if (nps1.ne.1) then
         x1=rx(nps1)
         y1=ry(nps1)
         r1=rd(nps1)+rd(ip)
         k=i+1
         alfaa=anca(i)
         alfas=ancs(i)
        do j=i+1,nchc
          nps2=lch(ncf(j))
         if (nps2.ne.1) then
          if (nps1.ne.nps2) then
            x2=rx(nps2)
            y2=ry(nps2)
            r2=rd(nps2)+rd(ip)
            vax=x2-x1
            vay=y2-y1
            va=dsqrt(vax*vax+vay*vay)
           if (va.le.r1+r2) then
             teta=dacos(vax/va)
             if (vay.lt.0.D0) teta=-teta
             gama=dacos((va*va+r1*r1-r2*r2)/(2.D0*va*r1))
             beta=dacos((va*va+r2*r2-r1*r1)/(2.D0*va*r2))
             ind1=0
             ind2=0
             alfa1=teta+gama
             alfa2=teta+pi-beta
             if (alfa1.lt.0.D0) alfa1=alfa1+pit2
             if (alfa2.lt.0.D0) alfa2=alfa2+pit2
            if (alfa1.ge.alfas) then
              if (alfa1.le.alfaa) ind1=1 
            else 
             if (alfa1+pit2.le.alfaa) then
               alfa1=alfa1+pit2
               ind1=1
             endif
            endif
            if (alfa2.ge.ancs(j)) then
              if (alfa2.le.anca(j)+epsa) ind2=1
            else
             if (alfa2+pit2.le.anca(j)+epsa) then
               alfa2=alfa2+pit2
               ind2=1
             endif
            endif
            if ((ind1.eq.1).and.(ind2.eq.1)) then
              k=j
             if (alfa1.gt.pit2) then
               alfas=alfa1-pit2
               alfaa=alfaa-pit2
               anca(nchf)=alfaa
             else
               alfas=alfa1
             endif
              anca(j)=alfa2
            endif
           endif
          endif
         
         else

           ximin=anca(j)
           ximax=ancs(j)
          if ((x1.lt.ximax).and.(x1+r1.ge.ximin)) then
           if (y1-r1.le.rd(ip)) then
             alfa=dasin((rd(ip)-y1)/r1)
             if (alfa.lt.0.D0) alfa=alfa+pit2
             xint=x1+r1*dcos(alfa)
            if ((xint.ge.ximin).and.(xint.le.ximax)) then
             if (alfa.ge.alfas) then
              if (alfa.le.alfaa) then
                alfas=alfa
                anca(j)=xint
                k=j
              endif
             else
              if (alfa+pit2.le.alfaa) then
                alfas=alfa
                alfaa=alfaa-pit2
                anca(nchf)=alfaa
                anca(j)=xint
                k=j
              endif
             endif
            endif
           endif
          endif
         endif

        enddo
         i=k
         nchf=nchf+1
         ncf(nchf)=ncf(k)
         anca(nchf)=anca(k)
         ancs(nchf-1)=alfas

       endif  

      goto 10                    ! end of while structure
 20   continue

       ancs(nchf)=ancs(nchc)

        nosha=3
cc        call dispcirc()

      return
      end

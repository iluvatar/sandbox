c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  chabase.f
  
c  A set of small disks is deposited along the line segments that form
c  the base of the wall. 

c
c  Option 1: Criteria for location (sedimentation) of particles
c

c                particle is located in the position where potential energy 
c                is minimun (minimize y coord. in the shadow).
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine chabase()
      implicit none
      include '../VAR/preglob'

c-----local variables
  
      integer i,j,k
      real*8 ysh,yshmin,beta,ramin,xl,yl,sdor,db

c       calculate the length of each line segment in the base
c       npbs = number of points in the base
c       nls = number of line segments in the base of the box
        nls=npbs-1
c       the total number of lines includes the line segments in the base, 
c       the vertical left wall,the lid of the box, and the vertical right wall.

c       All the line segments are numbered counterclockwise beginning with at the left-down
c       corner of the window

        nl=nls+3

      do i=1,nls

          xl=xb(i+1)-xb(i)
	  yl=yb(i+1)-yb(i)
c         sl(i) = length of the segment 'i'
	  sl(i)=dsqrt(xl*xl+yl*yl)
c         usx(i),usy(i)= unitary vector parallel to line segment 'i'
	  usx(i)=xl/sl(i)
	  usy(i)=yl/sl(i)
          rx(i)=xb(i)
          ry(i)=yb(i)

      enddo

        rx(npbs)=xb(npbs)
        ry(npbs)=yb(npbs)

c       nds(i)=number of disks to be placed in the chain in line segment 'i'
c              the number of disks including the first is n+1
	nds(1)=idint(dnint((sl(1)/rdel-1.d0/usx(1))/2.d0))
        nds(1)=max0(1,nds(1))
c       rdb(i)=radius of the disks in line segment 'i'
        rdb(1)=sl(1)/(2.d0*dble(nds(1))+1.d0/usx(1))

      do i=2,npbs-2

c         calculate number and radius of disks in line segment 'i'
          nds(i)=idint(dnint(((sl(i)-rdb(i-1))/rdel-1.d0)/2.d0))
          nds(i)=max0(0,nds(i))
          rdb(i)=(sl(i)-rdb(i-1))/(2.d0*dble(nds(i))+1)

      enddo

      if (npbs.ge.2) then
c        1         2         3         4         5         6         7  
c         calculate the number and radius of disks in line segment 'i'               
          nds(nls)=idint(dnint(((sl(nls)-rdb(npbs-2))
     &                            /rdel-1.d0-1.d0/usx(nls))/2))
          nds(nls)=max0(0,nds(nls))
          rdb(nls)=(sl(nls)-rdb(nls-1))
     &               /(2.d0*dble(nds(nls))+1.d0+1.d0/usx(nls))

      endif

      if (bshch) then
c         the set of small disks is shifted verticaly (downward) in order to reduce gaps
c         and voids between the polyline and the particle set.
c         bshch = boolean indicating if the chain of disks should be shifted
          beta=dasin(rdb(1)/(rdminm+rdb(1)))
          yshmin=((rdminm+rdb(1))*dcos(beta)-rdminm)/usx(1)

        do i=2,nls

            ramin=dmin1(rdb(i-1),rdb(i))
            beta=dasin(ramin/(rdminm+ramin))
            ysh=((rdminm+ramin)*dcos(beta)-rdminm)/usx(i)
            yshmin=dmin1(yshmin,ysh,rdb(i))

        enddo

      endif

        k=0

      do i=1,nls
c         calculate the list of small disks in the chain

        if (i.eq.1) then
c           sdor = distance of the origin of the disks along a line segment
            sdor=rdb(1)/usx(1)
        
        else

            sdor=rdb(i-1)+rdb(i)

        endif

        do j=0,nds(i)

            k=k+1
            db=sdor+dble(j)*2.d0*rdb(i)
            rx(nl+k)=xb(i)+usx(i)*db
            ry(nl+k)=yb(i)+usy(i)*db-yshmin
            rd(nl+k)=rdb(i)

            lch(k)=nl+k                ! element 1 = horizontal base

        enddo

      enddo
          
c       ndel = number of base line disks
        ndel=k
 
      return
      end



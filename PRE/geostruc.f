c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR geostruc()

c     This routine is called in option : irun 18 :
c     It defines different geological units with homogeneous 
c     granulometry and varying mechanical parameters. This option does
c     not deposit particles ! it uses the particle set generated from
c     irun = 15/16
c     It associates disks to different geologic units with different
c     mechanical parameters.

c     Geological units should be defined by a profile function between (0,rxw),
c     from left to right [jtgu(j)=1], or by a closed polyline [jtgu(j)=2],
c     defined clockwise.

c     Geological profiles should be numbered from the internal (lower)
c     units toward the external (upper) units: this means that if geological 
c     unit 'i' is a profile then it should contain geological units with index
c     'j < i' (profiles and polylines). Closed polylines and should not
c     intersect.
c

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine geostruc()
      implicit none
      include '../VAR/preglob'

c-----Local variables

      integer i,j,k,nps,indi,nints,ingu,jty

      real*8 x1,x2,y1,y2,x0,x3
      real*8 sm,sb,xpar,ypar,rpar,ygus
      real*8 svx,svy,sv,vnx,vny,vpx,vpy,dp,ds,da,psc1,psc2

c ------ Removing particles above lines

      do j=1,ngu
c       ngu = number of geological units
        xsmin=xgu(1,j)
        xsmax=xgu(1,j)
        ysmin=ygu(1,j)
        ysmax=ygu(1,j)
         
c       find the rectangular window that contains the geological unit
       do i=1,npgu(j)
         if (xgu(i,j).lt.xsmin) xsmin=xgu(i,j)
         if (xgu(i,j).gt.xsmax) xsmax=xgu(i,j)
         if (ygu(i,j).lt.ysmin) ysmin=ygu(i,j)
         if (ygu(i,j).gt.ysmax) ysmax=ygu(i,j)
       enddo  
c       jtgu(j) = type of profile definning geological unit:
c             jtgu(j)=1 : geological unit is defined by a profile function
c             between (0,rxw); jtgu(j)=2 : closed polyline
        jty=jtgu(j)
       do i=nl+1,np 
        if (idgd(i).eq.0) then
c         idgd(i) = indicates the number of geological domain to which the
c             disk is associated. idgd(i)=0, then the disk has not yet
c             been associated to a geological unit.
          xpar=rx(i)
          ypar=ry(i)
          rpar=rd(i)
          indi=0
            
c         Find out if disk 'i' belongs to the geological unit 'j':
c         indi = 0/1 : the disk is outside / inside the domain
         if ( (jty.eq.1).and.(ypar+rpar.lt.ysmin) ) then
c          geological unit is defined by a profile function between (0,rxw)
           indi=1    ! the particle is "inside" the slope (beneath ysmin)
         else
           nints=0 ! check if centre of particle is "inside" the slope.
c          (i.e. neither outside nor in the boundary)
           ingu=0
          if (jty.eq.2) then   ! closed geological unit
c           the center of the disk is within the external rectangle 
c           containning the geol. unit
            if ( (xpar.gt.xsmin).and.(xpar.lt.xsmax) ) then
              if ( (ypar.gt.ysmin).and.(ypar.lt.ysmax) ) ingu=1
            endif
          endif
           if ( (jty.eq.1).and.(ypar.lt.ysmax) ) ingu=1
          if (ingu.eq.1) then
c           the disk center may be inside the geological unit
c           The number of intersections between a vertical line with lower point
c           (xpar,ypar) and the geological unit profile, is calculated.
           do k=2,npgu(j)     ! line segments in geol. unit are analized
             x1=xgu(k-1,j)
             x2=xgu(k,j)
             y1=ygu(k-1,j)
             y2=ygu(k,j)
            if (x1.ne.x2) then
             if ((x1.lt.xpar).and.(xpar.le.x2)) then
c              xpar belongs to (x1,x2], an 'upper' profile line segment
               sm=(y2-y1)/(x2-x1)
               sb=y1-sm*x1
               ygus=sm*xpar+sb
c              centre of disk is below the line segment
               if (ypar.lt.ygus) nints=nints+1                
             else
              if ((x2.le.xpar).and.(xpar.lt.x1)) then
c               xpar belongs to (x2,x1], a 'lower' profile line segment
                sm=(y2-y1)/(x2-x1)
                sb=y1-sm*x1
                ygus=sm*xpar+sb
c               centre of disk is below or along the line segment
                if (ypar.le.ygus) nints=nints+1   
              endif
             endif
            else
             if ( (xpar.eq.x1).and.(ypar.lt.dmax1(y1,y2)) ) then
c              the center of disk is along a vertical axis passing by 
c              a boundary line segment, below the upper point
c              x0 / x3 = x coord. of points located previous / after line segment
c              in geological unit 
              if (k.ge.3) then
                x0=xgu(k-2,j)
              else
                x0=xgu(npgu(j)-1,j)
              endif
              if (k.le.npgu(j)-1) then
                x3=xgu(k+1,j)
              else
                x3=xgu(2,j)
              endif
              if ( ((x0-xpar)*(x3-xpar)).gt.0.d0 ) then
c               the vertical segment is the boundary of an 'overhang'
               if (ypar.lt.dmin1(y1,y2)) then
                 nints=nints+1
               else
c                ypar is in the interval [ymin,ymax)
                 if (y1.gt.y2) nints=nints+1
               endif
              else
c               the vertical segment is a 'wall' along the profile
c               ypar is in interval [ymin,ymax)
                if (y1.gt.y2) nints=nints+1
              endif
             endif
            endif
           enddo
          endif

          if (mod(nints,2).eq.1) then
c --------  if the number of intersections 'nints' is odd then
c           the centre of the particle (disk) is 'inside' the slope.
            indi=1
           if (nguc.eq.2) then 
c            check if the whole particle is inside the geological unit:
c            since indi=1, we suppose the whole disk is inside the geol. unit 
             k=2
            do while (k.le.npgu(j))
c             sv = vector parallel to line segment in slope

              x1=xgu(k-1,j)
              x2=xgu(k,j)
              y1=ygu(k-1,j)
              y2=ygu(k,j)

              svx=x2-x1
              svy=y2-y1
              sv=dsqrt(svx**2+svy**2)
c             vn = normalised vector parallel to sv
              vnx=svx/sv
              vny=svy/sv
c             vp = vector from initial segment point to centre of disk
              vpx=xpar-x1
              vpy=ypar-y1
c             dp = perpend. distance from slope segment to center of disk 
              dp=abs((vnx*vpy)-(vny*vpx)) 
             if (dp.lt.rpar) then
c              ds = parallel distance from initial point in slope
c              segment to center of disk
               ds=((vnx*vpx)+(vny*vpy))
c              da = distance indicating intersection between disk and line
               da=dsqrt(rpar**2-dp**2)
               psc1=ds-da
               psc2=ds+da
              if (((0.le.psc1).and.(psc1.le.sv)).or.
     &           ((0.le.psc2).and.(psc2.le.sv))) then
c               the circle intersects the line segment interval (0, sv)
c               Thus the circle is not completely inside the slope
                indi=0
                k=npgu(j)
              endif
             endif             
              k=k+1
            enddo  
           endif
          endif
         endif ! indi has been calculated
c         indi = 1 : disk 'i' is associated to geological unit 'j'
          if (indi.eq.1) idgd(i)=j
        endif ! if disk 'i' has not yet been associated to a geological unit
       enddo ! loop in disk list: i=nl+1,np
      enddo ! loop in geological units: j=1,ngu

c -----This routine creates the list of disks that are located within the 
c      geological units. Disks not belonging to geological units are 
c      eliminated from the list.

       nps=4
      do i=5,np 
       if (idgd(i).ne.0) then
c        disk 'i' is associated to a geological domain
         nps=nps+1
        if (nps.ne.i) then ! this means that nps < 1
          idgd(nps)=idgd(i)
          iobj(nps)=iobj(i)
c         it would be possible to correct for interpenetation
          rd(nps)=rd(i)   
          rx(nps)=rx(i)
          ry(nps)=ry(i)
c         all the other variables concerning the disks are set to 0,
c         thus the system is supposed to be in equilibrium after this stage
          rrot(nps)=0.d0
          vx(nps)=0.d0
          vy(nps)=0.d0
          vrot(nps)=0.d0
        endif
       endif
      enddo

       np=nps
       nd=np-nl

       write(*,*) ' number of disks below the slope profile =',nd

      return
      end

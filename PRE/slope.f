c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     SR slope()
c     Shearbox

c     Creates a Slope by removing particles (ONLY IF IRUN=6)

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
      subroutine slope()
      implicit none
      include '../VAR/preglob'

c-----Local variables

      integer i,j,nps,indi,nints

      real*8 x1,x2,y1,y2,sm,sb,xpar,ypar,rpar,yslp
      real*8 svx,svy,sv,vnx,vny,vpx,vpy,dp,ds,da,psc1,psc2

c ------ Removing particles above lines

         ysmin=ysl(1)
         ysmax=ysl(1)
         nps=nl
         
        do i=1,npsl
         if (ysl(i).lt.ysmin) then
              ysmin=ysl(i)
         endif
         if (ysl(i).gt.ysmax) then
              ysmax=ysl(i)
         endif
        enddo   
         
        do i=nl+1, np
          xpar=rx(i)
          ypar=ry(i)
          rpar=rd(i)
          indi=0
            
          if ((ypar+rpar).le.ysmin) then
             indi=1    ! the particle is "inside" the slope
          else
             nints=0 ! check if centre of particle is "inside" the slope
            if ((ypar+rpar).le.ysmax) then
              do j=2, npsl
                x1=xsl(j-1)
                x2=xsl(j)
                y1=ysl(j-1)
                y2=ysl(j)
                if (x1.ne.x2) then
                  if ((x1.lt.xpar).and.(xpar.le.x2)) then
                     sm=(y2-y1)/(x2-x1)
                     sb=y1-sm*x1
                     yslp=sm*xpar+sb
                    if (ypar.lt.yslp) then
                      nints=nints+1                
                    endif
                  else
                    if ((x2.le.xpar).and.(xpar.lt.x1)) then
                       sm=(y2-y1)/(x2-x1)
                       sb=y1-sm*x1
                       yslp=sm*xpar+sb
                      if (ypar.lt.yslp) then
                        nints=nints+1   
                      endif
                    endif
                  endif
                endif
              enddo
            endif

c -------- number of intersections is odd
c -------- the centre of particle is inside the slope
c -------- indi=1 : suppose the whole particle is inside th slope

            if (mod(nints,2).eq.1) then
                indi=1
             if (nslc.eq.2) then  !check if whole particle is inside the slope
                j=2
              do while (j.le.npsl)
c               sv = vector parallel to line segment in slope
                svx=x2-x1
                svy=y2-y1
                sv=dsqrt(svx**2+svy**2)
c               vn = normalised vector parallel to sv
                vnx=svx/sv
                vny=svy/sv
c               vp = vector from initial segment point to centre of disk
                vpx=xpar-x1
                vpy=ypar-y1
c               dp = perpend. distance from slope segment to center of disk 
                dp=abs((vnx*vpy)-(vny*vpx)) 
               if (dp.lt.rpar) then
c                  ds = parallel distance from initial point in slope
c                  segment to center of disk
                   ds=((vnx*vpx)+(vny*vpy))
c                  da = distance indicating intersection between disk and line
                   da=dsqrt(rpar**2-dp**2)
                   psc1=ds-da
                   psc2=ds+da
                 if (((0.le.psc1).and.(psc1.le.sv)).or.
     &               ((0.le.psc2).and.(psc2.le.sv))) then
c                  the circle intersects the line segment interval (0, sv)
c                  Thus the circle is not completely inside the slope
                   indi=0
                   j=npsl
                 endif
               endif             
                  j=j+1
              enddo  
             endif
            endif
          endif

c ------ The particle (disk) is within the slope
c ------ the disk must be added to the list

          if (indi.eq.1) then
             nps=nps+1
            if (nps.ne.i) then ! this means that nps < 1
              iobj(nps)=iobj(i)
              idgd(nps)=idgd(i)
c             it would be possible to correct for interpenetation
              rd(nps)=rd(i)   
              rx(nps)=rx(i)
              ry(nps)=ry(i)
c             all the other variables concerning the disks are set to 0,
c             thus the system is supposed to be in equilibrium after slope
              rrot(nps)=0.d0
              vx(nps)=0.d0
              vy(nps)=0.d0
              vrot(nps)=0.d0
            endif
          endif 
        enddo   

         np=nps
         nd=np-nl
         write(*,*) ' number of disks below the slope profile =',nd

      return
      end

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
c  depotg.f
  
c  The particles are deposited by means of geometric criteria as to prepare
c  an initial configuration (dense or loose)

c
c  Option 1: Criteria for location (sedimentation) of particles
c

c                particle is located in the position where potential energy 
c                is minimun (minimize y coord. in the shadow).
c
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      subroutine depotg()
      implicit none
      include '../VAR/preglob'

c-----local variables
  
      integer i
      real*8 rcirc,parss,ranf

       ip=4
c      initial conditions for the chain array are specified
       nch=1                   ! initial number of elements in the chain
       lch(0)=2                ! element 2 = left vertical wall
       lch(1)=1                ! element 1 = horizontal base
       lch(2)=4                ! element 4 = right vertical wall

       epsa=0.00000000001d0    ! delta for comparison of angle intervals

      do jgu=1,ngu
        xsmin=xgu(1,jgu)
        xsmax=xgu(1,jgu)
        ysmin=ygu(1,jgu)
        ysmax=ygu(1,jgu)
         
c       find the rectangular window that contains the geological unit
       do i=1,npgu(jgu)
         if (xgu(i,jgu).lt.xsmin) xsmin=xgu(i,jgu)
         if (xgu(i,jgu).gt.xsmax) xsmax=xgu(i,jgu)
         if (ygu(i,jgu).lt.ysmin) ysmin=ygu(i,jgu)
         if (ygu(i,jgu).gt.ysmax) ysmax=ygu(i,jgu)
       enddo  

c       Each geological unit is analized
        rdmin=rdming(jgu)
        rdmax=rdmaxg(jgu)
        ngran=ngrang(jgu)
        psp=pspg(jgu)
        pvsp=pvspg(jgu)

       if (ngran.eq.4) then
         psp=1.d0/(1.d0+rdmin*rdmin*(1.d0-pvsp)/(rdmax*rdmax*pvsp))
       endif

        inside=1

       do while (inside.eq.1)
c        deposit a new particle in geological unit 'jgu', while the disk is 
c        beneath the profile
         ip=ip+1

         parss=ranf()

         if ((ehgu.eq.1).and.(jgu.eq.nehgu)) then
          if (parss.lt.cris) then
*           choose the original particles
            rdmin=rdming(jgu)
            rdmax=rdmaxg(jgu)
          else
*           choose heterogeneity particles
            rdmin=rdming(ngu+1)
            rdmax=rdmaxg(ngu+1)
          endif
         endif
         
         call radius(rcirc)     ! this routine is in prepa.f  
         rd(ip)=rcirc

         vx(ip)=0.d0
         vy(ip)=0.d0
         vrot(ip)=0.0d0
         rrot(ip)=0.d0
         iobj(ip)=1
c        idgd(ip) = number of geological domain associated to particle 'i'
c        this value is calcualted in 'geostruc'
         idgd(ip)=jgu
         call shadow()
         call geochain()
       enddo

      enddo  

       np=ip
       nd=np-nl

      return
      end  

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   
      include '../PRE/geochain.f'
